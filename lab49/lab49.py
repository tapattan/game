import arcade

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

 
class MyGame(arcade.View):  #Class ของด่านสอง
    def __init__(self):
        super().__init__()
        self.setup()

        arcade.set_background_color(arcade.color.AMAZON)
    
    def setup(self):
        
        self.pico1 = arcade.SpriteList(use_spatial_hash=True)
        pico1 = arcade.Sprite('pico1.png')
        pico1.center_x = 200
        pico1.center_y = 150
        pico1.scale = 0.2
        self.pico1.append(pico1)
      
        
        self.pico2 = arcade.SpriteList(use_spatial_hash=True)
        pico2 = arcade.Sprite('pico2.png')
        pico2.center_x = 50
        pico2.center_y = 150
        pico2.scale = 0.2
        self.pico2.append(pico2)
      
       
        ### zone load map ########
        # Name of map file to load
        map_name = "map49.json"

        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "Tile Layer 1": {
                "use_spatial_hash": True,
            },
        }

        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)
        self.Level1 = self.tile_map.sprite_lists["Tile Layer 1"]
        # Keep player from running through the wall_list layer
        walls = [self.Level1,]# self.pico1]
        #walls2 = [self.Level1,]# self.pico2]

        self.physics_engine = arcade.PhysicsEnginePlatformer(
            self.pico1[0], walls, gravity_constant=1
        )  
        self.physics_engine2 = arcade.PhysicsEnginePlatformer(
            self.pico2[0], walls, gravity_constant=1
        )
        
    

        self.statushit = False 
        self.who_act1x = 0
        self.who_act1y = 0
        self.who_act2x = 0
        self.who_act2y = 0

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here
        self.Level1.draw()
        self.pico1.draw()
        self.pico2.draw()

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.W:
            #self.pico1[0].change_y = 5
            self.who_act1y = 1
            
        elif key == arcade.key.S:
            #self.pico1[0].change_y = -5
            self.who_act1y = 2
           
        if key == arcade.key.A:
            #self.pico1[0].change_x = -5
            self.who_act1x = 3
            
        elif key == arcade.key.D:
            #self.pico1[0].change_x = 5
            self.who_act1x = 4
           

        if(key==arcade.key.I):
            #self.pico2[0].change_y =  5
            self.who_act2y = 5
           
        elif key == arcade.key.K:
            #self.pico2[0].change_y = -5
            self.who_act2y = 6
            
        if key == arcade.key.J:
            #self.pico2[0].change_x = -5
            #self.pico2[0].texture = self.pico2left
            self.who_act2x = 7
           
        elif key == arcade.key.L:
            #self.pico2[0].change_x = 5
            #self.pico2[0].texture = self.pico2right
            self.who_act2x = 8
          
        
  

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        
        self.physics_engine.update()
        self.physics_engine2.update()
        self.pico1.update()
        self.pico2.update()   
         
        if(arcade.check_for_collision(self.pico1[0],self.pico2[0])):
           self.statushit = True
        else:
           self.statushit = False 
        
        if(self.who_act1x==3):
          self.pico1[0].center_x-=5
        elif(self.who_act1x==4):
          self.pico1[0].center_x+=5  

        if(self.who_act1y==1): #W
          self.pico1[0].center_y+=15
        elif(self.who_act1y==2):
          self.pico1[0].center_y-=5      

        if(self.who_act2x==7):
          self.pico2[0].center_x-=5  
        elif(self.who_act2x==8):
          self.pico2[0].center_x+=5 
        
        if(self.who_act2y==5):
          self.pico2[0].center_y+=15
        elif(self.who_act2y==6):
          self.pico2[0].center_y-=5        

        if(arcade.check_for_collision(self.pico1[0],self.pico2[0])):
          if(self.who_act1x==3):
            self.pico1[0].center_x+=5
          elif(self.who_act1x==4):
            self.pico1[0].center_x-=5
          if(self.who_act1y==1): #W
            self.pico1[0].center_y-=5
          elif(self.who_act1y==2):
            self.pico1[0].center_y+=5

          if(self.who_act2x==7):
            self.pico2[0].center_x+=5
          elif(self.who_act2x==8):
            self.pico2[0].center_x-=5

          if(self.who_act2y==5):
            self.pico2[0].center_y-=5
          elif(self.who_act2y==6):
            self.pico2[0].center_y+=5

        c = arcade.check_for_collision_with_list(self.pico1[0],self.Level1)
        if(len(c)>0):
          if(self.who_act1x==3):
            self.pico1[0].center_x+=5
          elif(self.who_act1x==4):
            self.pico1[0].center_x-=5

          if(self.who_act1y==1): #W
            self.pico1[0].center_y-=5
          elif(self.who_act1y==2):
            self.pico1[0].center_y+=5    

        c = arcade.check_for_collision_with_list(self.pico2[0],self.Level1)
        if(len(c)>0):
          if(self.who_act2x==7):
            self.pico2[0].center_x+=5
          elif(self.who_act2x==8):
            print('n')
            self.pico2[0].center_x-=5

          if(self.who_act2y==5):
            self.pico2[0].center_y-=5
          elif(self.who_act2y==6):
            self.pico2[0].center_y+=5       

        pass
    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        if(key == arcade.key.W or key == arcade.key.S):
           self.who_act1y = 0  
        if(key == arcade.key.A or key == arcade.key.D):
           self.who_act1x = 0 

        if(key == arcade.key.I or key == arcade.key.K):
           self.who_act2y = 0  
        if(key == arcade.key.J or key == arcade.key.L):
           self.who_act2x = 0    



def main():
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame() 
    window.show_view(game)
    arcade.run()



if __name__ == "__main__":
    main()
