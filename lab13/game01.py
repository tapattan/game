import arcade
SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500

import pyglet
import random 
import numpy as np
class trash:
    sp : arcade.Sprite 
    status : bool 
    def __init__(self,sp,status):
       self.sp = sp
       self.status = status 

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height,'🐶  🕶 เกมของชั้น')
 
        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        self.sea = arcade.Sprite('../resources/sea.png',0.4) #None’, ‘Simple’ or ‘Detailed’.
    
        self.sea.center_x = 110
        self.sea.center_y = 200
        self.timedelta = 0

        self.color_sea = '#000000'
        
        

        self.hook = arcade.Sprite('../resources/hook.png',0.1) #None’, ‘Simple’ or ‘Detailed’. ,hit_box_algorithm='Detailed'
        self.hook_obj = arcade.Sprite('../resources/hook_obj.png',0.1) #None’, ‘Simple’ or ‘Detailed’. ,hit_box_algorithm='Detailed'
        
        self.col_trash = []
        for i in range(9):
          if(i<3):
            obj = arcade.Sprite('../resources/can.png',0.05) 
          elif(i<6):
            obj = arcade.Sprite('../resources/boot.png',0.05)   
          else:
            obj = arcade.Sprite('../resources/bottle.png',0.1)   

          obj.center_x = random.randint(10,490)
          obj.center_y = random.randint(10,300)
        
          obj.angle = random.randint(-360,360)
          self.col_trash.append(trash(obj,False))

        self.max_trash = len(self.col_trash)
        self.score = (self.max_trash - len(self.col_trash))*int(np.floor((255/self.max_trash)))
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        #self.hook_obj.draw_hit_box(color=[255,0,0])

        self.sea.color = arcade.color_from_hex_string(self.color_sea)
        self.sea.draw()

        for i in self.col_trash:
            i.sp.draw()

        self.hook.draw()
    
    #Called when the user presses a mouse button
    def on_mouse_press(self, x, y, button, key_modifiers):
         
        pass

    #Called when the user presses a mouse button.
    def on_mouse_release(self, x: float, y: float, button: int, modifiers: int):
        self.hook.angle = 0
         
        pass

    #User moves mouse
    def on_mouse_motion(self, x: float, y: float, dx: float, dy: float):
        self.hook.center_x = x 
        self.hook.center_y = y*1.6

        self.hook_obj.center_x = self.hook.center_x 
        self.hook_obj.center_y = self.hook.center_y
        
        for i in self.col_trash:
          if(i.status):
            i.sp.center_x = self.hook_obj.center_x
            i.sp.center_y = self.hook_obj.center_y-200

        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass  
        
        if(self.hook_obj.center_y>750):
            for i in range(len(self.col_trash)):
                if(self.col_trash[i].status):
                    del self.col_trash[i]
                    self.score = (self.max_trash - len(self.col_trash))*int(np.floor((255/self.max_trash)))
                    break

        if(self.hook.center_y<=250):
            self.hook.center_y = 250

        self.timedelta +=delta_time
 
        if(int(self.timedelta)%2==0):
            self.sea.center_y = self.sea.center_y-1
        else:
            self.sea.center_y = self.sea.center_y+1

        
        for i in self.col_trash:
          hit_hook = arcade.check_for_collision(self.hook_obj,i.sp)
          if(hit_hook):
            i.status = True
       

        
        self.color_sea = '#%02X%02X%02X' % (255,255,self.score )

        

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)

    #window = pyglet.window.Window()

    #icon1 = pyglet.image.load('apple.ico')
    #game.set_icon(icon1)
    
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()