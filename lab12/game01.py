import arcade
SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500

import pyglet
import random 
class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height,'🐶  🕶 เกมของชั้น')
 
        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        self.sea = arcade.Sprite('../resources/sea.png',0.4) #None’, ‘Simple’ or ‘Detailed’.
    
        self.sea.center_x = 110
        self.sea.center_y = 200
        self.timedelta = 0

        self.color_sea = '#000000'
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        
        self.sea.color = arcade.color_from_hex_string(self.color_sea)
        self.sea.draw()

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass 
        
        self.timedelta +=delta_time
        if(self.timedelta>12):
            self.timedelta = 0
            #r = lambda: random.randint(0,255)
            #g = lambda: random.randint(0,255)
            #b = lambda: random.randint(0,255)
        
        z = int(self.timedelta)*30
        if(int(self.timedelta)%2==0):
            self.sea.center_y = self.sea.center_y-1
        else:
            self.sea.center_y = self.sea.center_y+1

        if(z>255):
           z = 255
        self.color_sea = '#%02X%02X%02X' % (255,255,z)

        print(z,self.timedelta)
        #print( '#%02X%02X%02X' % (r(),g(),b()))
      
        
        


def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)

    #window = pyglet.window.Window()

    icon1 = pyglet.image.load('apple.ico')
    game.set_icon(icon1)
    
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()