import arcade
import random 

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        pass
        
        width = 50
        height = 10
        border_size = 1
        
        self.HP = 50
        
        box = arcade.SpriteSolidColor(
            self.HP - border_size*3  ,
            height - border_size*3 ,
            arcade.color.GREEN,)
        
        box2 = arcade.SpriteSolidColor(
            width + border_size,
            height + border_size,
            arcade.color.BLACK,)
        
        box.center_x = 300  
        box.center_y = 300  
        box2.center_x = 300
        box2.center_y = 300
        
        self.HPBAR = arcade.SpriteList()
        self.HPBAR.append(box2)
        self.HPBAR.append(box)
        
       
        
        self.hero = arcade.Sprite('hero.png',0.5)
        self.hero.center_x = 300
        self.hero.center_y = 300 - 25
        self.dead = False 
        
        self.star_weapon = arcade.Sprite('star_weapon.png',0.5)
        self.star_weapon.center_x = 700
        self.star_weapon.center_y = random.randint(10,580)
        self.star_weapon.change_x = -5
        self.star_weapon.change_angle = 5

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.HPBAR.draw()
        self.hero.draw()
        self.star_weapon.draw()
        # Your drawing code goes here

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.star_weapon.update()
        self.HPBAR.update()
        self.hero.update()
        
        if(arcade.check_for_collision(self.hero,self.star_weapon) and self.dead==False):
            self.HP = self.HP - 10
            self.star_weapon.center_x = 700
            if(self.HP<=0):
               self.hero.angle = 90
               self.dead = True
        
        if(self.star_weapon.center_x < -20):
           self.star_weapon.center_x = 700
           self.star_weapon.center_y = random.randint(10,580)
        
            
          
        for i in range(len(self.HPBAR)):
            self.HPBAR[i].center_x = self.hero.center_x
            self.HPBAR[i].center_y = self.hero.center_y + 25
            
            if(i==1):
              if(self.HP<=0):
                 self.HPBAR[i].width = 0 
              else:     
                 self.HPBAR[i].width = self.HP - 2
                 
              self.HPBAR[i].left = self.HPBAR[i].center_x - 24
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        
        if(self.dead==False):
            if key == arcade.key.UP:
                self.hero.change_y = 5
            elif key == arcade.key.DOWN:
                self.hero.change_y = -5
            elif key == arcade.key.LEFT:
                self.hero.change_x = -5
            elif key == arcade.key.RIGHT:
                self.hero.change_x = 5

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP or key == arcade.key.DOWN:
            self.hero.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.hero.change_x = 0



def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()