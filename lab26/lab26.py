import arcade

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600


class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        pass
        
        width = 80
        height = 10
        border_size = 1
        box = arcade.SpriteSolidColor(
            width - border_size*3  ,
            height - border_size*3,
            arcade.color.GREEN,)
        
        box2 = arcade.SpriteSolidColor(
            width + border_size,
            height + border_size,
            arcade.color.BLACK,)
        
        box.center_x = 300  
        box.center_y = 300  
        box2.center_x = 300
        box2.center_y = 300
        
        self.HPBAR = arcade.SpriteList()
        self.HPBAR.append(box2)
        self.HPBAR.append(box)

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.HPBAR.draw()
        # Your drawing code goes here

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass


def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()