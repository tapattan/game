import arcade
import random 
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        pass
        self.rb01 = arcade.Sprite('rb01.png')
        self.rb01.center_x = 100 
        self.rb01.center_y = 100
        self.rb01.scale = 0.05
        
        self.heroHP = 100
        self.herostart = self.heroHP

        self.hpbar = arcade.SpriteSolidColor(self.heroHP,10,arcade.color.GREEN)
        self.hpbar.center_x = self.rb01.center_x 
        self.hpbar.center_y = self.rb01.center_y + 50 

        self.hpbarBorder = arcade.SpriteSolidColor(self.heroHP+4,14,arcade.color.BLACK)
        self.hpbarBorder.center_x = self.rb01.center_x 
        self.hpbarBorder.center_y = self.rb01.center_y + 50

        self.candycane = arcade.Sprite('candycane.png')
        self.candycane.center_x = 800
        self.candycane.center_y = 200#random.randint(10,580)
        self.candycane.scale = 0.2
        self.candycane.change_x = -5
        self.candycane.change_angle = 5

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.rb01.draw()
        self.candycane.draw()
        self.hpbarBorder.draw()
        self.hpbar.draw()
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        
        self.candycane.update()
        self.rb01.update()

        if(self.candycane.center_x < -50):
           self.candycane.center_x = 650
           self.candycane.center_y = 200# random.randint(10,580)
        
        self.hpbar.width = self.heroHP #ความกว้างต้องเท่ากับเลือดที่มี
        #self.hpbar.center_x = self.rb01.center_x
        self.hpbar.center_x = self.rb01.center_x
        self.hpbar.left = self.hpbar.center_x - (self.herostart/2) #************* การตรึงเลือดให้อยู่ฝั่งซ้ายตลอด !!!
        self.hpbar.center_y = self.rb01.center_y + 50


        self.hpbarBorder.center_x = self.rb01.center_x
        self.hpbarBorder.center_y = self.rb01.center_y + 50 #ให้กรอบของหลอดเลือดสูงกว่าตัวละคร 
        
        if(self.heroHP > 0):
          c = arcade.check_for_collision(self.rb01,self.candycane)
          if c:
               #ให้หลอดเลือดสีเขียวความกว้างมันลดลงทีละ 10
               self.candycane.center_x = -60
           
               self.heroHP = self.heroHP - 10 #ลดเลือดจริงๆ 

            
        else:
          self.rb01.angle = 90  

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        
        if key == arcade.key.W:
            self.rb01.change_y = 5
        elif key == arcade.key.S:
            self.rb01.change_y = -5
        elif key == arcade.key.A:
            self.rb01.change_x = -5
        elif key == arcade.key.D:
            self.rb01.change_x = 5

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.W or key == arcade.key.S:
            self.rb01.change_y = 0
        elif key == arcade.key.A or key == arcade.key.D:
            self.rb01.change_x = 0


def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
