import arcade

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600


class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)
    
    def setup(self):
        # Set up your game here
        
        self.pico1 = arcade.SpriteList(use_spatial_hash=True)
        pico1 = arcade.Sprite('pico1.png')
        pico1.center_x = 200
        pico1.center_y = 300
        pico1.scale = 0.2
        self.pico1.append(pico1)
        
        
        self.pico2 = arcade.SpriteList(use_spatial_hash=True)
        pico2 = arcade.Sprite('pico2.png')
        pico2.center_x = 50
        pico2.center_y = 300
        pico2.scale = 0.2
        self.pico2.append(pico2)

        ### zone load map ########
        # Name of map file to load
        map_name = "map45.json"


        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "Tile Layer 1": {
                "use_spatial_hash": True,
            },
        }

        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)


        self.Level1map = self.tile_map.sprite_lists["Tile Layer 1"]

        # Keep player from running through the wall_list layer
        walls = [self.Level1map, self.pico1]
        
        walls2 = [self.Level1map, self.pico2]

       
 
        self.physics_engine = arcade.PhysicsEnginePlatformer(
            self.pico1[0], walls2, gravity_constant=1
        )  
        self.physics_engine2 = arcade.PhysicsEnginePlatformer(
            self.pico2[0], walls, gravity_constant=1
        )  
      
   
        ### end zone load map ####
        #pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.Level1map.draw()
        # Your drawing code goes here
        self.pico1.draw()
        self.pico2.draw()
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        if(self.pico1[0].center_y>self.pico2[0].center_y):
          self.physics_engine2.update()
          self.physics_engine.update()
        else:
          self.physics_engine.update()
          self.physics_engine2.update()  

      
        
        pass

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.W:
            self.pico1[0].change_y = 10
        elif key == arcade.key.S:
            self.pico1[0].change_y = -5
        elif key == arcade.key.A:
            self.pico1[0].change_x = -5
        elif key == arcade.key.D:
            self.pico1[0].change_x = 5

        if key == arcade.key.I:
            self.pico2[0].change_y = 10
        elif key == arcade.key.K:
            self.pico2[0].change_y = -5
        elif key == arcade.key.J:
            self.pico2[0].change_x = -5
        elif key == arcade.key.L:
            self.pico2[0].change_x = 5    

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.W or key == arcade.key.S:
            self.pico1[0].change_y = 0
        elif key == arcade.key.A or key == arcade.key.D:
            self.pico1[0].change_x = 0
        
        if key == arcade.key.I or key == arcade.key.K:
            self.pico2[0].change_y = 0
        elif key == arcade.key.J or key == arcade.key.L:
            self.pico2[0].change_x = 0



def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()