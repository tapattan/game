import arcade

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600


class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)
    
    def setup(self):
        # Set up your game here
        
        self.pico1 = arcade.SpriteList(use_spatial_hash=True)
        pico1 = arcade.Sprite('pico1.png')
        pico1.center_x = 200
        pico1.center_y = 300
        pico1.scale = 0.2
        self.pico1textureL = arcade.load_texture('pico1.png',flipped_horizontally=True)
        self.pico1textureR = arcade.load_texture('pico1.png',flipped_horizontally=False)
        self.pico1.append(pico1)
        
        
        self.pico2 = arcade.SpriteList(use_spatial_hash=True)
        pico2 = arcade.Sprite('pico2.png')
        pico2.center_x = 50
        pico2.center_y = 320
        pico2.scale = 0.2
        self.pico2textureL = arcade.load_texture('pico2.png',flipped_horizontally=True)
        self.pico2textureR = arcade.load_texture('pico2.png',flipped_horizontally=False)
        self.pico2.append(pico2)
        
        self.pico3textureL = arcade.load_texture('pico3.png',flipped_horizontally=True)
        self.pico3textureR = arcade.load_texture('pico3.png',flipped_horizontally=False)

        ### zone load map ########
        # Name of map file to load
        map_name = "map45.json"


        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "Tile Layer 1": {
                "use_spatial_hash": True,
            },
        }

        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)


        self.Level1map = self.tile_map.sprite_lists["Tile Layer 1"]

        # Keep player from running through the wall_list layer
        walls = [self.Level1map, self.pico1]
        
        walls2 = [self.Level1map, self.pico2]

       
 
        self.physics_engine = arcade.PhysicsEnginePlatformer(
            self.pico1[0], walls2, gravity_constant=1
        )  
        self.physics_engine2 = arcade.PhysicsEnginePlatformer(
            self.pico2[0], walls, gravity_constant=1
        )  
      
   
        ### end zone load map ####
        #pass

        self.who_walk = 0
        self.fussion = False
        self.fussion_action = False

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.Level1map.draw()
        # Your drawing code goes here
        self.pico1[0].draw()
        self.pico2[0].draw()
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
     
        if(self.fussion_action):
           self.fussion_action = False 

           near = abs(self.pico1[0].center_x - self.pico2[0].center_x)
           print(near)
           if(near <= 20):
              print('near')  
              self.fussion = True
              self.pico2[0].center_x = 10
              self.pico2[0].center_y = 500
              self.pico2[0].scale = 0.001
              self.pico1[0].texture = self.pico3textureR 
              self.pico1[0].scale = 0.4

           elif(self.fussion):
              self.who_walk = 1 
              self.pico2[0].scale = 0.2
              self.fussion = False
              self.pico1[0].scale = 0.2 
              self.pico2[0].scale = 0.2
              x1 = self.pico1[0].center_x 
              y1 = self.pico1[0].center_y
              self.pico1[0].center_x = x1 - 50
              self.pico1[0].center_y = 200
              
              self.pico1[0].texture = self.pico1textureL
              print(x1,y1)
             
              self.pico2[0].center_x = x1 + 50
              self.pico2[0].center_y = 200

              self.pico2[0].texture = self.pico2textureR
     
        p = 0
        if(self.pico1[0].center_y>self.pico2[0].center_y and p==0):
          self.physics_engine2.update()
          self.physics_engine.update()
          p = 1
          print(1)
        elif(self.pico1[0].center_y<self.pico2[0].center_y and p==0):
          self.physics_engine.update()
          self.physics_engine2.update() 
          p = 1
          print(2)
        
        if(self.who_walk == 1 and p==0):
          self.physics_engine2.update()
          self.physics_engine.update() 
          print(3)
        elif(self.who_walk==2 and p==0):
          self.physics_engine.update()
          self.physics_engine2.update()
          print(4)
        
        
        

        #if(arcade.check_for_collision(self.pico1[0],self.pico2[0])):
        #  print('hit')
        pass

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.W:
            self.pico1[0].change_y = 10
            self.who_walk = 1
        elif key == arcade.key.S:
            self.pico1[0].change_y = -5
            self.who_walk = 1
        elif key == arcade.key.A:
            self.pico1[0].change_x = -5
            if(self.fussion):
              self.pico1[0].texture = self.pico3textureL
            else:
              self.pico1[0].texture = self.pico1textureL    

            self.who_walk = 1
        elif key == arcade.key.D:
            self.pico1[0].change_x = 5

            if(self.fussion):
              self.pico1[0].texture = self.pico3textureR
            else:
              self.pico1[0].texture = self.pico1textureR
            self.who_walk = 1

        if key == arcade.key.I:
            self.pico2[0].change_y = 10
            self.who_walk = 2
        elif key == arcade.key.K:
            self.pico2[0].change_y = -5
            self.who_walk = 2
        elif key == arcade.key.J:
            self.pico2[0].change_x = -5
            self.pico2[0].texture = self.pico2textureL
            self.who_walk = 2
        elif key == arcade.key.L:
            self.pico2[0].change_x = 5 
            self.pico2[0].texture = self.pico2textureR   
            self.who_walk = 2

        if(key == arcade.key.SPACE):
           self.fussion_action = True
           


    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.W or key == arcade.key.S:
            self.pico1[0].change_y = 0
        elif key == arcade.key.A or key == arcade.key.D:
            self.pico1[0].change_x = 0
        
        if key == arcade.key.I or key == arcade.key.K:
            self.pico2[0].change_y = 0
        elif key == arcade.key.J or key == arcade.key.L:
            self.pico2[0].change_x = 0



def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()