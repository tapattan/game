import arcade
import random

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

class Gamestage1(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.BLACK)
        self.setup()

    def setup(self):
        # Set up your game here
        self.hero = arcade.Sprite('hero/pusheen1.gif',0.3) 
        self.hero.center_x = 40
        self.hero.center_y = 200

        heroTexture1 = arcade.load_texture('hero/pusheen1.gif')  
        heroTexture2 = arcade.load_texture('hero/pusheen2.gif')  
        heroTexture3 = arcade.load_texture('hero/pusheen3.gif') 
        heroTexture4 = arcade.load_texture('hero/pusheen4.gif')
        self.heroTextureList = []
        self.heroTextureList.append(heroTexture1)
        self.heroTextureList.append(heroTexture2)
        self.heroTextureList.append(heroTexture3)
        self.heroTextureList.append(heroTexture4)
        
        heroTexture1 = arcade.load_texture('hero/pusheen1_L.gif')  
        heroTexture2 = arcade.load_texture('hero/pusheen2_L.gif')  
        heroTexture3 = arcade.load_texture('hero/pusheen3_L.gif') 
        heroTexture4 = arcade.load_texture('hero/pusheen4_L.gif')
        self.heroTextureList_L = []
        self.heroTextureList_L.append(heroTexture1)
        self.heroTextureList_L.append(heroTexture2)
        self.heroTextureList_L.append(heroTexture3)
        self.heroTextureList_L.append(heroTexture4)

        self.heroDirect = 'right'

        self.heroFrameAt = 0
        self.heroTimeAt = 0

        self.statusJump = False
        self.JumpTime =0
        #load map
        map_name = "lab42.json"

        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "wall_layer": {
                "use_spatial_hash": True,
            },
            "ceiling_layer": {
                "use_spatial_hash": True,
            }
            
        }

        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)


        self.wall_layer = self.tile_map.sprite_lists["wall_layer"]
        self.ceiling_layer = self.tile_map.sprite_lists["ceiling_layer"]
        

        ground = [self.wall_layer,self.ceiling_layer ]
        self.physics_engine = arcade.PhysicsEnginePlatformer(
            self.hero, ground, gravity_constant=1
        )
        

        self.pass_wall = 0
        for k in range(10):
          for i in self.wall_layer:
            i.center_x = i.center_x-20


        self.rock_ball = arcade.Sprite('rock_ball.png')
        self.rock_ball.center_x = 880 
        self.rock_ball.center_y = 280
        self.rock_ball.scale = 0.3
        self.rock_ball.change_angle = 5
        self.rock_ball.change_x = -5


    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here
        self.wall_layer.draw()
        self.ceiling_layer.draw()
        self.hero.draw()
        
        self.rock_ball.draw()
        #arcade.draw_text('Game Over', 10, 20, arcade.color.WHITE, 20)
        
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.physics_engine.update()
        self.rock_ball.update()
        self.wall_layer.update()
        self.hero.update()
        
        ############ ให้หินกลับมาเริ่มไหลใหม่ ################
        if(self.rock_ball.center_x < -50):
           self.rock_ball.center_x =880 
        


        ############ ส่วนควบคุมให้หลุมเลื่อน ####################
        if(self.hero.center_x>=250 and self.hero.center_x<=600):
          if(self.pass_wall<40):
            for i in self.wall_layer:
             i.center_x = i.center_x+5
        
            self.pass_wall = self.pass_wall+1

        if(self.hero.center_x>=600):
          if(self.pass_wall>0):
            for i in self.wall_layer:
             i.center_x = i.center_x-5
        
            self.pass_wall = self.pass_wall-1  
        ############ END ส่วนควบคุมให้หลุมเลื่อน ####################
       

    
        
        ############## zone ควบคุมการกระโดดของ hero ###########
        if(self.statusJump == True):
           self.JumpTime+=delta_time

           if(self.JumpTime>0.5):
              self.statusJump = False
              self.JumpTime = 0

        ################## zone ควบคุมการ animation ของhero ################
        self.heroTimeAt = self.heroTimeAt+delta_time 
        if(self.heroTimeAt>0.2): #ถ้าเวลาของเทอ ผ่านไป 0.2 วินาที 
           self.heroFrameAt = self.heroFrameAt + 1 
           self.heroTimeAt = 0 #reset เวลา 

        if(self.heroFrameAt==4):
           self.heroFrameAt = 0 
        
        #เอาเสื้อผ้าช่องไหน มาให้ตัวละคร
        self.hero.texture = self.heroTextureList[self.heroFrameAt]
        #เอาเสื้อผ้าช่องไหน มาให้ตัวละคร
        if(self.heroDirect == 'right'):
           self.hero.texture = self.heroTextureList[self.heroFrameAt] #ชุดหันขวา 
        elif(self.heroDirect == 'left'):
           self.hero.texture = self.heroTextureList_L[self.heroFrameAt] #ชุดหันขวา 
        ########################### end zone ###############################


        ############# ส่วนควบคุมการเดินของตัวละครหลัก ############
        if self.hero.center_x >= 770:
            self.hero.center_x = 770

        if self.hero.center_x <= 30:
            self.hero.center_x = 30

        ####################################    

        
        ########## ถ้าหินทับเรา !!!!!!!!
        Q = arcade.check_for_collision(self.hero,self.rock_ball)
        if (Q):
            print('hit rock')
            game_view = GameOver()
            self.window.show_view(game_view)


        ########### เดินมาถึงขวาสุดแล้ว
        if(self.hero.center_x >= 770):
            game_view = GameWin()
            self.window.show_view(game_view)


    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.UP or key == arcade.key.W:
            if(self.statusJump==False):
               self.hero.change_y = 10
               self.statusJump = True 

        elif key == arcade.key.DOWN or key == arcade.key.S:
            self.hero.change_y = -5

        elif key == arcade.key.LEFT or key == arcade.key.A:
            self.hero.change_x = -5
            self.heroDirect = 'left'

        elif key == arcade.key.RIGHT or key == arcade.key.D:
            self.hero.change_x = 5
            self.heroDirect = 'right'

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP or key == arcade.key.DOWN or key == arcade.key.W or key == arcade.key.S :
            self.hero.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT or key == arcade.key.D or key == arcade.key.A:
            self.hero.change_x = 0


#ฉาก win
class GameWin(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.BLACK)
        self.setup()

    def setup(self):
        # Set up your game here
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here
      
        #arcade.draw_text('Game Over', 10, 20, arcade.color.WHITE, 20)
        arcade.draw_text('You Win', 250, 350, arcade.color.WHITE, 60)
        arcade.draw_text('Press Space to play again', 250, 300, arcade.color.WHITE, 20)


    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        pass 

        if key == arcade.key.SPACE:
           game_view = Gamestage1()
           self.window.show_view(game_view)

#ฉาก GameOver
class GameOver(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.BLACK)
        self.setup()

    def setup(self):
        # Set up your game here
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here
      
        #arcade.draw_text('Game Over', 10, 20, arcade.color.WHITE, 20)
        arcade.draw_text('Game Over', 200, 350, arcade.color.WHITE, 60)
        arcade.draw_text('Press Space to play again', 250, 300, arcade.color.WHITE, 20)

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        pass 

        if key == arcade.key.SPACE:
           game_view = Gamestage1()
           self.window.show_view(game_view)

 
def main():
    #game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    #window.total_score = 0
    game = Gamestage1()   
    window.show_view(game)
    arcade.run()



if __name__ == "__main__":
    main()
