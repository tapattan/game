import arcade
import random

# Size/title of the window
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
SCREEN_TITLE = "แบบฝึกหัดเกมของชั้น"


class Particle(arcade.SpriteSolidColor):
    """ Particle from explosion """
    def update(self):
        """ Move the particle, and fade out """
        # Move
        self.center_x += self.change_x
        self.center_y += self.change_y
        # Fade
        self.alpha -= 5
        if self.alpha <= 0:
            self.remove_from_sprite_lists()


class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height, title):
        """ Initializer """
        # Call the parent class initializer
        super().__init__(width, height, title)
        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        """ Set up the game and initialize the variables. """
        pass
        self.bullet_sprite_list = arcade.SpriteList()
  
    def on_draw(self):
        """ Render the screen. """
        # This command has to happen before we start drawing
        arcade.start_render()
        self.bullet_sprite_list.draw()
 

    def on_update(self, delta_time):
        """ Movement and game logic """
        pass
        self.bullet_sprite_list.update()
 

    def on_mouse_release(self, x: int, y: int, button: int, modifiers: int):
        for i in range(12):
                    particle = Particle(4, 4, arcade.color.RED)
                    while particle.change_y == 0 and particle.change_x == 0:
                        particle.change_y = random.randrange(-3, 3)
                        particle.change_x = random.randrange(-3, 3)
                    particle.center_x = x
                    particle.center_y = y
                    self.bullet_sprite_list.append(particle)


    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.SPACE:
             for i in range(12):
                    particle = Particle(4, 4, arcade.color.RED)
                    while particle.change_y == 0 and particle.change_x == 0:
                        particle.change_y = random.randrange(-3, 3)
                        particle.change_x = random.randrange(-3, 3)
                    particle.center_x = 300
                    particle.center_y = 300
                    self.bullet_sprite_list.append(particle)

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        pass 


def main():
    """ Main function """
    window = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()