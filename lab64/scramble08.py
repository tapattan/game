import arcade
import random 
import pandas as pd

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
       
class bb(arcade.SpriteSolidColor):
    def __init__(self,x,y,tag):
        self.posx = x 
        self.posy = y
        self.tag = tag

        super().__init__(150,150,arcade.color.GREEN)
        self.center_x = (150*self.posx)+75
        self.center_y = 600-75-(150*self.posy)
    
    def on_draw(self):
        
        #arcade.draw_rectangle_filled((150*self.posx)+75,600-75-(150*self.posy), 150, 150, arcade.color.BLACK)
        #arcade.draw_rectangle_filled((150*self.posx)+75,600-75-(150*self.posy), 150-5, 150-5, arcade.color.BLUSH)

        arcade.draw_rectangle_filled(self.center_x,self.center_y, 150, 150, arcade.color.BLACK)
        arcade.draw_rectangle_filled(self.center_x,self.center_y, 150-5, 150-5, arcade.color.BLUSH)
        
        k = 0
        if(len(self.tag)==2):
           k = 20 
         
        arcade.draw_text(str(self.tag), self.center_x-20-k, self.center_y-20, arcade.color.BLACK, 50)

class bb_em(arcade.SpriteSolidColor):
    def __init__(self,x,y,tag):
        self.posx = x 
        self.posy = y
        self.tag = tag

        super().__init__(150,150,arcade.color.BLACK)
        self.center_x = (150*self.posx)+75
        self.center_y = 600-75-(150*self.posy)
    
    def on_draw(self):
         pass
    #    arcade.draw_rectangle_filled(self.center_x,self.center_y, 150, 150, arcade.color.BLACK)
    #    arcade.draw_rectangle_filled(self.center_x,self.center_y, 150-5, 150-5, arcade.color.BLACK)    

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        # Set up your game here

        #self.collection = [] 

        self.collection = arcade.SpriteList()
        
        b1 = bb(0,0,tag='1') #arcade.SpriteSolidColor(150,150,arcade.color.GREEN)
        b2 = bb(1,0,tag='2')
        b3 = bb(2,0,tag='3')
        b4 = bb(3,0,tag='4')
        b5 = bb(0,1,tag='5')
        b6 = bb(1,1,tag='6')
        b7 = bb(2,1,tag='7')
        b8 = bb(3,1,tag='8')
        b9 = bb(0,2,tag='9')
        b10 = bb(1,2,tag='10')
        b11 = bb(2,2,tag='11')
        b12 = bb(3,2,tag='12')
        b13 = bb(0,3,tag='13')
        b14 = bb(1,3,tag='14')
        b15 = bb(2,3,tag='15')
        self.b_em = bb_em(3,3,tag='16') # empty 
        
        
        
        self.collection.append(b1)
        self.collection.append(b2)
        self.collection.append(b3)
        self.collection.append(b4)
        self.collection.append(b5)
        self.collection.append(b6)
        self.collection.append(b7)
        self.collection.append(b8)
        self.collection.append(b9)
        self.collection.append(b10)
        self.collection.append(b11)
        self.collection.append(b12)
        self.collection.append(b13)
        self.collection.append(b14)
        self.collection.append(b15)
        self.collection.append(self.b_em)

        m = [i+1 for i in range(15)]
        #random.shuffle(m)  # ********************** ส่วนที่ให้แผ่นกระเบื้องสลับ ###

        m[-1],m[-5] = m[-5],m[-1] #ส่วนที่ทดลองปรับ
        m[-4],m[-5] = m[-5],m[-4]
        
        
        for i in range(len(m)):
          #print(m[i])
          self.collection[i].tag = str(m[i])  


        ### shuffle
        #random.shuffle(self.collection) 


        self.cursor = arcade.Sprite('cursor.png')
        self.cursor.scale = 0.2

     
        self.onpress = False
        self.onrelease_key = False
        self.onpressTag = '0'
        self.onrelease = '0'

      
        self.playtime = 180
        self.gamestart = 3

        
        self.audio = arcade.load_sound('MouseClickSoundEffect.wav',False)
    

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()

        
        # Your drawing code goes here
        
        for i in self.collection:
          #i.draw()
          i.on_draw()
        #self.collection.draw()
        
        
        if(self.gamestart>=0):
          arcade.draw_text(int(round(self.gamestart,0)),260, 300, arcade.color.WHITE, 120)
        arcade.draw_text("time:"+str(int(round(self.playtime,0))),15, 580, arcade.color.WHITE, 14)
        

        #for i in range(3):
        #  arcade.draw_rectangle_filled((150*i)+75,600-75, 150, 150, arcade.color.BLACK)
        #  arcade.draw_rectangle_filled((150*i)+75,600-75, 150-5, 150-5, arcade.color.BLUSH)
        #  arcade.draw_text(str(i+1), (150*i)+75-20, 600-75-20, arcade.color.BLACK, 50)

        #arcade.draw_rectangle_filled(150+75,600-75, 150, 150, arcade.color.BLACK)
        #arcade.draw_rectangle_filled(150+75,600-75, 150-5, 150-5, arcade.color.BLUSH)
        #arcade.draw_text("2", 150+75-20, 600-75-20, arcade.color.BLACK, 50)

        self.cursor.draw()


    def update(self, delta_time):
        pass 
    
        #self.cursor.update()
        
        if(self.gamestart>=0):
           self.gamestart = self.gamestart - delta_time 

        if(self.gamestart<=0):
           self.playtime = self.playtime - delta_time
      

        """ All the logic to move, and the game logic goes here. """
        '''
        if(self.onpress==True):
           self.onpress = False
           hit_list = arcade.check_for_collision_with_list(self.cursor,self.collection)
           for i in hit_list:
             print(i.tag)
             self.onpressTag = i.tag  
             break
             #for i in self.collection: 
             #if(arcade.check_for_collision(i,self.cursor)):
             #   #print(i.tag)
             #   self.onpressTag = i.tag
             #   break 
        '''
        
        '''
        if(self.onrelease_key==True):
            self.onrelease_key = False
            if(arcade.check_for_collision(self.b_em,self.cursor)):
                #print(self.b_em.tag)
                #print('can move')
                for i in self.collection:
                    if(i.tag == self.onpressTag):
                      #print('move',i.tag,self.b_em.tag)
                      #print(i.center_x,i.center_y)
                      #print(self.b_em.center_x,self.b_em.center_y)
                      self.b_em.center_x,i.center_x = i.center_x,self.b_em.center_x
                      self.b_em.center_y,i.center_y = i.center_y,self.b_em.center_y
                      #print(i.center_x,i.center_y)
                      #print(self.b_em.center_x,self.b_em.center_y)
                      break    

                else:
                    print('can not move')
        '''
    
         

    def on_mouse_press(self, x, y, button, key_modifiers):
        """ Called when the user presses a mouse button. """

        if(self.gamestart>=0):
            return 0 
        
        #print('press')
        self.onpress = True
        self.cursor.center_x = x 
        self.cursor.center_y = y

        # Get list of cards we've clicked on
        cards = arcade.get_sprites_at_point((x, y), self.collection)
        #print(cards[-1].tag)
        self.onpressTag = cards[-1].tag
        

    def on_mouse_release(self, x: float, y: float, button: int,
                         modifiers: int):
        
        if(self.gamestart>=0):
            return 0 
        
        self.onrelease_key = True 
        self.cursor.center_x = x 
        self.cursor.center_y = y
        """ Called when the user presses a mouse button. """
        
        cards = arcade.get_sprites_at_point((x, y), self.collection)
        if(cards[-1].tag=='16'):
          
          for i in self.collection:
              if(i.tag == self.onpressTag):

                print('*',i.tag,self.b_em.tag)
                #print(self.b_em.center_x,i.center_x)

                print('***',self.b_em.center_x-i.center_x,self.b_em.center_y-i.center_y)
                px = abs(self.b_em.center_x-i.center_x)
                py = abs(self.b_em.center_y-i.center_y)
                if((px==0 or px==150) and (py==0 or py==150) and (px+py==150)):
                  self.b_em.center_x,i.center_x = i.center_x,self.b_em.center_x
                  self.b_em.center_y,i.center_y = i.center_y,self.b_em.center_y
                  arcade.play_sound(self.audio,1.0,-1,False)
                  break
        
        m = 1
        score = 0
        for i in range(4):
          for j in range(4):
            kx,ky = self.getPosColl(i,j)
            rx,ry = self.getPosReal(str(m))  
            if(kx==rx and ky==ry):
               score+=1 

            m+=1
        
        #print('*',score) 
        if(score==16):
           game_view = youwin(int(round(self.playtime,0)))
           self.window.show_view(game_view) 

        if(self.playtime<0):
           game_view = youlose()
           self.window.show_view(game_view)   
              

    def getPosReal(self,m):
        for i in range(16):
          if(self.collection[i].tag==m):
            return  self.collection[i].center_x,self.collection[i].center_y 
                    
    def getPosColl(self,i,j):
        kx = int((150*j)+75)
        ky = int(600-75-(150*i))
        return kx,ky 
    
    def on_mouse_motion(self, x: float, y: float, dx: float, dy: float):
        """ User moves mouse """
        self.cursor.center_x = x 
        self.cursor.center_y = y
        pass


class title(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.BLACK)
        self.setup()

    def setup(self):
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        arcade.draw_text('Press Spacebar to start game', 180, 300, arcade.color.WHITE, 15)
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass   

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.SPACE:
            game_view = MyGame()
            self.window.show_view(game_view)

class youwin(arcade.View):
    """ Main application class. """

    def __init__(self,score_user=0):
        super().__init__()
        self.score_user = 180 - score_user
        arcade.set_background_color(arcade.color.BLACK)
        self.setup()

    def setup(self):
        pass

        df = pd.read_csv('score.csv')
        df = df[['score']]

        p1 = pd.DataFrame({'score':[self.score_user]})
        df = pd.concat([df,p1],ignore_index=True)
        df.to_csv('score.csv')

        k = df['score'].min()
        self.scoremax = int(k)

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        arcade.draw_text('You Win!!', 180, 400, arcade.color.WHITE, 45)

        arcade.draw_text('Your Score : '+str(self.score_user) + ' Seconds', 180, 300, arcade.color.WHITE, 20)
        arcade.draw_text('High Score : '+str(self.scoremax) + ' Seconds', 180, 250, arcade.color.WHITE, 20)

        arcade.draw_text('Press Spacebar to restart game', 180, 200, arcade.color.WHITE, 15)
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass   

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.SPACE:
            game_view = MyGame()
            self.window.show_view(game_view)


class youlose(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.BLACK)
        self.setup()

    def setup(self):
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        arcade.draw_text('You Lose!!', 180, 400, arcade.color.WHITE, 45)
        arcade.draw_text('Press Spacebar to restart game', 180, 300, arcade.color.WHITE, 15)
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass   

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.SPACE:
            game_view = MyGame()
            self.window.show_view(game_view)

def main():
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = title()
    window.set_mouse_visible(False) #hide mouse
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()

