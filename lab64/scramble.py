import arcade

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600


class bb(arcade.SpriteSolidColor):
    def __init__(self,x,y,tag):
        self.posx = x 
        self.posy = y
        self.tag = tag

        super().__init__(150,150,arcade.color.GREEN)
        self.center_x = (150*self.posx)+75
        self.center_y = 600-75-(150*self.posy)
    
    def on_draw2(self):
        arcade.draw_rectangle_filled((150*self.posx)+75,600-75-(150*self.posy), 150, 150, arcade.color.BLACK)
        arcade.draw_rectangle_filled((150*self.posx)+75,600-75-(150*self.posy), 150-5, 150-5, arcade.color.BLUSH)
        
        k = 0
        if(len(self.tag)==2):
           k = 20 
         
        arcade.draw_text(str(self.tag), (150*self.posx)+75-20-k, 600-75-20-(150*self.posy), arcade.color.BLACK, 50)

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here

        self.collection = [] 
        
        b1 = bb(0,0,tag='1') #arcade.SpriteSolidColor(150,150,arcade.color.GREEN)
        b2 = bb(1,0,tag='2')
        b3 = bb(2,0,tag='3')
        b4 = bb(3,0,tag='4')
        b5 = bb(0,1,tag='5')
        b6 = bb(1,1,tag='6')
        b7 = bb(2,1,tag='7')
        b8 = bb(3,1,tag='8')
        b9 = bb(0,2,tag='9')
        b10 = bb(1,2,tag='10')
        b11 = bb(2,2,tag='11')
        b12 = bb(3,2,tag='12')
        b13 = bb(0,3,tag='13')
        b14 = bb(1,3,tag='14')
        b15 = bb(2,3,tag='15')
        

        self.collection.append(b1)
        self.collection.append(b2)
        self.collection.append(b3)
        self.collection.append(b4)
        self.collection.append(b5)
        self.collection.append(b6)
        self.collection.append(b7)
        self.collection.append(b8)
        self.collection.append(b9)
        self.collection.append(b10)
        self.collection.append(b11)
        self.collection.append(b12)
        self.collection.append(b13)
        self.collection.append(b14)
        self.collection.append(b15)
     



    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here
        
        for i in self.collection:
          i.draw()
          i.on_draw2()

        

        #for i in range(3):
        #  arcade.draw_rectangle_filled((150*i)+75,600-75, 150, 150, arcade.color.BLACK)
        #  arcade.draw_rectangle_filled((150*i)+75,600-75, 150-5, 150-5, arcade.color.BLUSH)
        #  arcade.draw_text(str(i+1), (150*i)+75-20, 600-75-20, arcade.color.BLACK, 50)

        #arcade.draw_rectangle_filled(150+75,600-75, 150, 150, arcade.color.BLACK)
        #arcade.draw_rectangle_filled(150+75,600-75, 150-5, 150-5, arcade.color.BLUSH)
        #arcade.draw_text("2", 150+75-20, 600-75-20, arcade.color.BLACK, 50)



    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass


def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
