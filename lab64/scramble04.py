import arcade

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
       

class bb(arcade.SpriteSolidColor):
    def __init__(self,x,y,tag):
        self.posx = x 
        self.posy = y
        self.tag = tag

        super().__init__(150,150,arcade.color.GREEN)
        self.center_x = (150*self.posx)+75
        self.center_y = 600-75-(150*self.posy)
    
    def on_draw(self):
        
        #arcade.draw_rectangle_filled((150*self.posx)+75,600-75-(150*self.posy), 150, 150, arcade.color.BLACK)
        #arcade.draw_rectangle_filled((150*self.posx)+75,600-75-(150*self.posy), 150-5, 150-5, arcade.color.BLUSH)

        arcade.draw_rectangle_filled(self.center_x,self.center_y, 150, 150, arcade.color.BLACK)
        arcade.draw_rectangle_filled(self.center_x,self.center_y, 150-5, 150-5, arcade.color.BLUSH)
        
        k = 0
        if(len(self.tag)==2):
           k = 20 
         
        arcade.draw_text(str(self.tag), self.center_x-20-k, self.center_y-20, arcade.color.BLACK, 50)

class bb_em(arcade.SpriteSolidColor):
    def __init__(self,x,y,tag):
        self.posx = x 
        self.posy = y
        self.tag = tag

        super().__init__(150,150,arcade.color.BLACK)
        self.center_x = (150*self.posx)+75
        self.center_y = 600-75-(150*self.posy)
    
    def on_draw(self):
         pass
    #    arcade.draw_rectangle_filled(self.center_x,self.center_y, 150, 150, arcade.color.BLACK)
    #    arcade.draw_rectangle_filled(self.center_x,self.center_y, 150-5, 150-5, arcade.color.BLACK)
        
         

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        self.set_mouse_visible(False) #hide mouse

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here

        #self.collection = [] 

        self.collection = arcade.SpriteList()
        
        b1 = bb(0,0,tag='1') #arcade.SpriteSolidColor(150,150,arcade.color.GREEN)
        b2 = bb(1,0,tag='2')
        b3 = bb(2,0,tag='3')
        b4 = bb(3,0,tag='4')
        b5 = bb(0,1,tag='5')
        b6 = bb(1,1,tag='6')
        b7 = bb(2,1,tag='7')
        b8 = bb(3,1,tag='8')
        b9 = bb(0,2,tag='9')
        b10 = bb(1,2,tag='10')
        b11 = bb(2,2,tag='11')
        b12 = bb(3,2,tag='12')
        b13 = bb(0,3,tag='13')
        b14 = bb(1,3,tag='14')
        b15 = bb(2,3,tag='15')
        self.b_em = bb_em(3,3,tag='16') # empty 
        

        self.collection.append(b1)
        self.collection.append(b2)
        self.collection.append(b3)
        self.collection.append(b4)
        self.collection.append(b5)
        self.collection.append(b6)
        self.collection.append(b7)
        self.collection.append(b8)
        self.collection.append(b9)
        self.collection.append(b10)
        self.collection.append(b11)
        self.collection.append(b12)
        self.collection.append(b13)
        self.collection.append(b14)
        self.collection.append(b15)
        self.collection.append(self.b_em)


        self.cursor = arcade.Sprite('cursor.png')
        self.cursor.scale = 0.2

     
        self.onpress = False
        self.onrelease_key = False
        self.onpressTag = '0'
        self.onrelease = '0'

    

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()

        
        # Your drawing code goes here
        
        for i in self.collection:
          #i.draw()
          i.on_draw()
        #self.collection.draw()
        
        

        #for i in range(3):
        #  arcade.draw_rectangle_filled((150*i)+75,600-75, 150, 150, arcade.color.BLACK)
        #  arcade.draw_rectangle_filled((150*i)+75,600-75, 150-5, 150-5, arcade.color.BLUSH)
        #  arcade.draw_text(str(i+1), (150*i)+75-20, 600-75-20, arcade.color.BLACK, 50)

        #arcade.draw_rectangle_filled(150+75,600-75, 150, 150, arcade.color.BLACK)
        #arcade.draw_rectangle_filled(150+75,600-75, 150-5, 150-5, arcade.color.BLUSH)
        #arcade.draw_text("2", 150+75-20, 600-75-20, arcade.color.BLACK, 50)

        self.cursor.draw()


    def update(self, delta_time):
        pass 
    
        #self.cursor.update()
        """ All the logic to move, and the game logic goes here. """
        '''
        if(self.onpress==True):
           self.onpress = False
           hit_list = arcade.check_for_collision_with_list(self.cursor,self.collection)
           for i in hit_list:
             print(i.tag)
             self.onpressTag = i.tag  
             break
             #for i in self.collection: 
             #if(arcade.check_for_collision(i,self.cursor)):
             #   #print(i.tag)
             #   self.onpressTag = i.tag
             #   break 
        '''
        
        '''
        if(self.onrelease_key==True):
            self.onrelease_key = False
            if(arcade.check_for_collision(self.b_em,self.cursor)):
                #print(self.b_em.tag)
                #print('can move')
                for i in self.collection:
                    if(i.tag == self.onpressTag):
                      #print('move',i.tag,self.b_em.tag)
                      #print(i.center_x,i.center_y)
                      #print(self.b_em.center_x,self.b_em.center_y)
                      self.b_em.center_x,i.center_x = i.center_x,self.b_em.center_x
                      self.b_em.center_y,i.center_y = i.center_y,self.b_em.center_y
                      #print(i.center_x,i.center_y)
                      #print(self.b_em.center_x,self.b_em.center_y)
                      break    

                else:
                    print('can not move')
        '''
    
     

    def on_mouse_press(self, x, y, button, key_modifiers):
        """ Called when the user presses a mouse button. """
        #print('press')
        self.onpress = True
        self.cursor.center_x = x 
        self.cursor.center_y = y

        # Get list of cards we've clicked on
        cards = arcade.get_sprites_at_point((x, y), self.collection)
        #print(cards[-1].tag)
        self.onpressTag = cards[-1].tag
        

    def on_mouse_release(self, x: float, y: float, button: int,
                         modifiers: int):
        self.onrelease_key = True 
        self.cursor.center_x = x 
        self.cursor.center_y = y
        """ Called when the user presses a mouse button. """
        
        cards = arcade.get_sprites_at_point((x, y), self.collection)
        if(cards[-1].tag=='16'):
          for i in self.collection:
              if(i.tag == self.onpressTag):
                self.b_em.center_x,i.center_x = i.center_x,self.b_em.center_x
                self.b_em.center_y,i.center_y = i.center_y,self.b_em.center_y

                break
        
     
    def on_mouse_motion(self, x: float, y: float, dx: float, dy: float):
        """ User moves mouse """
        self.cursor.center_x = x 
        self.cursor.center_y = y
        pass

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
