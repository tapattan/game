import arcade
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
MAX_SCREEN_WIDTH = 2400

from hero import hero

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()

        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        print(arcade.__version__)

        self.hero = hero()
        self.hero.center_x = 100
        self.hero.center_y = 400
        
        self.view_position_player = 0 #กล้อง

        ### zone load map ########
        # Name of map file to load
        map_name = "../resources/map/lab08/map01.json"

        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "ground": {
                "use_spatial_hash": True,
            },
            "cloud": {
                "use_spatial_hash": True,
            },
        }

        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)

        self.groundList = self.tile_map.sprite_lists["ground"]
        self.cloudList = self.tile_map.sprite_lists["cloud"]
        
        self.physics_engine = arcade.PhysicsEnginePlatformer(
            self.hero, self.groundList, gravity_constant=1
        )  

        pass


    def update(self, delta_time):
        self.physics_engine.update()

        self.hero.update()
        self.hero.update_animation()

        #ป้องกันตกโลก
        if(self.hero.center_x<=21):
            self.hero.center_x=21
        elif(self.hero.center_x>=MAX_SCREEN_WIDTH-21):
            self.hero.center_x = MAX_SCREEN_WIDTH-21
        

        #ส่วนของกล้องที่เลื่อนตาม
        self.view_position_player = self.hero.center_x - (SCREEN_WIDTH//2)
        print(self.view_position_player)
        self.view_left = self.view_position_player

        if(self.view_position_player<0):
            arcade.set_viewport(0,
                                0+SCREEN_WIDTH,
                                0,
                                SCREEN_HEIGHT)
            self.view_left = 0                  
        elif(self.view_position_player>MAX_SCREEN_WIDTH-SCREEN_WIDTH):  #6000-600
            arcade.set_viewport(MAX_SCREEN_WIDTH-SCREEN_WIDTH,
                                MAX_SCREEN_WIDTH,
                                0,
                                SCREEN_HEIGHT)
            self.view_left = MAX_SCREEN_WIDTH - SCREEN_WIDTH               
        else:
            arcade.set_viewport(self.view_position_player,
                                self.view_position_player+SCREEN_WIDTH,
                                0,
                                SCREEN_HEIGHT)
        #end ส่วนของกล่้องที่เลื่อนตาม

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()

        self.hero.draw()
        self.groundList.draw()
        self.cloudList.draw() 
        
        

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP or key == arcade.key.W:
            self.hero.change_y = 10
            self.hero.direct = 'UP'
        elif key == arcade.key.LEFT or key == arcade.key.A:
            self.hero.change_x = -5
            self.hero.textureList = self.hero.texturesLeft
            self.hero.direct = 'LEFT'
        elif key == arcade.key.RIGHT or key == arcade.key.D:
            self.hero.change_x = 5
            self.hero.textureList = self.hero.texturesRight
            self.hero.direct = 'RIGHT'

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        if key == arcade.key.LEFT or key == arcade.key.RIGHT or key == arcade.key.A or key == arcade.key.D:
            self.hero.change_x = 0
        elif(key == arcade.key.W or key == arcade.key.UP):
            self.hero.change_y = 0

        self.hero.direct=''
 
 


def main():
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()