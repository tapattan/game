import arcade

class hero(arcade.Sprite):

    def __init__(self):

        # Set up parent class
        super().__init__()
        
 
        self.texturesDown = []
        self.texturesDown.append(arcade.load_texture("../resources/sokobanpack/Character4.png"))
        self.texturesDown.append(arcade.load_texture("../resources/sokobanpack/Character5.png"))
        self.texturesDown.append(arcade.load_texture("../resources/sokobanpack/Character6.png"))
        
        self.textureList = self.texturesDown 
        self.statusFrameaction = len(self.texturesDown)
        print(self.statusFrameaction)


        self.timeFrame = 0
        self.timeKFrame = 0
        self.texture = self.textureList[self.timeFrame]
        self.center_x = 100
        self.center_y = 300
        self.scale = 1

        self.texturesLeft = []
        self.texturesLeft.append(arcade.load_texture("../resources/sokobanpack/Character1.png"))
        self.texturesLeft.append(arcade.load_texture("../resources/sokobanpack/Character10.png"))
        
        self.texturesRight = []
        self.texturesRight.append(arcade.load_texture("../resources/sokobanpack/Character2.png"))
        self.texturesRight.append(arcade.load_texture("../resources/sokobanpack/Character3.png"))
        
        self.texturesUp = []
        self.texturesUp.append(arcade.load_texture("../resources/sokobanpack/Character7.png"))
        self.texturesUp.append(arcade.load_texture("../resources/sokobanpack/Character8.png"))
        self.texturesUp.append(arcade.load_texture("../resources/sokobanpack/Character9.png"))


        self.direct = ''
        

    def update_animation(self, delta_time: float = 1 / 20):
        self.statusFrameaction = len(self.textureList)
        self.timeKFrame +=(delta_time)
        if(self.timeKFrame>0.2):
            self.timeKFrame=0
            self.timeFrame+=1
            if(self.timeFrame%self.statusFrameaction==0):
                self.timeFrame = 0
            
            try:
              self.texture = self.textureList[self.timeFrame]
            except:
              pass