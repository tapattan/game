import arcade
SCREEN_WIDTH = 640
SCREEN_HEIGHT = 640

from hero import hero
 

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()

        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        print(arcade.__version__)
        self.hero = hero()
        

        ### zone load map ########
        # Name of map file to load
        map_name = "../resources/map/lab08/map01.json"

        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "ground": {
                "use_spatial_hash": True,
            },
            "cloud": {
                "use_spatial_hash": True,
            },
        }

        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)

        self.groundList = self.tile_map.sprite_lists["ground"]
        self.cloudList = self.tile_map.sprite_lists["cloud"]

        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.groundList.draw()
        self.cloudList.draw() 
        
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.hero.draw()
        self.hero.update()
        pass

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
         
   

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass


def main():
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()