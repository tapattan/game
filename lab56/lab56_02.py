import arcade
import random
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600


class heart(arcade.Sprite):
    def __init__(self):
        super().__init__('heart_s.png')
        #self.texture = arcade.load_texture()
        self.scale = 0.25
        self.timeshake = 0
        r = random.randint(0,1)
        if(r==0):
          self.angle = 30
        else:
          self.angle = -30       


class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        pass
        
        self.heartList = arcade.SpriteList()
        self.dbclick = 0
        self.clicktime = 0
        self.posx = 0
        self.posy = 0

    def on_mouse_release(self, x: int, y: int, button: int, modifiers: int):
        self.dbclick+=1 
        self.posx = x 
        self.posy = y

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.heartList.draw()
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """

        #### check double click
        self.clicktime += delta_time
        if(self.clicktime>1):
           self.clicktime = 0 
           self.dbclick = 0  
        
        if(self.dbclick>1):
           hobj = heart() 
           hobj.center_x = self.posx
           hobj.center_y = self.posy
           self.heartList.append(hobj)
           self.dbclick = 0

        #### end check double click
        print(len(self.heartList))
        for i in self.heartList:
            i.timeshake += delta_time 
            if(i.timeshake>0.5):
               i.timeshake = 0 
               if(i.angle>0):
                 i.angle = 30*-1 
               else:
                 i.angle = 30  
        pass

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
