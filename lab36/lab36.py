import arcade
import random
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600


class MyGame(arcade.Window):
    def __init__(self, width, height):
        super().__init__(width, height)
        arcade.set_background_color(arcade.color.BABY_BLUE_EYES)

    def setup(self):
        # Set up your game here
        pass
        
        self.house = arcade.Sprite('house.png')
        self.house.center_x = 300
        self.house.center_y = 300
        self.house.scale = 0.5
        
        self.color = [arcade.color.CADET_GREY,arcade.color.ASH_GREY,
                     arcade.color.ASH_GREY,arcade.color.ASH_GREY,
                     arcade.color.ASH_GREY,arcade.color.ASH_GREY]
        self.smoke = arcade.SpriteList()
        for i in range(30):
         s1 = arcade.SpriteCircle(radius=random.randint(7,9),color=random.choice(self.color),soft=True)
         s1.center_x = random.randint(342,368)
         s1.center_y = random.randint(390,400)
         s1.alpha = random.randint(10,255)
         s1.change_x = 1
         s1.change_y = 1
         self.smoke.append(s1)

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        
        self.house.draw()
        self.smoke.draw()
       
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.house.update()
        self.smoke.update()

        for i in self.smoke:
            if(i.alpha>0):
               i.alpha -= 1
            if(i.alpha<=0):
               i.alpha = 0
            print(i.alpha)
            if(i.center_x > random.randint(400,600)):
               i.center_x = random.randint(342,368)
               i.center_y = random.randint(390,400)
               i.alpha = random.randint(10,255)
        pass
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        pass
         
      

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass 
def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()