import arcade
import random 

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)
    
    def setup(self):
        # Set up your game here
        self.home = arcade.Sprite('house.png',0.3) #SCALING = 0.3 
        self.home.center_x = 300 
        self.home.center_y = 300

        self.firewood = arcade.Sprite('firewood.png',0.05) #SCALING = 0.3 
        self.firewood.center_x = 150
        self.firewood.center_y = 150
        
        
        self.smokeList = [] 
        self.colorsmoke = [arcade.color.ASH_GREY, arcade.color.BATTLESHIP_GREY,arcade.color.CHARCOAL]

        for i in range(20):
          k = random.randint(2,5)/10
          color_p = self.colorsmoke[random.randint(0,2)]
          c1 = arcade.SpriteCircle(10,color_p,soft=True)
          c1.center_x = random.randint(320,340)
          c1.center_y = random.randint(355,365)
          c1.scale = k
         
          self.smokeList.append(c1) 

        self.fireList = [] 
        self.colorfire = [arcade.color.RED, arcade.color.BURNT_ORANGE,arcade.color.RED,arcade.color.AMERICAN_ROSE,arcade.color.AMBER]

        for i in range(20):
          l = random.randint(2,5)/10
          color_o = self.colorfire[random.randint(0,4)]
          c2 = arcade.SpriteCircle(10,color_o,soft=True)
          c2.center_x = random.randint(141,160)
          c2.center_y = random.randint(120,135)
          c2.scale = l
         
          self.fireList.append(c2) 


        self.fireList2 = [] 
        self.colorfire2 = [arcade.color.RED, arcade.color.BURNT_ORANGE,arcade.color.RED,arcade.color.AMERICAN_ROSE,arcade.color.AMBER]

        for i in range(20):
          l2 = random.randint(2,5)/10
          color_o2 = self.colorfire2[random.randint(0,4)]
          c22 = arcade.SpriteCircle(10,color_o2,soft=True)
          c22.center_x = random.randint(100,120)
          c22.center_y = random.randint(120,135)
          c22.scale = l
         
          self.fireList2.append(c22) 


        self.fireList3 = [] 
        self.colorfire3 = [arcade.color.RED, arcade.color.BURNT_ORANGE,arcade.color.RED,arcade.color.AMERICAN_ROSE,arcade.color.AMBER]

        for i in range(20):
          l3 = random.randint(2,5)/10
          color_o3 = self.colorfire3[random.randint(0,4)]
          c3 = arcade.SpriteCircle(10,color_o3,soft=True)
          c3.center_x = random.randint(161,170)
          c3.center_y = random.randint(120,135)
          c3.scale = l
         
          self.fireList3.append(c3) 



        ####### ส่วนของหิมะ ########
        self.snowList = [] 
        for i in range(50):
          snow01 = arcade.SpriteCircle(10,arcade.color.ALICE_BLUE,soft=True)
          snow01.center_x = random.randint(20,720)
          snow01.center_y = random.randint(600,1200)
          snow01.scale = 1
          snow01.change_y = -2
          snow01.change_x = -.5

          self.snowList.append(snow01)

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()

        # Your drawing code goes here
        self.home.draw()
        self.firewood.draw()
        

        for i in self.smokeList:
          i.draw()

        for i in self.fireList:
          i.draw()

        for i in self.fireList2:
          i.draw()

        for i in self.fireList3:
          i.draw()

        

        for i in self.snowList:
          i.draw()
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        

        for i in self.snowList:
          i.update()
          if(i.center_y < -100):
              i.center_x = random.randint(20,720)
              i.center_y = random.randint(600,900)


        for i in self.smokeList:
          i.center_y = i.center_y+ random.randint(10,30)/20
          i.center_x = i.center_x+ random.randint(10,30)/15
          b = random.randint(500,600)
          if(i.center_y > b):
            i.center_x = random.randint(330,350)
            i.center_y = random.randint(355,365)
            i.scale = random.randint(5,15)/10

        

        for i in self.fireList:
          i.center_y = i.center_y+ random.randint(10,30)/20
          a = 210
          if(i.center_y > a):
            i.center_x = random.randint(141,160)
            i.center_y = random.randint(130,170)
            i.scale = random.randint(2,10)/10

        for i in self.fireList2:
          i.center_y = i.center_y+ random.randint(10,30)/20
          a2 = 170
          if(i.center_y > a2):
            i.center_x = random.randint(130,140)
            i.center_y = random.randint(130,131)
            i.scale = random.randint(2,10)/10

        for i in self.fireList3:
          i.center_y = i.center_y+ random.randint(10,30)/20
          a3 = 170
          if(i.center_y > a3):
            i.center_x = random.randint(161,170)
            i.center_y = random.randint(130,131)
            i.scale = random.randint(2,10)/10


        pass


def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
