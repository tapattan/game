import arcade
import random

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_TITLE = "Intense Fire Effect Example"

class IntenseFireEffect(arcade.Window):
    def __init__(self, width, height, title):
        super().__init__(width, height, title)
        arcade.set_background_color(arcade.color.BLACK)
        self.fire_particles = []

    def setup(self):
        for i in range(100):  # เพิ่มจำนวนเปลวไฟเป็น 100
            # ตำแหน่งเริ่มต้นของเปลวไฟ
            x = SCREEN_WIDTH // 2
            y = 50  # ปรับให้เปลวไฟเริ่มต้นใกล้กับพื้นมากขึ้น
            # สีของเปลวไฟ
            color = random.choice([arcade.color.RED, arcade.color.ALIZARIN_CRIMSON, arcade.color.ORANGE, arcade.color.RED_ORANGE])
            # ความเร็วในแนว X และ Y
            dx = random.uniform(-2.5, 2.5)
            dy = random.uniform(4, 8)
            # อายุของเปลวไฟ
            lifetime = random.uniform(1, 2)
            # ขนาดของเปลวไฟ
            size = random.uniform(4, 8)
            self.fire_particles.append([x, y, dx, dy, color, lifetime, size])

    def on_draw(self):
        arcade.start_render()
        for particle in self.fire_particles:
            x, y, _, _, color, _, size = particle
            arcade.draw_circle_filled(x, y, size, color)  # ใช้ขนาดที่แปรผัน

    def update(self, delta_time):
        for particle in self.fire_particles:
            # อัพเดทตำแหน่ง
            particle[0] += particle[2]  # x += dx
            particle[1] += particle[3]  # y += dy
            # ลดอายุของเปลวไฟ
            particle[5] -= delta_time
            # หากเปลวไฟหมดอายุ กำหนดค่าใหม่
            if particle[5] <= 0:
                particle[0] = SCREEN_WIDTH // 2
                particle[1] = 50  # ตำแหน่งเริ่มต้นใหม่ใกล้กับพื้น
                particle[2] = random.uniform(-2.5, 2.5)
                particle[3] = random.uniform(4, 8)
                particle[5] = random.uniform(1, 2)
                particle[6] = random.uniform(4, 8)  # ขนาดใหม่

def main():
    window = IntenseFireEffect(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    window.setup()
    arcade.run()

if __name__ == "__main__":
    main()