import arcade
import random

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
SCREEN_TITLE = "Fire Effect Example"

class FireEffect(arcade.Window):
    def __init__(self, width, height, title):
        super().__init__(width, height, title)
        arcade.set_background_color(arcade.color.BLACK)
        self.fire_particles = []

    def setup(self):
        for i in range(50):
            # ตำแหน่งเริ่มต้นของเปลวไฟ
            x = SCREEN_WIDTH // 2
            y = 100
            # สีของเปลวไฟ
            color = random.choice([arcade.color.RED, arcade.color.YELLOW, arcade.color.ORANGE])
            # ความเร็วในแนว X และ Y
            dx = random.uniform(-0.5, 0.5)
            dy = random.uniform(2, 4)
            # อายุของเปลวไฟ
            lifetime = random.uniform(0.5, 1.5)
            self.fire_particles.append([x, y, dx, dy, color, lifetime])

    def on_draw(self):
        arcade.start_render()
        for particle in self.fire_particles:
            x, y, _, _, color, _ = particle
            arcade.draw_circle_filled(x, y, 5, color)

    def update(self, delta_time):
        for particle in self.fire_particles:
            # อัพเดทตำแหน่ง
            particle[0] += particle[2]  # x += dx
            particle[1] += particle[3]  # y += dy
            # ลดอายุของเปลวไฟ
            particle[5] -= delta_time
            # หากเปลวไฟหมดอายุ กำหนดค่าใหม่
            if particle[5] <= 0:
                particle[0] = SCREEN_WIDTH // 2
                particle[1] = 100
                particle[5] = random.uniform(0.5, 1.5)

def main():
    window = FireEffect(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    window.setup()
    arcade.run()

if __name__ == "__main__":
    main()