import arcade
import random

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_TITLE = "Intense Fire Effect with Logs"

class IntenseFireWithLogs(arcade.Window):
    def __init__(self, width, height, title):
        super().__init__(width, height, title)
        arcade.set_background_color(arcade.color.BLACK)
        self.fire_particles = []

    def setup(self):
        for i in range(150):  # เพิ่มจำนวนเปลวไฟเพื่อให้ไฟดูแรงขึ้น
            # ตำแหน่งเริ่มต้นของเปลวไฟ
            x = random.uniform(SCREEN_WIDTH // 2 - 50, SCREEN_WIDTH // 2 + 50)
            y = 60  # ปรับให้เปลวไฟเริ่มต้นใกล้กับพื้นมากขึ้น
            # สีของเปลวไฟ
            color = random.choice([arcade.color.RED, arcade.color.YELLOW, arcade.color.ORANGE, arcade.color.RED_ORANGE, arcade.color.DARK_ORANGE])
            # ความเร็วในแนว X และ Y
            dx = random.uniform(-3, 3)
            dy = random.uniform(5, 10)
            # อายุของเปลวไฟ
            lifetime = random.uniform(1.5, 3)
            # ขนาดของเปลวไฟ
            size = random.uniform(5, 10)
            self.fire_particles.append([x, y, dx, dy, color, lifetime, size])

    def on_draw(self):
        arcade.start_render()
        # วาดไม้ฟืน
        for x_offset in range(-60, 80, 20):
            arcade.draw_rectangle_filled(SCREEN_WIDTH // 2 + x_offset, 30, 20, 60, arcade.color.DARK_BROWN)
        # วาดเปลวไฟ
        for particle in self.fire_particles:
            x, y, _, _, color, _, size = particle
            arcade.draw_circle_filled(x, y, size, color)  # ใช้ขนาดที่แปรผัน

    def update(self, delta_time):
        for particle in self.fire_particles:
            # อัพเดทตำแหน่ง
            particle[0] += particle[2]  # x += dx
            particle[1] += particle[3]  # y += dy
            # ลดอายุของเปลวไฟ
            particle[5] -= delta_time
            # หากเปลวไฟหมดอายุ กำหนดค่าใหม่
            if particle[5] <= 0:
                particle[0] = random.uniform(SCREEN_WIDTH // 2 - 50, SCREEN_WIDTH // 2 + 50)
                particle[1] = 60
                particle[2] = random.uniform(-3, 3)
                particle[3] = random.uniform(5, 10)
                particle[5] = random.uniform(1.5, 3)
                particle[6] = random.uniform(5, 10)  # ขนาดใหม่

def main():
    window = IntenseFireWithLogs(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    window.setup()
    arcade.run()

if __name__ == "__main__":
    main()