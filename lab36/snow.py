import arcade
import random 
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        pass
        

        self.snow_list = []  # สร้าง list ของหิมะ
        num_snow = 600 #กำหนดให้จะมี หิมะกี่ก้อน
        
        for i in range(num_snow):
          c1 = arcade.SpriteCircle(10,arcade.color.SNOW,True) # สร้างวงกลมแบบ Sprite  , ถ้ามี True ด้านหลัง จะนวลๆ สีแบบไล่เฉด
          c1.center_x = random.randint(10,1200)
          c1.center_y = random.randint(620,1200) 
          c1.change_y = random.randint(1,3)*-1 #ให้มันเลื่อน y ลงมาทีละ 1

          self.snow_list.append(c1) # เอาหิมะ แต่ละก้อน ยัดใส่ list เอาไว้

          c1.ground = 'no'

          self.timegame = 0



    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
         
        for i in self.snow_list: #วนลูปใน list ของตัวที่เก็บหิมะ
            i.draw() #วาดหิมะ แต่ละตัวออกไป


        #แสดงว่าส่วนนี้เป็นแค่ รูป 
        #arcade.draw_circle_filled(300, 300, 100, arcade.color.RED) 

        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        
        for i in self.snow_list:
            i.update() 

            if(i.ground == 'no'):
              i.center_x = i.center_x + random.randint(-2,0) #-3,-2,-1,0,1,2,3

            if(i.center_x < -100 and i.center_y <-100): # ถ้าเทอหลุดออกนอกฉากไปแล้ว ชั้นจะให้กลับมาด้านบนใหม่อีกครั้ง
                i.center_x = random.randint(10,1200)
                i.center_y = random.randint(620,1200) 


            if(i.center_y < 0 and i.center_x>0 and i.center_x<600 and i.ground=='no'):
               i.change_y = 0 
               i.ground = 'yes'
    
               for _ in self.snow_list:
                  if(arcade.check_for_collision(i,_)):
                    i.center_y = _.center_y + 5
                    break 
                
 

               k = random.randint(0,100) #
               if(k>70):
                 i.ground = 'no'
                 i.center_x = random.randint(10,1200)
                 i.center_y = random.randint(620,1200) 

        
        # โอกาสละลายทุกๆ สองวิ มี 5% 
        self.timegame+=delta_time
        if(self.timegame>1): #หิมะ ละลาย 
           self.timegame = 0
           for i in self.snow_list:
              if(i.ground=='yes'):
                 m = random.randint(0,100)
                 if(m>95):
                   i.ground='no'
                   i.center_x = random.randint(10,1200)
                   i.center_y = random.randint(620,1200) 


def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
