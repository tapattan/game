import arcade
import random 
import arcade.gui

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

class Setting(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        self.ShowPlayer1 = arcade.Sprite('X.png')
        self.ShowPlayer1.center_x = 300 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.ShowPlayer1.center_y = 400 
        self.ShowPlayer1.scale = 0.3

        self.ShowPlayer2 = arcade.Sprite('O.png')
        self.ShowPlayer2.center_x = 500 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.ShowPlayer2.center_y = 400 
        self.ShowPlayer2.scale = 0.3

        self.border = arcade.Sprite('border.png')
        self.border.center_x = 300 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.border.center_y = 400 
        self.border.scale = 0.3 

        self.direct = 'left'

        self.robot = arcade.Sprite('robot.png')
        self.robot.center_x = 650
        self.robot.center_y = 200
        self.robot.scale = 0.8



       

    def on_draw(self):
        arcade.start_render()

        self.ShowPlayer1.draw()
        self.ShowPlayer2.draw()
        self.border.draw()
        self.robot.draw()

        arcade.draw_text('Pls Select X or O Using the keys A/D', 200, 500, arcade.color.WHITE, 20)
        arcade.draw_text('Enter to Start the game', 280, 200, arcade.color.WHITE, 20)

    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.A:
           self.direct = 'left'  
        elif key == arcade.key.D:
           self.direct = 'right'  

        if key == arcade.key.ENTER:
           uchoose = 'X' 
           if(self.direct=='left'):
              uchoose = 'X'
           elif(self.direct=='right'):
              uchoose = 'O' 

           game_view = MyGame(uchoose)
           self.window.show_view(game_view)
 

             
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        if(self.direct=='left'):
          self.border.center_x = 300  
        

        if(self.direct == 'right'):
          self.border.center_x = 500  
           


class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self,uchoose):
        super().__init__()
        self.turn = uchoose #ตัวละครที่เลือกมาจากด่านแรก
        if(uchoose=='X'):
           self.ai_play = 'O' 
        else:
           self.ai_play = 'X'    

        

        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        
        self.table = arcade.Sprite('table.png')
        self.table.center_x = 300 
        self.table.center_y = 300 

        self.POS1 = arcade.Sprite('X.png')
        self.POS1.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS1.center_y = -1000 
        self.POS1.scale = 0.6

        self.POS2 = arcade.Sprite('O.png')
        self.POS2.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS2.center_y = -1000 
        self.POS2.scale = 0.6

        self.POS3 = arcade.Sprite('O.png')
        self.POS3.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS3.center_y = -1000 
        self.POS3.scale = 0.6

        self.POS4 = arcade.Sprite('O.png')
        self.POS4.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS4.center_y = -1000 
        self.POS4.scale = 0.6

        self.POS5 = arcade.Sprite('O.png')
        self.POS5.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS5.center_y = -1000 
        self.POS5.scale = 0.6

        self.POS6 = arcade.Sprite('O.png')
        self.POS6.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS6.center_y = -1000 
        self.POS6.scale = 0.6

        self.POS7 = arcade.Sprite('O.png')
        self.POS7.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS7.center_y = -1000 
        self.POS7.scale = 0.6

        self.POS8 = arcade.Sprite('O.png')
        self.POS8.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS8.center_y = -1000 
        self.POS8.scale = 0.6

        self.POS9 = arcade.Sprite('O.png')
        self.POS9.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS9.center_y = -1000 
        self.POS9.scale = 0.6

        self.textureX = arcade.load_texture('X.png')
        self.textureO = arcade.load_texture('O.png')

        #self.turn = 'X'

        self.histplay = ['','','','','','','','',''] #เอาไว้เก็บประวัติการเล่นของผู้เล่น

        self.ShowPlayer1 = arcade.Sprite('X.png')
        self.ShowPlayer1.center_x = 700 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.ShowPlayer1.center_y = 500 
        self.ShowPlayer1.scale = 0.3

        self.ShowPlayer2 = arcade.Sprite('O.png')
        self.ShowPlayer2.center_x = 700 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.ShowPlayer2.center_y = 400 
        self.ShowPlayer2.scale = 0.3

        self.border = arcade.Sprite('border.png')
        self.border.center_x = 700 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.border.center_y = 500 
        self.border.scale = 0.3


        self.player_action = True
        self.timeai = 0

        if(self.turn=='X'):
           self.border.center_x = 700 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
           self.border.center_y = 500   
        elif(self.turn=='O'):
           self.border.center_x = 700 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
           self.border.center_y = 400  

        self.robot = arcade.Sprite('robot.png')
        if(self.ai_play=='O'):
          self.robot.center_x = 620
          self.robot.center_y = 400
        else:
          self.robot.center_x = 620
          self.robot.center_y = 500

        self.robot.scale = 0.25


        ###### zone dialog ########
        # Create and enable the UIManager
        self.manager = arcade.gui.UIManager()
        self.manager.enable()

        ######### call UIMessageBox #######
        self.message_boxWin = arcade.gui.UIMessageBox(
        width=300,
        height=200,
        message_text=(
            "You Win\n Press Close to new Game"
        ),
        callback=self.on_message_box_close,
        buttons=['Close']
        )

        self.message_boxLose = arcade.gui.UIMessageBox(
        width=300,
        height=200,
        message_text=(
            "You Lose!! \n Press Close to new Game"
        ),
        callback=self.on_message_box_close,
        buttons=['Close']
        )

        self.message_boxTie = arcade.gui.UIMessageBox(
        width=300,
        height=200,
        message_text=(
            "Tied Game!! \n Press Close to new Game"
        ),
        callback=self.on_message_box_close,
        buttons=['Close']
        )

        
        
        ###################


        self.endGame = False 

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.table.draw()
        self.POS1.draw()
        self.POS2.draw()
        self.POS3.draw()
        self.POS4.draw()
        self.POS5.draw()
        self.POS6.draw()
        self.POS7.draw()
        self.POS8.draw()
        self.POS9.draw()

        self.ShowPlayer1.draw()
        self.ShowPlayer2.draw()
        self.border.draw()
        self.robot.draw()
        self.manager.draw()
        pass
    
    def checkEndGame(self):
        p = 0
        for i in self.histplay:
          if(i==''):
            p+=1 
        if(p==0): #แสดงว่าไม่มีช่องว่างเหลือเลย
           return False 
        else:
           return True 

    def checkWin(self):
        casewin = [[1,2,3],[4,5,6],[7,8,9],[1,4,7],[2,5,8],[3,6,9],[7,5,3],[1,5,9]]
        for i in casewin:
          cnt = 0  
          for j in range(len(self.histplay)):
            if(self.histplay[j]=='X'):
               if(j+1 in i):
                cnt+=1 
          if cnt == 3:
            return 'XWin'

        for i in casewin:
          cnt = 0  
          for j in range(len(self.histplay)):
            if(self.histplay[j]=='O'):
               if(j+1 in i):
                cnt+=1 
          if cnt == 3:
            return 'OWin'   

        return '0' 
             

           
        
    def on_message_box_close(self, button_text):
        print(f"User pressed {button_text}.")
        game_view = Setting()
        self.window.show_view(game_view)

    
    def checkEndGameOver(self):
        if(self.checkWin()=='XWin'):
              if(self.ai_play=='X'):
                self.manager.add(self.message_boxLose)
              else:
                self.manager.add(self.message_boxWin)  

              self.endGame = True 
              return 0

        elif(self.checkWin()=='OWin'):
              if(self.ai_play=='O'):
                self.manager.add(self.message_boxLose)
              else:
                self.manager.add(self.message_boxWin) 
              
              self.endGame = True 
              return 0
        else:
             if(self.checkEndGame()==False):
                ########## end call UIMessageBox #######
                self.manager.add(self.message_boxTie)
                self.endGame = True 
                return 0 #แสดงว่ามีการลงครบ 9 ช่องแล้ว AI ไม่ต้องคิดต่อด้านล่าง 


    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        
        self.checkEndGameOver()

        if(self.player_action == False and self.endGame==False): 
          self.timeai = self.timeai + delta_time 
          if(self.timeai > 1): #ถ้าผ่านไปแล้ว 1 วินาที
           
           
              

           while(1):
            self.timeai = 0
            r = random.randint(0,8)  
            if(self.histplay[r] == ''): 
               self.histplay[r] = self.ai_play
               
               if(r == 0):
                 self.POS1.center_x = 100 
                 self.POS1.center_y = 100 
                 if(self.ai_play=='O'):
                   self.turn = 'X'
                   self.POS1.texture = self.textureO
                 else:
                   self.turn = 'O'
                   self.POS1.texture = self.textureX
               elif(r == 1):
                 self.POS2.center_x = 300 - 8
                 self.POS2.center_y = 100  
                 if(self.ai_play=='O'):
                   self.turn = 'X'
                   self.POS2.texture = self.textureO
                 else:
                   self.turn = 'O'
                   self.POS2.texture = self.textureX
               elif(r == 2):
                 self.POS3.center_x = 500
                 self.POS3.center_y = 100
                 if(self.ai_play=='O'):
                   self.turn = 'X'
                   self.POS3.texture = self.textureO
                 else:
                   self.turn = 'O'
                   self.POS3.texture = self.textureX
               elif(r == 3):
                 self.POS4.center_x = 100
                 self.POS4.center_y = 300 
                 if(self.ai_play=='O'):
                   self.turn = 'X'
                   self.POS4.texture = self.textureO
                 else:
                   self.turn = 'O'
                   self.POS4.texture = self.textureX
               elif(r == 4):
                 self.POS5.center_x = 300
                 self.POS5.center_y = 300
                 if(self.ai_play=='O'):
                   self.turn = 'X'
                   self.POS5.texture = self.textureO
                 else:
                   self.turn = 'O'
                   self.POS5.texture = self.textureX
               elif(r == 5):
                 self.POS6.center_x = 500
                 self.POS6.center_y = 300
                 if(self.ai_play=='O'):
                   self.turn = 'X'
                   self.POS6.texture = self.textureO
                 else:
                   self.turn = 'O'
                   self.POS6.texture = self.textureX
               elif(r == 6):
                 self.POS7.center_x = 100
                 self.POS7.center_y = 500
                 if(self.ai_play=='O'):
                   self.turn = 'X'
                   self.POS7.texture = self.textureO
                 else:
                   self.turn = 'O'
                   self.POS7.texture = self.textureX
               elif(r == 7):
                 self.POS8.center_x = 300
                 self.POS8.center_y = 500 
                 if(self.ai_play=='O'):
                   self.turn = 'X'
                   self.POS8.texture = self.textureO
                 else:
                   self.turn = 'O'
                   self.POS8.texture = self.textureX
               elif(r == 8):
                 self.POS9.center_x = 500
                 self.POS9.center_y = 500   
                 if(self.ai_play=='O'):
                   self.turn = 'X'
                   self.POS9.texture = self.textureO
                 else:
                   self.turn = 'O'
                   self.POS9.texture = self.textureX             



               
               #ส่วนที่ให้ ตัวแสดงเลือก turn เด้งกลับไปเมื่อ AI ทำงานเสร็จ
               if(self.turn=='X'):
                 self.border.center_x = 700 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
                 self.border.center_y = 500   
               elif(self.turn=='O'):
                 self.border.center_x = 700 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
                 self.border.center_y = 400 
                   

               self.player_action = True
               break 
        
         


    # Creating function to check the mouse clicks
    def on_mouse_press(self, x, y, button, modifiers):

        if(self.endGame == True):
           return 0  

        if(x>0 and x<200 and y>0 and y<200): #ช่องแถวล่างซ้าย
           if(self.histplay[0]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม

             self.player_action = False

             self.POS1.center_x = 100 
             self.POS1.center_y = 100 
             if(self.turn=='X'):
               self.POS1.texture = self.textureX
               self.histplay[0] = 'X'
               self.turn = 'O'
             else:
               self.POS1.texture = self.textureO
               self.histplay[0] = 'O'
               self.turn = 'X'
             

        if(x>200 and x<400 and y>0 and y<200): #ช่องแถวล่างตรงกลาง
           if(self.histplay[1]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม 

             self.player_action = False

             self.POS2.center_x = 300 - 8
             self.POS2.center_y = 100 
             if(self.turn=='X'): 
               self.POS2.texture = self.textureX
               self.histplay[1] = 'X'
               self.turn = 'O'
             else:
               self.POS2.texture = self.textureO
               self.histplay[1] = 'O'
               self.turn = 'X'

        if(x>400 and x<600 and y>0 and y<200): #ช่องแถวล่างขวา
           if(self.histplay[2]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม  

             self.player_action = False

             self.POS3.center_x = 500
             self.POS3.center_y = 100 
             if(self.turn=='X'): 
               self.POS3.texture = self.textureX
               self.turn = 'O'
               self.histplay[2] = 'X'
             else:
               self.POS3.texture = self.textureO
               self.turn = 'X'
               self.histplay[2] = 'O'

        
        if(x>0 and x<200 and y>200 and y<400):   
           if(self.histplay[3]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม  
             
             self.player_action = False

             self.POS4.center_x = 100
             self.POS4.center_y = 300 
             if(self.turn=='X'): 
               self.POS4.texture = self.textureX
               self.turn = 'O'
               self.histplay[3] = 'X'
             else:
               self.POS4.texture = self.textureO
               self.turn = 'X'
               self.histplay[3] = 'O'
               
        
        if(x>200 and x<400 and y>200 and y<400): 
           if(self.histplay[4]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม   

             self.player_action = False

             self.POS5.center_x = 300
             self.POS5.center_y = 300 
             if(self.turn=='X'): 
               self.POS5.texture = self.textureX
               self.turn = 'O'
               self.histplay[4] = 'X'
             else:
               self.POS5.texture = self.textureO
               self.turn = 'X'
               self.histplay[4] = 'O'

        if(x>400 and x<600 and y>200 and y<400):  
           if(self.histplay[5]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม 

             self.player_action = False

             self.POS6.center_x = 500
             self.POS6.center_y = 300 
             if(self.turn=='X'): 
               self.POS6.texture = self.textureX
               self.turn = 'O'
               self.histplay[5] = 'X'
             else:
               self.POS6.texture = self.textureO
               self.turn = 'X'
               self.histplay[5] = 'O'

        if(x>0 and x<200 and y>400 and y<600): 
           if(self.histplay[6]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม   

             self.player_action = False

             self.POS7.center_x = 100
             self.POS7.center_y = 500 
             if(self.turn=='X'): 
               self.POS7.texture = self.textureX
               self.turn = 'O'
               self.histplay[6] = 'X'
             else:
               self.POS7.texture = self.textureO
               self.turn = 'X'  
               self.histplay[6] = 'O'  

        if(x>200 and x<400 and y>400 and y<600):  
           if(self.histplay[7]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม   
             
             self.player_action = False

             self.POS8.center_x = 300
             self.POS8.center_y = 500 
             if(self.turn=='X'): 
               self.POS8.texture = self.textureX
               self.turn = 'O'
               self.histplay[7] = 'X' 
             else:
               self.POS8.texture = self.textureO
               self.turn = 'X' 
               self.histplay[7] = 'O' 

        if(x>400 and x<600 and y>400 and y<600):  
           if(self.histplay[8]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม  

             self.player_action = False

             self.POS9.center_x = 500
             self.POS9.center_y = 500 
             if(self.turn=='X'): 
               self.POS9.texture = self.textureX
               self.turn = 'O'
               self.histplay[8] = 'X'
             else:
               self.POS9.texture = self.textureO
               self.turn = 'X'   
               self.histplay[8] = 'O'   


        if(self.turn=='X'):
           self.border.center_x = 700 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
           self.border.center_y = 500   
        elif(self.turn=='O'):
           self.border.center_x = 700 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
           self.border.center_y = 400        

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = Setting()# MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()


 