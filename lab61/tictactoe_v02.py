import arcade
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        
        self.table = arcade.Sprite('table.png')
        self.table.center_x = 300 
        self.table.center_y = 300 

        self.POS1 = arcade.Sprite('X.png')
        self.POS1.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS1.center_y = -1000 
        self.POS1.scale = 0.6

        self.POS2 = arcade.Sprite('O.png')
        self.POS2.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS2.center_y = -1000 
        self.POS2.scale = 0.6

        self.POS3 = arcade.Sprite('O.png')
        self.POS3.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS3.center_y = -1000 
        self.POS3.scale = 0.6

        self.POS4 = arcade.Sprite('O.png')
        self.POS4.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS4.center_y = -1000 
        self.POS4.scale = 0.6

        self.POS5 = arcade.Sprite('O.png')
        self.POS5.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS5.center_y = -1000 
        self.POS5.scale = 0.6

        self.POS6 = arcade.Sprite('O.png')
        self.POS6.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS6.center_y = -1000 
        self.POS6.scale = 0.6

        self.POS7 = arcade.Sprite('O.png')
        self.POS7.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS7.center_y = -1000 
        self.POS7.scale = 0.6

        self.POS8 = arcade.Sprite('O.png')
        self.POS8.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS8.center_y = -1000 
        self.POS8.scale = 0.6

        self.POS9 = arcade.Sprite('O.png')
        self.POS9.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS9.center_y = -1000 
        self.POS9.scale = 0.6

        self.textureX = arcade.load_texture('X.png')
        self.textureO = arcade.load_texture('O.png')

        self.turn = 'X'

        self.histplay = ['','','','','','','','',''] #เอาไว้เก็บประวัติการเล่นของผู้เล่น

        self.ShowPlayer1 = arcade.Sprite('X.png')
        self.ShowPlayer1.center_x = 700 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.ShowPlayer1.center_y = 500 
        self.ShowPlayer1.scale = 0.3

        self.ShowPlayer2 = arcade.Sprite('O.png')
        self.ShowPlayer2.center_x = 700 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.ShowPlayer2.center_y = 400 
        self.ShowPlayer2.scale = 0.3

        self.border = arcade.Sprite('border.png')
        self.border.center_x = 700 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.border.center_y = 500 
        self.border.scale = 0.3

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.table.draw()
        self.POS1.draw()
        self.POS2.draw()
        self.POS3.draw()
        self.POS4.draw()
        self.POS5.draw()
        self.POS6.draw()
        self.POS7.draw()
        self.POS8.draw()
        self.POS9.draw()

        self.ShowPlayer1.draw()
        self.ShowPlayer2.draw()
        self.border.draw()
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass

    # Creating function to check the mouse clicks
    def on_mouse_press(self, x, y, button, modifiers):
        print(x,y) 

        if(x>0 and x<200 and y>0 and y<200): #ช่องแถวล่างซ้าย
           if(self.histplay[0]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม
             self.POS1.center_x = 100 
             self.POS1.center_y = 100 
             if(self.turn=='X'):
               self.POS1.texture = self.textureX
               self.histplay[0] = 'X'
               self.turn = 'O'
             else:
               self.POS1.texture = self.textureO
               self.histplay[0] = 'O'
               self.turn = 'X'
             

        if(x>200 and x<400 and y>0 and y<200): #ช่องแถวล่างตรงกลาง
           if(self.histplay[1]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม 
             self.POS2.center_x = 300 - 8
             self.POS2.center_y = 100 
             if(self.turn=='X'): 
               self.POS2.texture = self.textureX
               self.histplay[1] = 'X'
               self.turn = 'O'
             else:
               self.POS2.texture = self.textureO
               self.histplay[1] = 'O'
               self.turn = 'X'

        if(x>400 and x<600 and y>0 and y<200): #ช่องแถวล่างขวา
           if(self.histplay[2]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม  
             self.POS3.center_x = 500
             self.POS3.center_y = 100 
             if(self.turn=='X'): 
               self.POS3.texture = self.textureX
               self.turn = 'O'
               self.histplay[2] = 'X'
             else:
               self.POS3.texture = self.textureO
               self.turn = 'X'
               self.histplay[2] = 'O'

        
        if(x>0 and x<200 and y>200 and y<400):   
           if(self.histplay[3]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม  
             self.POS4.center_x = 100
             self.POS4.center_y = 300 
             if(self.turn=='X'): 
               self.POS4.texture = self.textureX
               self.turn = 'O'
               self.histplay[3] = 'X'
             else:
               self.POS4.texture = self.textureO
               self.turn = 'X'
               self.histplay[3] = 'O'
               
        
        if(x>200 and x<400 and y>200 and y<400): 
           if(self.histplay[4]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม    
             self.POS5.center_x = 300
             self.POS5.center_y = 300 
             if(self.turn=='X'): 
               self.POS5.texture = self.textureX
               self.turn = 'O'
               self.histplay[4] = 'X'
             else:
               self.POS5.texture = self.textureO
               self.turn = 'X'
               self.histplay[4] = 'O'

        if(x>400 and x<600 and y>200 and y<400):  
           if(self.histplay[5]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม   
             self.POS6.center_x = 500
             self.POS6.center_y = 300 
             if(self.turn=='X'): 
               self.POS6.texture = self.textureX
               self.turn = 'O'
               self.histplay[5] = 'X'
             else:
               self.POS6.texture = self.textureO
               self.turn = 'X'
               self.histplay[5] = 'O'

        if(x>0 and x<200 and y>400 and y<600): 
           if(self.histplay[6]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม    
             self.POS7.center_x = 100
             self.POS7.center_y = 500 
             if(self.turn=='X'): 
               self.POS7.texture = self.textureX
               self.turn = 'O'
               self.histplay[6] = 'X'
             else:
               self.POS7.texture = self.textureO
               self.turn = 'X'  
               self.histplay[6] = 'O'  

        if(x>200 and x<400 and y>400 and y<600):  
           if(self.histplay[7]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม   
             self.POS8.center_x = 300
             self.POS8.center_y = 500 
             if(self.turn=='X'): 
               self.POS8.texture = self.textureX
               self.turn = 'O'
               self.histplay[7] = 'X' 
             else:
               self.POS8.texture = self.textureO
               self.turn = 'X' 
               self.histplay[7] = 'O' 

        if(x>400 and x<600 and y>400 and y<600):  
           if(self.histplay[8]==''): #ป้องกันการกดเล่นซ้ำตำแหน่งเดิม   
             self.POS9.center_x = 500
             self.POS9.center_y = 500 
             if(self.turn=='X'): 
               self.POS9.texture = self.textureX
               self.turn = 'O'
               self.histplay[8] = 'X'
             else:
               self.POS9.texture = self.textureO
               self.turn = 'X'   
               self.histplay[8] = 'O'   


        if(self.turn=='X'):
           self.border.center_x = 700 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
           self.border.center_y = 500   
        elif(self.turn=='O'):
           self.border.center_x = 700 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
           self.border.center_y = 400        

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()


import numpy as np
data = [3,4,5]
data = np.array(data)**2
print(sum(data))


import numpy as np
speed = [3,14,5,7,7,4,2]
x = np.median(speed)

print(x)