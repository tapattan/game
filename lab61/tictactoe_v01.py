import arcade
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        
        self.table = arcade.Sprite('table.png')
        self.table.center_x = 300 
        self.table.center_y = 300 

        self.POS1 = arcade.Sprite('X.png')
        self.POS1.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS1.center_y = -1000 
        self.POS1.scale = 0.6

        self.POS2 = arcade.Sprite('O.png')
        self.POS2.center_x = -1000 #ใส่ๆ ไปเพื่อไม่ให้แสดงบนจอ 
        self.POS2.center_y = -1000 
        self.POS2.scale = 0.6

        self.textureX = arcade.load_texture('X.png')
        self.textureO = arcade.load_texture('O.png')

        self.turn = 'X'

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.table.draw()
        self.POS1.draw()
        self.POS2.draw()
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass

    # Creating function to check the mouse clicks
    def on_mouse_press(self, x, y, button, modifiers):
        print(x,y) 

        if(x>0 and x<200 and y>0 and y<200): #ช่องแถวล่างซ้าย
           self.POS1.center_x = 100 
           self.POS1.center_y = 100 
           if(self.turn=='X'):
             self.POS1.texture = self.textureX
             self.turn = 'O'
           else:
             self.POS1.texture = self.textureO
             self.turn = 'X'
             

        if(x>200 and x<400 and y>0 and y<200): #ช่องแถวล่างตรงกลาง
           self.POS2.center_x = 300 - 8
           self.POS2.center_y = 100 
           if(self.turn=='X'): 
             self.POS2.texture = self.textureX
             self.turn = 'O'
           else:
             self.POS2.texture = self.textureO
             self.turn = 'X'
               

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
