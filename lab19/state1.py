import arcade
import random 
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

from state2 import GameState2
from hero import hero 

class GameState1(arcade.View):
    def __init__(self):
        super().__init__()
        self.setup()
        self.score  = 0
    
    def setup(self):
        pass
        self.hero = hero() 
         
    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('GameState1 : '+str(self.score), 10, 20, arcade.color.WHITE, 14)
        self.hero.draw()
    
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        print('state1',self.score)
        self.hero.update()
        self.hero.update_animation()

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.SPACE:
            self.score = self.score + 100
            game_view = GameState2()
            game_view.score = self.score
            self.window.show_view(game_view)
        

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass
 
