import arcade
import random 
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

from state1 import GameState1
from hero import hero 

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
 
        self.score = 0
        self.hero = hero()
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
         
        arcade.draw_text('main', 10, 20, arcade.color.WHITE, 14)
        self.hero.draw()

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.hero.update()
        self.hero.update_animation()
        pass

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.SPACE:
            
            self.score = 100
            game_view = GameState1()
            game_view.score = self.score 
            self.window.show_view(game_view)
        

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass


def main():
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    window.total_score = 0
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()