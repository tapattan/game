import arcade
from hero import hero 
 
class GameState2(arcade.View):
    def __init__(self):
        super().__init__()
        self.score = 0
        self.setup()
    
    def setup(self):
        pass
        self.hero = hero() 

    def on_show(self):
        arcade.set_background_color(arcade.color.BLUE)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('GameState2 : '+str(self.score), 10, 20, arcade.color.WHITE, 14)
        self.hero.draw()
        
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        print('state1',self.score)
        self.hero.update()
        self.hero.update_animation()