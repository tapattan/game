import arcade
import random
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600


class MyGame(arcade.Window):
    def __init__(self, width, height):
        super().__init__(width, height)
        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        pass
        
        self.StarList = arcade.SpriteList()
        for i in range(20):
          star = arcade.Sprite('star.png')
          star.center_x = random.randint(20,580)
          star.center_y = random.randint(20,580)
          star.scale = 0.05
          self.StarList.append(star)

        self.timeblink = 0 
        self.Frameblink = 0 

        self.StarListStatic = arcade.SpriteList()
        for i in range(50):
          star = arcade.Sprite('star.png')
          star.center_x = random.randint(20,580)
          star.center_y = random.randint(20,580)
          star.scale = 0.02
          self.StarListStatic.append(star)

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        
        self.StarListStatic.draw()
        self.StarList.draw()

        
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.StarList.update()
        
        self.timeblink += delta_time 
        if(self.timeblink>0.5):
           self.timeblink = 0

           self.Frameblink +=1 
        
        if(self.Frameblink%2==0):
           k = random.choices(self.StarList,k=5)
           for i in k:
             i.scale = random.randint(30,40)/1000
        else:
           k = random.choices(self.StarList,k=5)
           for i in k:
             i.scale = random.randint(30,40)/1000
        

        if(self.Frameblink==2):
           self.Frameblink=0   

        pass
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        pass
         
      

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass 
def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()