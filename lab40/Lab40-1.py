import arcade

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600


class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        self.hero = arcade.Sprite('images/walk1.png')
        self.hero.center_x = 100
        self.hero.center_y = 100

        
        ######step down
        self.heroTexture1 = arcade.load_texture('images/walk1.png')
        #self.heroTexture2 = arcade.load_texture('images/walk2.png') #โหลดเสื้อผ้า
        self.heroTexture3 = arcade.load_texture('images/walk3.png') #โหลดเสื้อผ้า

        ######step up
        self.heroTexture4 = arcade.load_texture('images/back1.png')
        #self.heroTexture5 = arcade.load_texture('images/back2.png') #โหลดเสื้อผ้า
        self.heroTexture6 = arcade.load_texture('images/back3.png') #โหลดเสื้อผ้า
        
        ######step right
        self.heroTexture7 = arcade.load_texture('images/right1.png')
        #self.heroTexture8 = arcade.load_texture('images/back2.png') #โหลดเสื้อผ้า
        self.heroTexture9 = arcade.load_texture('images/right3.png') #โหลดเสื้อผ้า

        ######step left
        self.heroTexture10 = arcade.load_texture('images/left1.png')
        #self.heroTexture11 = arcade.load_texture('images/back2.png') #โหลดเสื้อผ้า
        self.heroTexture12 = arcade.load_texture('images/left3.png') #โหลดเสื้อผ้า

        self.timeaction = 0 #รันครั้งแรก ครั้งเดียว
        self.heroFrmae = 0

        self.direct = 'down'
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.hero.draw()
        # Your drawing code goes here

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.hero.update()

        self.timeaction = self.timeaction + delta_time 
        print(self.timeaction)

        if(self.timeaction < 0.3): #แปลว่า มันผ่านไปแล้ว 1 วินาที
            self.heroFrmae = 1
        elif(self.timeaction>0.3 and self.timeaction<0.6):
            self.heroFrmae = 2
        #elif(self.timeaction>0.4 and self.timeaction<0.6):
        #    self.heroFrmae = 3
        else:
           self.timeaction = 0 #กำหนดให้เค้าไปนับใหม่
         
        
        if(self.direct == 'down'):
          if(self.heroFrmae==1):
             self.hero.texture = self.heroTexture1
          if(self.heroFrmae==2):
             self.hero.texture = self.heroTexture3
          #if(self.heroFrmae==3):
          #   self.hero.texture = self.heroTexture3
        elif(self.direct == 'up'):
          if(self.heroFrmae==1):
             self.hero.texture = self.heroTexture4
          if(self.heroFrmae==2):
             self.hero.texture = self.heroTexture6
        elif(self.direct == 'right'):
          if(self.heroFrmae==1):
             self.hero.texture = self.heroTexture7
          if(self.heroFrmae==2):
             self.hero.texture = self.heroTexture9
        elif(self.direct == 'left'):
          if(self.heroFrmae==1):
             self.hero.texture = self.heroTexture10
          if(self.heroFrmae==2):
             self.hero.texture = self.heroTexture12

        pass
  
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.S:
            self.hero.change_y = -5
            self.direct = 'down'
        elif key == arcade.key.W:
            self.hero.change_y = 5    
            self.direct = 'up'
        elif key == arcade.key.D:
            self.hero.change_x= 5    
            self.direct = 'right'  
        elif key == arcade.key.A:
            self.hero.change_x= -5    
            self.direct = 'left'      
     
    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        if key == arcade.key.S:
            self.hero.change_y = 0
        elif key == arcade.key.W:
            self.hero.change_y = 0  
        elif key == arcade.key.D:
            self.hero.change_x = 0   
        elif key == arcade.key.A:
            self.hero.change_x = 0        
 
def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()