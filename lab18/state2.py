import arcade
 
class GameState2(arcade.View):
    def __init__(self):
        super().__init__()
        self.score = 0
        self.setup()
    
    def setup(self):
        pass

    def on_show(self):
        arcade.set_background_color(arcade.color.BLUE)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('GameState2 : '+str(self.score), 10, 20, arcade.color.WHITE, 14)