import arcade
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600


class fruits(arcade.Sprite): # inheritance

    def __init__(self,filename,center_x,center_y):
        super().__init__(filename)
        #self.texture = arcade.load_texture(filename)
 
        self.center_x = center_x
        self.center_y = center_y
        self.scale = 1.25

        self.box = arcade.Sprite('images/box.png')
        self.box.center_x = center_x
        self.box.center_y = center_y
        
    def setMove(self):
        self.center_y -= 50
        self.box.center_y -= 50

    def draw_object(self):
        self.box.draw()
        self.draw() 

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    
    def setup(self):
        
        #self.f1 = fruits('f1.png',50,650)
        self.f1 = fruits('images/f5.png',250,650)

        self.timemove = 0

        
    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()

        self.f1.draw_object()
         
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.timemove = self.timemove+delta_time 
        if(self.timemove>=0.5):
           if(self.f1.center_y>50): 
             self.f1.setMove()

           self.timemove = 0

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
