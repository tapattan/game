import arcade
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600


class fruits:

    def __init__(self,filename,center_x,center_y):
        self.sprite = arcade.Sprite(filename)
        self.sprite.center_x = center_x
        self.sprite.center_y = center_y
        self.sprite.scale = 1.25

        self.box = arcade.Sprite('images/box.png')
        self.box.center_x = center_x
        self.box.center_y = center_y
        
    def setMove(self):
        self.sprite.center_y -= 50
        self.box.center_y -= 50

    def draw(self):
        self.box.draw() 
        self.sprite.draw()

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    
    def setup(self):
        
        #self.f1 = fruits('f1.png',50,650)
        self.f1 = fruits('images/f5.png',250,650)

        self.timemove = 0

        
    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()

        self.f1.draw()
         
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.timemove = self.timemove+delta_time 
        if(self.timemove>=0.5):
           if(self.f1.sprite.center_y>50): 
             self.f1.setMove()

           self.timemove = 0

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
