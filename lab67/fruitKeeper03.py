import arcade
import random 

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 800


class fruits:

    def __init__(self,filename,center_x,center_y):
        self.sprite = arcade.Sprite(filename)
        self.sprite.center_x = center_x
        self.sprite.center_y = center_y
        self.sprite.scale = 1.25/2

        self.box = arcade.Sprite('images/box.png')
        self.box.center_x = center_x
        self.box.center_y = center_y
        self.box.scale = 0.9/2
        
    def setMove(self):
        self.sprite.center_y -= 50
        self.box.center_y -= 50

    def draw(self):
        self.box.draw() 
        self.sprite.draw()

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        
        self.setup()

    
    def setup(self):
        
        #self.f1 = fruits('f1.png',50,500)

        self.all_item = []

        for j in range(12):
          for i in range(12):
            if(i>=1 and i<=10 and j>=1 and j<=10):  
              k = random.randint(1,6)
              f1 = fruits('images/f'+str(k)+'.png',25+(i*50),25+(j*50))
              self.all_item.append(f1)

        self.timemove = 0

        self.mouse = arcade.Sprite('images/mouse.png')
        self.mouse.scale = 0.5

        self.border = arcade.Sprite('images/border.png')
        #self.border.center_x = 325
        #self.border.center_y = 325

        self.firstX = 0 
        self.firstY = 0
        self.secondX = 0 
        self.secondY = 0


        
    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        
        for i in self.all_item:
          i.draw() 
        
        self.border.draw()
        self.mouse.draw()  
         
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        '''self.timemove = self.timemove+delta_time 
        if(self.timemove>=0.5):
           if(self.f1.sprite.center_y>50): 
             self.f1.setMove()

           self.timemove = 0'''
        
    def on_mouse_motion(self, x, y, dx, dy): 
        """ 
        Called whenever the mouse moves. 
        """
        self.mouse.center_x = x 
        self.mouse.center_y = y 

      
    # Creating function to check the mouse clicks 
    def on_mouse_press(self, x, y, button, modifiers): 
        print("Mouse button is pressed")
        for i in self.all_item:
           k = arcade.check_for_collision(self.mouse, i.sprite)
           if(k):
             print(i.sprite.center_x,i.sprite.center_y)

             self.border.center_x = i.sprite.center_x
             self.border.center_y = i.sprite.center_y

             if(self.firstX==0 and self.firstY==0):
               self.firstX = i.sprite.center_x
               self.firstY = i.sprite.center_y
             else:
               self.secondX = i.sprite.center_x    
               self.secondY = i.sprite.center_y 
               
               absX = abs(self.firstX-self.secondX)
               absY = abs(self.firstY-self.secondY)

               if(absX <=50 and absY<=50):
                  print('near')
               else:
                  print('far')  

               self.firstX = 0 
               self.firstY = 0
               self.secondX = 0 
               self.secondY = 0   




             
             break

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.set_mouse_visible(False)
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
