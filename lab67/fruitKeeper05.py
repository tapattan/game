import arcade
import random 

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 800


class fruits:

    def __init__(self,filename,center_x,center_y):
        self.sprite = arcade.Sprite()
        self.sprite.texture = arcade.load_texture(filename)

        self.sprite.center_x = center_x
        self.sprite.center_y = center_y
        self.sprite.scale = 1.25/2
      
        self.boxID = str(center_x)+str(center_y)+str(filename)

        self.box = arcade.Sprite('images/box.png')
        self.box.center_x = center_x
        self.box.center_y = center_y
        self.box.scale = 0.9

        self.name = filename.split('/')[1]
        self.show = True
        
    def setMove(self):
        self.sprite.center_y -= 50
        self.box.center_y -= 50

    def draw(self):
        self.box.draw() 
        self.sprite.draw()

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        
        self.setup()

    
    def setup(self):
        
        #self.f1 = fruits('f1.png',50,500)

        self.all_item = []

        for j in range(12):
          for i in range(12):
            if(i>=1 and i<=10 and j>=1 and j<=10):  
              k = random.randint(1,8)
              f1 = fruits('images/f'+str(k)+'.png',25+(i*50),25+(j*50))
              self.all_item.append(f1)

        self.timemove = 0

        self.mouse = arcade.Sprite('images/mouse.png')
        self.mouse.scale = 0.5

        self.border = arcade.Sprite('images/border.png')
        #self.border.center_x = 325
        #self.border.center_y = 325
        
        self.firstID = 0
        self.firstX = 0 
        self.firstY = 0

        self.secondID = 0
        self.secondX = 0 
        self.secondY = 0

        self.c1 = arcade.Sprite('images/c1.jpg')

        self.timeAntimate = 0
        
    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        
        for i in self.all_item:
          i.draw() 
        
        if(not (self.firstX==0 and self.firstY==0 and self.secondX==0 and self.secondY==0)):
          self.border.draw()

        self.mouse.draw()  
         
        pass
        #self.c1.draw()
    
    def checkHideAtRow(self,p,row='row'):
      #print('*',p[0].name)
      m = p[0].name
      cnt = 0
      c_ans = []
      start = 0
      stop = 0

      distance = 25

      for i in range(len(p)):
        print(p[i].sprite.center_x)
        if(m==p[i].name and p[i].sprite.center_x==distance+50 and row=='row'): #ระยะห่างห้าม มี block เว้น
          cnt=cnt+1 
          stop=stop+1
          distance = p[i].sprite.center_x 
        elif(m==p[i].name and row=='col'): 
          cnt=cnt+1 
          stop=stop+1
        else:
          if(cnt>=3):
            c_ans.append((m,start,cnt))
          cnt = 1
          start = i
          stop = i
          m = p[i].name 
          distance = p[i].sprite.center_x 

      if(cnt>=3):
        c_ans.append((m,start,cnt))

      #print(c_ans)  #ตัวไหนหลาย เริ่มจากตำแหน่งอะไร ไปกี่ตัว 
      return c_ans

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        '''self.timemove = self.timemove+delta_time 
        if(self.timemove>=0.5):
           if(self.f1.sprite.center_y>50): 
             self.f1.setMove()

           self.timemove = 0'''

        if(self.firstX==0 and self.firstY==0 and self.secondX==0 and self.secondY==0):
          
          for kk in range(1,11):
            row_at = kk
            row1 = self.getRow(row_at)
            
            try:
              k = self.checkHideAtRow(row1)  
            

              if(k==0):
                break
              
              if(len(k)>0):
                  for j in range(len(k)):
                      who,start ,cnt = k[j]
                      #print('*',who,start,cnt)
                      m = 0
                      for i in range(len(row1)):
                          if(i>=start and m<cnt):
                              m+=1
                              #row1[i].show = False 
                              row1[i].sprite.center_x = -100
                              row1[i].box.center_x = -100
                              print('doneRow')
            except:
               #print('*')
               #for i in range(len(row1)):
               #    print(row1[i].name,end=',')
               #print('#')
               pass
                              
                            


        if(self.firstX==0 and self.firstY==0 and self.secondX==0 and self.secondY==0):
           
           for kk in range(1,11):
             col = self.getCol(kk)
             #print('*C',col)
             #for i in range(len(col)):
             #     print(col[i].name,end=',')
             #print('--')
             
            
             try:
               k = self.checkHideAtRow(col,row='col')
               if(k==0):
                  break 
               
               #print(k)
               if(len(k)>0):
                  for j in range(len(k)):
                      who,start ,cnt = k[j]
                      #print('*',who,start,cnt)
                      m = 0
                      for i in range(len(col)):
                          if(i>=start and m<cnt):
                              m+=1
                              #col[i].show = False 
                              col[i].sprite.center_x = - 100
                              col[i].box.center_x = -100
             except:
               #print('*')
               #for i in range(len(col)):
               #    print(col[i].name,end=',')
               #print('#')
               pass
              
                               
     


        if(self.firstX==0 and self.firstY==0 and self.secondX==0 and self.secondY==0):
            #for km in range(1,11):
            for kk in range(1,11):
              col = self.getCol(kk)
              #print('*C',col)
              #for i in range(len(col)):
              #    print(col[i].name,end=',')
              #print('--')
              self.c1.center_x = 25+(kk*50)
              self.c1.center_y = 25+(1*50)
              for i in range(len(col)):
                m = arcade.check_for_collision(self.c1,col[i].sprite)  
                if(m):
                  self.c1.center_y +=50
                else:
                  col[i].sprite.center_y -= 50   
                  col[i].box.center_y -=50
                   
                   
                   
              

             
    def getCol(self,colAt):
        row_data = []
        posX =  25+(colAt*50)  
        #print(posX)
        for i in self.all_item:
           #print(i.sprite.center_y,posY)
           if(i.sprite.center_x==posX and i.sprite.center_x!=-100):  
             row_data.append(i)
        

        #sort data
        for i in range(len(row_data)):
           for j in range(len(row_data)-1):
              if(row_data[j].sprite.center_y>=row_data[j+1].sprite.center_y):
                row_data[j], row_data[j+1] = row_data[j+1],row_data[j]

        return row_data
             
    def getRow(self,rowAt):
        row_data = []
        posY =  25+(rowAt*50)  
        for i in self.all_item:
           #print(i.sprite.center_y,posY)
           if(i.sprite.center_y==posY and i.sprite.center_x!=-100): 
             #p = int((i.sprite.center_x-25)/50)
             row_data.append(i)

        #sort data
        for i in range(len(row_data)):
           for j in range(len(row_data)-1):
              if(row_data[j].sprite.center_x>=row_data[j+1].sprite.center_x):
                row_data[j], row_data[j+1] = row_data[j+1],row_data[j]

        return row_data

    
             
             
           
        
    def on_mouse_motion(self, x, y, dx, dy): 
        """ 
        Called whenever the mouse moves. 
        """
        self.mouse.center_x = x 
        self.mouse.center_y = y 

      
    # Creating function to check the mouse clicks 
    def on_mouse_press(self, x, y, button, modifiers): 
        print("Mouse button is pressed")
        for i in self.all_item:
           k = arcade.check_for_collision(self.mouse, i.sprite)
           if(k):
             #print(i.sprite.center_x,i.sprite.center_y)

             self.border.center_x = i.sprite.center_x #ให้เกิด border 
             self.border.center_y = i.sprite.center_y #ให้เกิด border

             if(self.firstX==0 and self.firstY==0):
               self.firstX = i.sprite.center_x
               self.firstY = i.sprite.center_y
               self.firstID = i.boxID
             else:
               self.secondX = i.sprite.center_x    
               self.secondY = i.sprite.center_y 
               
               absX = abs(self.firstX-self.secondX)
               absY = abs(self.firstY-self.secondY)
               self.secondID = i.boxID

               if(absX <=50 and absY<=50):
                  print('near')
                  
                
                  for m in self.all_item:
                    if(m.boxID==self.firstID):
                       k1 = m

                  for m in self.all_item:
                     if(m.boxID==self.secondID):
                       k2 = m
                  
                  # สลับ texture
                  k1.sprite.texture , k2.sprite.texture = k2.sprite.texture , k1.sprite .texture
                  k1.name , k2.name = k2.name , k1.name
                  #print(self.secondID,self.firstID)


               else:
                  print('far')  

               self.firstX = 0 
               self.firstY = 0
               self.secondX = 0 
               self.secondY = 0   






             
             break

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.set_mouse_visible(False)
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
