import arcade
import random 

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 800


class fruits:

    def __init__(self,filename,center_x,center_y):
        self.sprite = arcade.Sprite()
        self.sprite.texture = arcade.load_texture(filename)

        self.sprite.center_x = center_x
        self.sprite.center_y = center_y

        self.position_x_old = center_x
        self.position_y_old = center_y

        self.sprite.scale = 1.25/2
      
        self.boxID = str(center_x)+str(center_y)+str(filename)

        self.box = arcade.Sprite('images/box.png')
        self.box.center_x = center_x
        self.box.center_y = center_y
        self.box.scale = 0.9

        self.name = filename.split('/')[1]
        self.show = True
        
    def setMove(self):
        self.sprite.center_y -= 50
        self.box.center_y -= 50

    def draw(self):
        self.box.draw() 
        self.sprite.draw()

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        
        self.setup()

    
    def setup(self):
        
        #self.f1 = fruits('f1.png',50,500)

        self.all_item = []

        for j in range(12):
          for i in range(12):
            if(i>=1 and i<=10 and j>=1 and j<=10):  
              k = random.randint(1,8)
              f1 = fruits('images/f'+str(k)+'.png',25+(i*50),25+(j*50))
              self.all_item.append(f1)

        self.timemove = 0

        self.mouse = arcade.Sprite('images/mouse.png')
        self.mouse.scale = 0.5

        self.border = arcade.Sprite('images/border.png')
        #self.border.center_x = 325
        #self.border.center_y = 325
        
        self.firstID = 0
        self.firstX = 0 
        self.firstY = 0

        self.secondID = 0
        self.secondX = 0 
        self.secondY = 0

        self.c1 = arcade.Sprite('images/c1.jpg')

        self.timeAntimate = 0

        self.statusBlink = False
        self.statusBlinkTime = 0

        self.blinkTexture = arcade.load_texture('images/blink.png')

        t1 = arcade.load_texture('images/f1.png')
        t2 = arcade.load_texture('images/f2.png')
        t3 = arcade.load_texture('images/f3.png')
        t4 = arcade.load_texture('images/f4.png')
        t5 = arcade.load_texture('images/f5.png')
        t6 = arcade.load_texture('images/f6.png')
        t7 = arcade.load_texture('images/f7.png')
        t8 = arcade.load_texture('images/f8.png')
      

        self.textureList = [t1,t2,t3,t4,t5,t6,t7,t8] 
        self.textureListName = ['f1.png','f2.png','f3.png','f4.png','f5.png','f6.png','f7.png','f8.png']

        self.level = random.randint(1,8)
        
        self.objective = arcade.Sprite("images/"+self.textureListName[self.level-1])
        self.objective.center_x = 100
        self.objective.center_y = 700

        self.scoreShow = '10' #คะแนนเป้าหมายที่ต้องการทำ 
        self.score = 10

        self.scoreNum1 = arcade.Sprite("number/"+self.scoreShow[0]+'.png')
        self.scoreNum1.center_x = 430
        self.scoreNum1.center_y = 700
        self.scoreNum1.scale = 0.4

        self.scoreNum2 = arcade.Sprite("number/"+self.scoreShow[1]+'.png')
        self.scoreNum2.center_x = 500
        self.scoreNum2.center_y = 700
        self.scoreNum2.scale = 0.4

        k0 = arcade.load_texture('number/0.png')
        k1 = arcade.load_texture('number/1.png')
        k2 = arcade.load_texture('number/2.png')
        k3 = arcade.load_texture('number/3.png')
        k4 = arcade.load_texture('number/4.png')
        k5 = arcade.load_texture('number/5.png')
        k6 = arcade.load_texture('number/6.png')
        k7 = arcade.load_texture('number/7.png')
        k8 = arcade.load_texture('number/8.png')
        k9 = arcade.load_texture('number/9.png')
      

        self.NumberTextureList = [k0,k1,k2,k3,k4,k5,k6,k7,k8,k9] 

        self.count_score = False
        self.count_scoreList = []
        
        
        
    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        
        for i in self.all_item:
          i.draw() 
        
        if(not (self.firstX==0 and self.firstY==0 and self.secondX==0 and self.secondY==0)):
          self.border.draw()

        self.mouse.draw() 
        self.objective.draw() 
        
        if(self.score>9):
          self.scoreNum1.draw()
        if(self.score>=0):
          self.scoreNum2.draw()

        pass
        

    
        

        #if(self.statusBlink==True):
           
        #self.c1.draw()
    
    def checkHideAtRow(self,p,row='row'):
      #print('*',p[0].name)
      m = p[0].name
      cnt = 0
      c_ans = []
      start = 0
      stop = 0

      distance = 25

      for i in range(len(p)):
        #print(p[i].sprite.center_x)
        if(m==p[i].name and p[i].sprite.center_x==distance+50 and row=='row'): #ระยะห่างห้าม มี block เว้น
          cnt=cnt+1 
          stop=stop+1
          distance = p[i].sprite.center_x 
        elif(m==p[i].name and row=='col'): 
          cnt=cnt+1 
          stop=stop+1
        else:
          if(cnt>=3):
            c_ans.append((m,start,cnt))
          cnt = 1
          start = i
          stop = i
          m = p[i].name 
          distance = p[i].sprite.center_x 

      if(cnt>=3):
        c_ans.append((m,start,cnt))

      #print(c_ans)  #ตัวไหนหลาย เริ่มจากตำแหน่งอะไร ไปกี่ตัว 
      return c_ans

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        '''self.timemove = self.timemove+delta_time 
        if(self.timemove>=0.5):
           if(self.f1.sprite.center_y>50): 
             self.f1.setMove()

           self.timemove = 0'''

        if(self.firstX==0 and self.firstY==0 and self.secondX==0 and self.secondY==0):
          
          for kk in range(1,11):
            row_at = kk
            row1 = self.getRow(row_at)
            
            try:
              k = self.checkHideAtRow(row1)  
            

              if(k==0):
                break
              
              
              if(len(k)>0):
                  for j in range(len(k)):  
                      who,start ,cnt = k[j]
                      #print('*',who,start,cnt)
                      m = 0
                      for i in range(len(row1)):
                          if(i>=start and m<cnt):
                              m+=1
                              row1[i].show = False 
                              #row1[i].sprite.center_x = -100
                              row1[i].box.center_x = -100
                              #print('*** row **',row1[i].name)
                              
                              
                              match_obj = self.textureListName[self.level-1] 
                              #print('ROW at',match_obj , row1[i].name)
                              if(match_obj == row1[i].name):
                                 #print('**** match row ****',row1[i].name,match_obj,who)
                                 self.count_score = True

                              
                              if(not i in self.count_scoreList):
                                 #print('&&&',i)
                                 self.count_scoreList.append(i)
                                 
                        
                              row1[i].sprite.texture = self.blinkTexture
                              #print('kkkkmmmm',i,j)
                              self.statusBlink = True
                
            except:
               #print('*')
               #for i in range(len(row1)):
               #    print(row1[i].name,end=',')
               #print('#')
               pass
                              
                            

        
        if(self.firstX==0 and self.firstY==0 and self.secondX==0 and self.secondY==0):
           
           for kk in range(1,11):
             col = self.getCol(kk)
             #print('*C',col)
             #for i in range(len(col)):
             #     print(col[i].name,end=',')
             #print('--')
             
            
             try:
               k = self.checkHideAtRow(col,row='col')
               if(k==0):
                  break 
               
               #print(k)
               if(len(k)>0):
                  for j in range(len(k)):
                    
                      who,start ,cnt = k[j]
                      #print('*',who,start,cnt)
                      m = 0
                      for i in range(len(col)):
                          if(i>=start and m<cnt):
                              m+=1
                              col[i].show = False 
                              #col[i].sprite.center_x = - 100
                              col[i].box.center_x = -100

                              match_obj = self.textureListName[self.level-1] 
                              if(match_obj == col[i].name):
                                 #print('**** match col ****',col[i].name,match_obj,who)
                                 self.count_score = True 
                                  
                              
                              if(not i in self.count_scoreList):
                                 self.count_scoreList.append(i)

                              col[i].sprite.texture = self.blinkTexture
                              #print('kkkkbbbb',i,j)
                              self.statusBlink = True
                              
             except:
               #print('*')
               #for i in range(len(col)):
               #    print(col[i].name,end=',')
               #print('#')
               pass
              
                               
        if(self.statusBlink == True):
           self.statusBlinkTime += delta_time
           self.statusBlink = False

           if(self.statusBlinkTime>0.2):
              self.statusBlinkTime = 0

              if(self.count_score==True):
                print('@@@@',len(self.count_scoreList))
                self.score = self.score - len(self.count_scoreList) #self.score_cando #ปรับคะแนน
              
              self.count_score = False 

              for i in range(len(self.count_scoreList)):
                self.count_scoreList.pop() 

              if(self.score<=0):
                 self.score=0 
                 self.scoreNum1.texture = self.NumberTextureList[0]
                 self.scoreNum2.texture = self.NumberTextureList[0]
                 
                 #ถ้าศูนย์แล้ว
                 game_view = YouWin()
                 self.window.show_view(game_view)

              else:
                 self.scoreShow = str(self.score)
                 print('** scoreShow **',self.scoreShow)
                 
                 if(self.scoreShow=='0'):
                   self.scoreNum1.texture = self.NumberTextureList[0]
                   self.scoreNum2.texture = self.NumberTextureList[0] 
                 elif(self.score<10):
                   self.scoreNum1.texture = self.NumberTextureList[0]
                   self.scoreNum2.texture = self.NumberTextureList[int(self.scoreShow[0])]
                 else:
                   self.scoreNum1.texture = self.NumberTextureList[int(self.scoreShow[0])]
                   self.scoreNum2.texture = self.NumberTextureList[int(self.scoreShow[1])]
                 

              
              
              

              for i in self.all_item:
                if(i.show==False):
                    i.position_x_old = i.sprite.center_x
                    i.position_y_old = i.sprite.center_y
                    i.sprite.center_x = -100   #วาร์ป again!!


           return 0 
        
        ####### ปรับตำแหน่งให้ไปข้างบน #########
        for i in self.all_item:
            if(i.show==False): 
                #print(i.position_x_old)
                i.sprite.center_x = i.position_x_old
                i.sprite.center_y = i.position_y_old + 500
                i.box.center_x = i.position_x_old 
                i.box.center_y = i.sprite.center_y

                r  = random.randint(0,len(self.textureList)-1)
                i.sprite.texture = self.textureList[r]
                i.name = self.textureListName[r]
                i.show = True 

        ###### ส่วนที่ทำให้เลื่อนลงมา ############
        if(self.firstX==0 and self.firstY==0 and self.secondX==0 and self.secondY==0):
            #for km in range(1,11):
            for kk in range(1,12):
              col = self.getCol(kk)
              #print('*C',col)
              #for i in range(len(col)):
              #    print(col[i].name,end=',')
              #print('--')
              self.c1.center_x = 25+(kk*50)
              self.c1.center_y = 25+(1*50)
              self.timeAntimate += delta_time
              if(self.timeAntimate > 0.1):
                for i in range(len(col)):
                  m = arcade.check_for_collision(self.c1,col[i].sprite)  
                  if(m):
                    self.c1.center_y +=50 #ตัวเช็คการเลื่อน ตรวจที่ไปตรวจ
                  else:
                    col[i].sprite.center_y -= 50   
                    col[i].box.center_y -=50

                self.timeAntimate = 0    
                   
                   
        ###################
                   
 
 
        ###### ส่วนป้องกัน ให้กลับไปเริ่มใหม่
        m_pos_x = []
        m_pos_y = []
        for i in self.all_item:
           m_pos_x.append(i.sprite.center_x)
           m_pos_y.append(i.sprite.center_y)
           if(i.sprite.center_y < -500):
              i.sprite.center_y = 625
              i.box.center_y = 625
        
        #print('MAXMAIN',max(m_pos_x),min(m_pos_x),max(m_pos_y),min(m_pos_y))


        
      

        
             
    def getCol(self,colAt):
        row_data = []
        posX =  25+(colAt*50)  
        #print(posX)
        for i in self.all_item:
           #print(i.sprite.center_y,posY)
           if(i.sprite.center_x==posX and i.sprite.center_x!=-100):  
             row_data.append(i)
        

        #sort data
        for i in range(len(row_data)):
           for j in range(len(row_data)-1):
              if(row_data[j].sprite.center_y>=row_data[j+1].sprite.center_y):
                row_data[j], row_data[j+1] = row_data[j+1],row_data[j]

        return row_data
             
    def getRow(self,rowAt):
        row_data = []
        posY =  25+(rowAt*50)  
        for i in self.all_item:
           #print(i.sprite.center_y,posY)
           if(i.sprite.center_y==posY and i.sprite.center_x!=-100): 
             #p = int((i.sprite.center_x-25)/50)
             row_data.append(i)

        #sort data
        for i in range(len(row_data)):
           for j in range(len(row_data)-1):
              if(row_data[j].sprite.center_x>=row_data[j+1].sprite.center_x):
                row_data[j], row_data[j+1] = row_data[j+1],row_data[j]

        return row_data

    
             
             
           
        
    def on_mouse_motion(self, x, y, dx, dy): 
        """ 
        Called whenever the mouse moves. 
        """
        self.mouse.center_x = x 
        self.mouse.center_y = y 

      
    # Creating function to check the mouse clicks 
    def on_mouse_press(self, x, y, button, modifiers): 
        print("Mouse button is pressed")
        for i in self.all_item:
           k = arcade.check_for_collision(self.mouse, i.sprite)
           if(k):
             #print(i.sprite.center_x,i.sprite.center_y)

             self.border.center_x = i.sprite.center_x #ให้เกิด border 
             self.border.center_y = i.sprite.center_y #ให้เกิด border

             if(self.firstX==0 and self.firstY==0):
               self.firstX = i.sprite.center_x
               self.firstY = i.sprite.center_y
               self.firstID = i.boxID
             else:
               self.secondX = i.sprite.center_x    
               self.secondY = i.sprite.center_y 
               
               absX = abs(self.firstX-self.secondX)
               absY = abs(self.firstY-self.secondY)
               self.secondID = i.boxID

               if(absX <=50 and absY<=50):
                  print('near')
                  
                
                  for m in self.all_item:
                    if(m.boxID==self.firstID):
                       k1 = m

                  for m in self.all_item:
                     if(m.boxID==self.secondID):
                       k2 = m
                  
                  # สลับ texture
                  k1.sprite.texture , k2.sprite.texture = k2.sprite.texture , k1.sprite .texture
                  k1.name , k2.name = k2.name , k1.name
                  #print(self.secondID,self.firstID)

                  


               else:
                  print('far')  

               self.firstX = 0 
               self.firstY = 0
               self.secondX = 0 
               self.secondY = 0   






             
             break

class YouWin(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.BLACK)
        
        self.setup()

    
    def setup(self):
       pass 

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        arcade.draw_text('You Win', 200, 450, arcade.color.WHITE, 40)
        arcade.draw_text('press space to new game', 160, 250, arcade.color.WHITE, 20)

    def update(self, delta_time):
        pass

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.SPACE:
           game_view = MyGame()
           self.window.show_view(game_view)
       
       
def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.set_mouse_visible(False)
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()


