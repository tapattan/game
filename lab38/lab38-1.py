from turtle import width
import arcade
import arcade.gui
import random 

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

MOVEMENT_SPEED = 5

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)
    
    def setup(self):
        # Set up your game here
        
        # https://api.arcade.academy/en/latest/api/gui_widgets.html
        self.manager = arcade.gui.UIManager()
        self.manager.enable()
        box = arcade.gui.UIBoxLayout()
        button = arcade.gui.UITextureButton(x=0, y=0, width=100,height=50,
                                            texture=arcade.load_texture('b1.png'), 
                                            texture_hovered=arcade.load_texture('b2.png'), 
                                            texture_pressed=arcade.load_texture('b3.png'))
        box.add(button.with_space_around(bottom=20))
        button.on_click = self.on_click_button
        
        self.manager.add(
            arcade.gui.UIAnchorWidget(
                #anchor_x="center_x",
                #anchor_y="center_y",
                align_x = -340,
                align_y = 260,
                child=box)
        )
        
        self.bee = arcade.Sprite('bee.png',0.05 ,flipped_horizontally = True)
        self.bee.center_x = 130
        self.bee.center_y = 555 
        self.bee.change_y = 1

    @staticmethod
    def on_click_button(event):
        print('Button clicked!')

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.manager.draw()
        self.bee.draw()
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        #pass
        self.bee.update()
        if(self.bee.center_y>580):
           self.bee.change_y *=-1
        elif(self.bee.center_y<555):
           self.bee.change_y *=-1
 
        

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()