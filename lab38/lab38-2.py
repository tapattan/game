from functools import partial

import arcade
import arcade.gui
import random 

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

MOVEMENT_SPEED = 5

class GameStateStart(arcade.View):
    def __init__(self):
        super().__init__()
        self.score = 0
        self.setup()
    
    def setup(self):
        pass

    def on_show(self):
        arcade.set_background_color(arcade.color.BLUE)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('GameState Start', 10, 10, arcade.color.WHITE, 14)
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.SPACE:
           game_view = MyGame()
           self.window.show_view(game_view)  


class GameStateSettings(arcade.View):
    def __init__(self):
        super().__init__()
        self.score = 0
        self.setup()
    
    def setup(self):
        pass

    def on_show(self):
        arcade.set_background_color(arcade.color.GREEN)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('GameState Settings', 10, 10, arcade.color.WHITE, 14)
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.SPACE:
           game_view = MyGame()
           self.window.show_view(game_view) 
           
class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    
    def setup(self):
        # Set up your game here
        
        # https://api.arcade.academy/en/latest/api/gui_widgets.html
        self.manager = arcade.gui.UIManager()
        self.manager.enable()
        box = arcade.gui.UIBoxLayout()
        button = arcade.gui.UITextureButton(x=0, y=0, width=100,height=50,
                                            texture=arcade.load_texture('b1.png'), 
                                            texture_hovered=arcade.load_texture('b2.png'), 
                                            texture_pressed=arcade.load_texture('b3.png'))
        button.on_click = partial(self.on_click_button,1)

        box.add(button.with_space_around(bottom=20))

        
        
        ##### settings ######### 
        settings_button = arcade.gui.UIFlatButton(text="Settings", width=200)
        settings_button.on_click = partial(self.on_click_button,2)

        box.add(settings_button.with_space_around(bottom=20))


        self.manager.add(
            arcade.gui.UIAnchorWidget(
                #anchor_x="center_x",
                #anchor_y="center_y",
                align_x = -240,
                align_y = 160,
                child=box)
        )
        
        self.bee = arcade.Sprite('bee.png',0.05 ,flipped_horizontally = True)
        self.bee.center_x = 230
        self.bee.center_y = 490 
        self.bee.change_y = 1

    #@staticmethod
    def on_click_button(self,k,event):
        if(k==1):
          print('Button clicked! Start',self,k,event)
          game_view = GameStateStart()
          self.window.show_view(game_view)

        elif(k==2):  
          print('Button clicked! Settings',k) 
          game_view = GameStateSettings()
          self.window.show_view(game_view) 

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.manager.draw()
        self.bee.draw()
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        #pass
        self.bee.update()
        if(self.bee.center_y>520):
           self.bee.change_y *=-1
        elif(self.bee.center_y<490):
           self.bee.change_y *=-1
 
        
def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()