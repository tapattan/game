import arcade

SCREEN_WIDTH = 640
SCREEN_HEIGHT = 640
MOVEMENT_SPEED = 1

class hero(arcade.Sprite):
    def __init__(self):

        # Set up parent class
        super().__init__()
        
 
        self.texturesDown = []
        self.texturesDown.append(arcade.load_texture("../resources/sokobanpack/Character4.png"))
        self.texturesDown.append(arcade.load_texture("../resources/sokobanpack/Character5.png"))
        self.texturesDown.append(arcade.load_texture("../resources/sokobanpack/Character6.png"))
        
        self.textureList = self.texturesDown 
        self.statusFrameaction = len(self.texturesDown)
        print(self.statusFrameaction)


        self.timeFrame = 0
        self.timeKFrame = 0
        self.texture = self.textureList[self.timeFrame]
        self.center_x = 100
        self.center_y = 300
        self.scale = 1

        self.texturesLeft = []
        self.texturesLeft.append(arcade.load_texture("../resources/sokobanpack/Character1.png"))
        self.texturesLeft.append(arcade.load_texture("../resources/sokobanpack/Character10.png"))
        
        self.texturesRight = []
        self.texturesRight.append(arcade.load_texture("../resources/sokobanpack/Character2.png"))
        self.texturesRight.append(arcade.load_texture("../resources/sokobanpack/Character3.png"))
        
        self.texturesUp = []
        self.texturesUp.append(arcade.load_texture("../resources/sokobanpack/Character7.png"))
        self.texturesUp.append(arcade.load_texture("../resources/sokobanpack/Character8.png"))
        self.texturesUp.append(arcade.load_texture("../resources/sokobanpack/Character9.png"))


        self.direct = ''
        

    def update_animation(self, delta_time: float = 1 / 20):
        self.statusFrameaction = len(self.textureList)
        self.timeKFrame +=(delta_time)
        if(self.timeKFrame>0.2):
            self.timeKFrame=0
            self.timeFrame+=1
            if(self.timeFrame%self.statusFrameaction==0):
                self.timeFrame = 0
            
            try:
              self.texture = self.textureList[self.timeFrame]
            except:
              pass

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        #pass
        print(arcade.__version__)
        self.hero = hero()


        ### zone load map ########
        # Name of map file to load
        map_name = "../resources/map/lab06/map01.json"

        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "LayerGrass": {
                "use_spatial_hash": True,
            },
            "LayerWall": {
                "use_spatial_hash": True,
            },
            "LayerGoal": {
                "use_spatial_hash": True,
            },
        }

        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)


        self.LayerGrass = self.tile_map.sprite_lists["LayerGrass"]
        self.LayerWall = self.tile_map.sprite_lists["LayerWall"]
        self.LayerGoal = self.tile_map.sprite_lists["LayerGoal"]

 
        #สร้าง box จาก Sprite
        self.BlueBoxList = arcade.SpriteList()
        
        self.BlueBox = arcade.Sprite("../resources/sokobanpack/Crate_Blue.png")
        self.BlueBox.center_x = 200
        self.BlueBox.center_y = 190
        
        self.BlueBoxList.append(self.BlueBox)
            

        # Keep player from running through the wall_list layer
        ### end zone load map ####


        walls = [self.LayerWall, ]
        self.physics_engine = arcade.PhysicsEnginePlatformer(
            self.hero, walls, gravity_constant=0
        )  

        self.physics_engineBlueBoxWall = arcade.PhysicsEnginePlatformer(
            self.BlueBox, walls, gravity_constant=0
        ) 

        boxs = [self.BlueBoxList ,]
        self.physics_engineHeroBlueBox = arcade.PhysicsEnginePlatformer(
            self.hero, boxs, gravity_constant=0
        ) 
 
        
    
       

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here
        self.LayerGrass.draw()
        self.BlueBoxList.draw()
        self.LayerWall.draw()
        self.LayerGoal.draw()
      
        
        self.hero.draw()


    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.hero.update()
        self.hero.update_animation()


        self.physics_engine.update()
        self.physics_engineBlueBoxWall.update()
        self.physics_engineHeroBlueBox.update()
       
        
        self.heroSoul = self.hero  #สร้างวิญาณฮีโร่
        if(self.hero.direct=='UP'):
            self.heroSoul.center_y = self.hero.center_y+1
        elif(self.hero.direct=='DOWN'):
            self.heroSoul.center_y = self.hero.center_y-1
        elif(self.hero.direct=='LEFT'):
            self.heroSoul.center_x = self.hero.center_x-1
        elif(self.hero.direct=='RIGHT'):
            self.heroSoul.center_x = self.hero.center_x+1

        hit_bluebox = arcade.check_for_collision(self.heroSoul,self.BlueBox)
        if(hit_bluebox):
            print(self.BlueBox,self.hero.direct)
            if(self.hero.direct=='UP'):
                self.BlueBox.change_y = MOVEMENT_SPEED
            elif(self.hero.direct=='DOWN'):
                self.BlueBox.change_y = -MOVEMENT_SPEED
            elif(self.hero.direct=='LEFT'):
                self.BlueBox.change_x = -MOVEMENT_SPEED
            elif(self.hero.direct=='RIGHT'):
                self.BlueBox.change_x = MOVEMENT_SPEED
        else:
            self.BlueBox.change_x = 0
            self.BlueBox.change_y = 0
           


    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP or key == arcade.key.W:
            self.hero.change_y = MOVEMENT_SPEED
            self.hero.textureList = self.hero.texturesUp
            self.hero.direct = 'UP'
        elif key == arcade.key.DOWN or key == arcade.key.S:
            self.hero.change_y = -MOVEMENT_SPEED
            self.hero.textureList = self.hero.texturesDown
            self.hero.direct = 'DOWN'
        elif key == arcade.key.LEFT or key == arcade.key.A:
            self.hero.change_x = -MOVEMENT_SPEED
            self.hero.textureList = self.hero.texturesLeft
            self.hero.direct = 'LEFT'
        elif key == arcade.key.RIGHT or key == arcade.key.D:
            self.hero.change_x = MOVEMENT_SPEED
            self.hero.textureList = self.hero.texturesRight
            self.hero.direct = 'RIGHT'

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP or key == arcade.key.DOWN or key == arcade.key.W or key == arcade.key.S:
            self.hero.change_y = 0
       
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT or key == arcade.key.A or key == arcade.key.D:
            self.hero.change_x = 0
                 
        self.hero.direct=''
        self.BlueBox.change_x = 0
        self.BlueBox.change_y = 0

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()