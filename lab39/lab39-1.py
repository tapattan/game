from mimetypes import init
from turtle import width
import arcade
from PIL import Image

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class block(arcade.SpriteSolidColor):
    def __init__(self,filename,x,y):
        im = Image.open(filename+'.png')
        im = im.resize((200,200))
        im.save(filename+'200x200.png')

        super().__init__(width=50, height=50,color=(0,0,0,0))
        self.texture = arcade.load_texture(filename+'200x200.png')
        self.scale = 0.25
        self.center_x = x
        self.center_y = y

    def draw_border(self):
        arcade.draw_rectangle_outline(self.center_x,
                         self.center_y,
                         52,
                         52,
                         arcade.color.ALMOND)  

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)
    
    def setup(self):
        # Set up your game here
        
        self.r1 = block('hippo',x=300,y=300)
        self.r2 = block('pig',x=354,y=300)
        self.r3 = block('penguin',x=354+54,y=300)

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.r1.draw_border()
        self.r1.draw()
        
        self.r2.draw_border()
        self.r2.draw()

        self.r3.draw_border()
        self.r3.draw()

        
      

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.r1.draw()
 

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
