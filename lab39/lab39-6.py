# ไปปรับให้ trackpad ช้าลงหน่อย หรือลองใช้ mouse usb ต่อ
# กรณีที่คุณใช้ notebook


import arcade
from PIL import Image
import random 
import gc 
import numpy as np 

import threading
import time 
 

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class block(arcade.Sprite):
    def __init__(self,filename,x,y,ID):
        #ส่วนของการ resize ภาพ 
        '''im = Image.open(filename+'.png')
        im = im.resize((200,200))
        im.save(filename+'200x200.png')'''
        #จบส่วนของการ resize ภาพ 

        super().__init__() # filename+'200x200.png',0.25
        #self.texture = arcade.load_texture()
        self.scale = 0.25
        self.center_x = x
        self.center_y = y
        self.blockID = ID
        self.blockName = filename
        self.selected = False
        self.nearWall = False
        self.stop = True
        
    def __del__(self):
        print('del_block')
        #self.clear_spatial_hashes()
        #self.kill()
  

    #def draw_border(self):
    def update_animation(self):#, delta_time: float = 1 / 20):   
        if(self.selected):
          arcade.draw_rectangle_outline(self.center_x,
                         self.center_y,
                         52,
                         52,
                         arcade.color.ALMOND)  

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)
        arcade.set_background_color(arcade.color.AMAZON)
        
        


    def setup(self):
        # Set up your game here
        self.set_update_rate(1 / 60) 

        self.blockID = 0
        self.listBlockName = ['elephant','giraffe','hippo','monkey','panda','parrot','penguin','pig','rabbit','snake','snake']

        ###### load texture2dict ########
        self.listTexture = {}
        for i in self.listBlockName:
          t1 = arcade.load_texture(i+'200x200.png')
          self.listTexture[i] = t1
        ###### end load texture2dict 

        self.listXpos = [45,96,147,198,249,300,351,402,453,504,555]
        self.BlockM = [0,1,2,3,4,5,6,7,8,9,10]
        random.shuffle(self.BlockM)
        kIndex = self.BlockM[0:3]
        
        #use_spatial_hash=True เพิ่มความเร็วสำหรับการตรวจสอบการชน https://api.arcade.academy/en/latest/api/sprite_list.html
        self.listBlock = arcade.SpriteList(use_spatial_hash=True) #,lazy=True
     
        for i in kIndex:
          self.blockID+=1   
          r1 = block(self.listBlockName[i],x=self.listXpos[i],y=560,ID=self.blockID)
          r1.texture = self.listTexture[self.listBlockName[i]]
          self.listBlock.append(r1)
          #r1.change_y = -5
          del r1
       

        map_name = "map01_v2.json"

        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "wall_Layer": {
                "use_spatial_hash": True,
            },
        }

        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)
        self.wall_Layer = self.tile_map.sprite_lists["wall_Layer"]
        self.wall_Layer2 = self.tile_map.sprite_lists["wall_Layer2"]
        
        for i in range(len(self.wall_Layer)):
            self.wall_Layer[i].blockID = -1*(i+1000)
        
        self.timeNewBlock = 0
        self.StackRecordClick = []
        
        self.pixel1 = arcade.Sprite('1px.png')
        self.pixel1.blockID = -1 
        self.pixel1.add_spatial_hashes()
        #self.pixel1.clicked = False

        self.forscan_ = arcade.Sprite('1px.png')
        self.forscan_.blockID = -2 
        self.forscan_.add_spatial_hashes()
        

        
    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        
        self.wall_Layer.draw()
        self.wall_Layer2.draw()

        self.listBlock.draw()
    
        for i in self.listBlock:
            #i.draw()
            i.update_animation()
        
        self.pixel1.draw()
        arcade.draw_text(str(len(self.listBlock)), 10, 10, arcade.color.WHITE, 10)
    
    def randomNtoList(self,k,m):
        r = [] 
        while(len(r)<=m-1):
            c = random.randint(0,len(k)-1)
            if(not (k[c] in r)):
               r.append(k[c]) 
        
        del k 
        del m 
        return r 

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        
   
        #gc.collect()
        self.pixel1.update()
        
        #for i in self.listBlock:
        #    i.update()
        self.listBlock.update()
  
        
        #print ("Garbage collector: collected objects.", gc.get_count())
        
        #pass
        
        #print(delta_time)
        for i in self.listBlock:
            #i.update()
            i.center_y -=5

            '''hit_list = arcade.check_for_collision_with_list(i,self.wall_Layer)
            #c,o = self.checkCollisionWithList_v2(i,self.wall_Layer)
            #if(c):
            for k in hit_list:
                #i.change_y = 0
                i.stop = True
                i.center_y +=5
                i.nearWall = True
                #continue
                break # if version checkCollisionWithList_v2 ต้องมี break ด้วย
            
            del hit_list'''

            if(i.center_y<=45):
               i.center_y = 45
               i.stop = True 
               i.nearWall = True 
               
               continue #คือเจอฐานแล้ว
               #break


            #hit_listB2B = arcade.check_for_collision_with_list(i,self.listBlock)
            #for k in hit_listB2B:
            c,o = self.checkCollisionWithList_v2(i,self.listBlock)
            if(c):
               #i.change_y = 0
               i.stop = True
               i.center_y +=5
               i.nearWall = False
               
               
               continue 
               #break #ถ้าใช้ arcade.check_for_collision_with_list
             
            #del hit_listB2B
        
        self.timeNewBlock += delta_time
        if(self.timeNewBlock>3):
            self.timeNewBlock = 0
            self.createNewBlock()
             
             

        # ตรวจสอบการเรียงครบ more than 3 หรือเปล่า 
        #self.checkDone()
        
        #thr = threading.Thread(target=self.checkDone)
        #thr.start()
        #thr.join()
        self.checkDone()
         
        
        '''if(self.pixel1.clicked):
           c,o = self.checkCollisionWithList_v2(self.pixel1,self.listBlock)
           if(c):
              o.selected = True
              self.StackRecordClick.append(o)    
           print(self.pixel1.center_x,self.pixel1.center_y)
           self.pixel1.clicked = False ''' 
    
    def createNewBlock(self):
        #random.shuffle(self.BlockM)
        #random.shuffle(self.listBlockName)
        kIndex = self.BlockM[0:3]
           
        kIndex = self.randomNtoList(self.BlockM,3)
        BName = self.randomNtoList(self.listBlockName,3)
        #print(kIndex,BName)
        
        for i in range(len(kIndex)):
             self.blockID+=1
             #r1 = block(self.listBlockName[i],x=self.listXpos[i],y=560,ID=self.blockID)
             r1 = block(BName[i],x=self.listXpos[kIndex[i]],y=560,ID=self.blockID)
             r1.texture = self.listTexture[BName[i]]
             self.listBlock.append(r1)
             #r1.change_y = -5
             del r1
           
        del kIndex 
        del BName

    def checkDone(self):
          #print('checkProcess')
          cnt = 0
          nameMeet = '' 
       
          checkPass = False
          histID = [] 
          for i in range(len(self.listXpos)):
            #block = arcade.get_sprites_at_point((self.listXpos[i], 25), self.listBlock) #เฉพาะแถว y = 25
            self.forscan_.center_x = self.listXpos[i]
            self.forscan_.center_y = 25 

            #block = arcade.check_for_collision_with_list(self.forscan_,self.listBlock)
            block = self.forscan_.collides_with_list(self.listBlock)
          
            #c,block = self.checkCollisionWithList_v2(self.forscan_,self.listBlock)
            #if(c):

            if(len(block)>0):  #มีกล่อง
              if(nameMeet==''):
                 #nameMeet = block[0].blockName
                 nameMeet = block[0].blockName  
             
                 cnt = 1
                 histID = []
                 #histID.append(block[0].blockID)
                 histID.append(block[0].blockID)
              #elif(nameMeet==block[0].blockName):
              elif(nameMeet==block[0].blockName):    
                 cnt+=1
                 #histID.append(block[0].blockID)
                 histID.append(block[0].blockID)
              else:
                 if(cnt>=3):
                    #print(nameMeet,cnt,indexStart)  
                    checkPass = True
                    break #เลิกแล้ว เจอ case นึง
                 cnt = 1 
                 #nameMeet = block[0].blockName
                 nameMeet = block[0].blockName
            
                 histID = []
                 #histID.append(block[0].blockID)
                 histID.append(block[0].blockID)
          
          if(cnt>=3):
             #print(nameMeet,cnt,indexStart) 
             checkPass = True #เจอตอนจบ
        
          del block 

          ########
          if(checkPass==True):
             #print('passssss',histID)
             dCnt = 0
             for i in range(len(self.listBlock)-1,-1,-1):
               if(self.listBlock[i].blockID in histID):

                  #mk = self.listBlock.pop(i)
                  #del mk #delete object 
                  self.listBlock[i].remove_from_sprite_lists() 

                  #gc.collect()
                  dCnt+=1 
             
               if(dCnt==cnt):
                  break
            
          del histID
          #gc.collect()
          #print('checkProcessFinish')
        

    def checkCollisionWithList_v2(self,PosObject,ListObject):
        for i in ListObject:
            if(i.blockID!=PosObject.blockID):
              #c = arcade.check_for_collision(PosObject,i) 
          
              c = PosObject.collides_with_sprite(i)
          
              if(c):
                del c 
                return True,i 
        
        return False,PosObject
    
     
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.SPACE:
           '''   for i in range(len(self.listBlock)-1,-1,-1):
               k = self.listBlock.pop(i)
               del k'''
           for i in self.listBlock:
               i.remove_from_sprite_lists()       
                
             
    def on_mouse_release(self, x: int, y: int, button: int, modifiers: int):
        #thr = threading.Thread(target=self.mouse_releaseProcess,args=[x, y])
        #thr.start()
        #thr.join()
        self.mouse_releaseProcess(x,y)

    def mouse_releaseProcess(self, x: int, y: int):
        #self.pixel1.clicked = True

        '''self.pixel1.center_x = x 
        self.pixel1.center_y = y

        #c,o = self.checkCollisionWithList_v2(self.pixel1,self.listBlock)
        #if(c):
        c = arcade.check_for_collision_with_list(self.pixel1,self.listBlock)
        if(len(c)>0):
           #o.selected = True
           #self.StackRecordClick.append(o)    
           
           c[0].selected = True
           self.StackRecordClick.append(c[0]) '''

        
        c = arcade.get_sprites_at_point((x, y), self.listBlock)
        print('cccc',len(c))
        if(len(c)>0):
           #o.selected = True
           #self.StackRecordClick.append(o)    
           
           c[0].selected = True
           self.StackRecordClick.append(c[0]) 


        # ป้องกันกรณีเบิ้ลๆ คลิ๊กเข้ามา
        if(len(self.StackRecordClick)>2):
           self.StackRecordClick = list() 
        #pass 

        del c

        # เมื่อมีคลิ๊กเลือก 2 ตัวแล้ว
        if(len(self.StackRecordClick)==2):
           c1 = self.StackRecordClick[0]
           c2 = self.StackRecordClick[1] 
           #c1.blockName ,c2.blockName = c2.blockName , c1.blockName
           
           p,k = c1.center_x , c1.center_y
           c1.center_x ,c1.center_y =  c2.center_x,c2.center_y 
           c2.center_x ,c2.center_y = p,k
        
           c1.selected = False
           c2.selected = False
           

           #m1 = self.StackRecordClick.pop()
           #m2 = self.StackRecordClick.pop() 
           
           #del m1 
           #del m2
           del c1 
           del c2 
           self.StackRecordClick = list()

           #gc.collect()

        '''def on_mouse_press(self, x, y, button, key_modifiers):
        """ Called when the user presses a mouse button. """
        
        # Get list of block we've clicked on
        self.pixel1.center_x = x 
        self.pixel1.center_y = y   '''

    

        '''hit_listB2B = arcade.check_for_collision_with_list(self.pixel1,self.listBlock)
        for i in hit_listB2B:
          print(i.blockName,i.blockID)
          i.selected = True
          self.StackRecordClick.append(i) 
          break '''

        '''block = arcade.get_sprites_at_point((x, y), self.listBlock)
        if(len(block)>0):
           #print(block[0].blockID)
           block[0].selected = True
           self.StackRecordClick.append(block[0])
           return 0'''


def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    print(arcade.__version__)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()


