from turtle import width
import arcade
from PIL import Image

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)
    
    def setup(self):
        # Set up your game here
        
        im = Image.open('elephant.png')
        im = im.resize((200,200))
        im.save("elephant200x200.png")

        self.r1 = arcade.SpriteSolidColor(width=50,height=50,color=(0,0,0,0))
        self.r1.texture = arcade.load_texture('elephant200x200.png')
        self.r1.scale = 0.25
        self.r1.center_x = 300
        self.r1.center_y = 300

        im = Image.open('giraffe.png')
        im = im.resize((200,200))
        im.save("giraffe200x200.png")

        self.r2 = arcade.Sprite('giraffe200x200.png')
        self.r2.scale = 0.25
        self.r2.center_x = 351
        self.r2.center_y = 300

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.r1.draw()
        self.r2.draw()

        arcade.draw_rectangle_outline(300,
                         300,
                         52,
                         52,
                         arcade.color.ALMOND)
      

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
 

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
