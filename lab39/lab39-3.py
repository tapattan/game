import arcade
from PIL import Image
import random 

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class block(arcade.Sprite):
    def __init__(self,filename,x,y,ID):
        im = Image.open(filename+'.png')
        im = im.resize((200,200))
        im.save(filename+'200x200.png')

        super().__init__(filename+'200x200.png',0.25)
        #self.texture = arcade.load_texture()
        #self.scale = 0.25
        self.center_x = x
        self.center_y = y
        self.blockID = ID


    def draw_border(self):
        arcade.draw_rectangle_outline(self.center_x,
                         self.center_y,
                         52,
                         52,
                         arcade.color.ALMOND)  

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)
    
    def setup(self):
        # Set up your game here
        
        self.blockID = 0
        self.listBlockName = ['elephant','giraffe','hippo','monkey','panda','parrot','penguin','pig','rabbit','snake','snake']
        self.listXpos = [45,96,147,198,249,300,351,402,453,504,555]
        self.BlockM = [0,1,2,3,4,5,6,7,8,9,10]
        random.shuffle(self.BlockM)
        kIndex = self.BlockM[0:3]
        
        self.listBlock = arcade.SpriteList(use_spatial_hash=True) 
        for i in kIndex:
          self.blockID+=1   
          r1 = block(self.listBlockName[i],x=self.listXpos[i],y=560,ID=self.blockID)
          self.listBlock.append(r1)
          r1.change_y = -5
       

        map_name = "map01.json"

        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "wall_Layer": {
                "use_spatial_hash": True,
            },
        }
        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)
        self.wall_Layer = self.tile_map.sprite_lists["wall_Layer"]

        
        self.timeNewBlock = 0
        

    
    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.wall_Layer.draw()

        for i in self.listBlock:
          #i.draw_border()
          i.draw()


    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        #pass
        for i in self.listBlock:
            i.update()
            
            hit_list = arcade.check_for_collision_with_list(i,self.wall_Layer)
            for k in hit_list:
                i.change_y = 0
                i.center_y +=1
                break
            

            hit_listB2B = arcade.check_for_collision_with_list(i,self.listBlock)
            for k in hit_listB2B:
              i.change_y = 0
              i.center_y +=5
              break

        
        
        self.timeNewBlock += delta_time
        if(self.timeNewBlock>3):
           self.timeNewBlock = 0 
           random.shuffle(self.BlockM)
           random.shuffle(self.listBlockName)
           kIndex = self.BlockM[0:3]
        
           for i in kIndex:
             self.blockID+=1
             r1 = block(self.listBlockName[i],x=self.listXpos[i],y=560,ID=self.blockID)
             self.listBlock.append(r1)
             r1.change_y = -5
 

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
