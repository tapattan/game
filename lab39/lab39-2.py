import arcade
from PIL import Image
import random 

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class block(arcade.SpriteSolidColor):
    def __init__(self,filename,x,y):
        im = Image.open(filename+'.png')
        im = im.resize((200,200))
        im.save(filename+'200x200.png')

        super().__init__(width=50, height=50,color=(0,0,0,0))
        self.texture = arcade.load_texture(filename+'200x200.png')
        self.scale = 0.25
        self.center_x = x
        self.center_y = y

    def draw_border(self):
        arcade.draw_rectangle_outline(self.center_x,
                         self.center_y,
                         52,
                         52,
                         arcade.color.ALMOND)  

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)
    
    def setup(self):
        # Set up your game here
        
        listBlockName = ['elephant','giraffe','hippo','monkey','panda','parrot','penguin','pig','rabbit','snake']
        k = random.choices(listBlockName,k=6)
        print(k)
        self.listBlock = []
        for i in range(len(k)):
          r1 = block(k[i],x=100+(i*54),y=300)
          self.listBlock.append(r1)

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()

        for i in self.listBlock:
          i.draw_border()
          i.draw()


    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
 
 

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
