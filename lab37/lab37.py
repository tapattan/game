import arcade
import random 
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

 
class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)
        arcade.set_background_color(arcade.color.AMAZON)
        
    def setup(self):
        # Set up your game here
        pass
        self.apple = arcade.SpriteList(use_spatial_hash=True)
        for i in range(30):
          k = random.randint(3,10)/100
          b1 = arcade.Sprite('apple.png',k,hit_box_algorithm='Simple')
          b1.center_x = random.randint(50,550)
          b1.center_y = random.randint(200,550)
          self.apple.append(b1)
 
        map_name = "map01.json"

        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "wall_Layer": {
                "use_spatial_hash": True,
            } 
        }

        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)


        self.wall_Layer = self.tile_map.sprite_lists["wall_Layer"]
         

        self.physics_engine = arcade.PymunkPhysicsEngine(damping=0.7,
                                                         gravity=(0,-1000))
 
 
        self.physics_engine.add_sprite_list(self.apple,
                                            friction=0.1,
                                            mass=2,
                                            collision_type="apple")

        self.physics_engine.add_sprite_list(self.wall_Layer,
                                            friction=1,
                                            collision_type="wall",
                                            body_type=arcade.PymunkPhysicsEngine.STATIC)

        '''self.physics_engine.add_sprite_list(self.apple,
                                            body_type=arcade.PymunkPhysicsEngine.KINEMATIC)'''

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.wall_Layer.draw()
        self.apple.draw()
        
    
 
        
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.physics_engine.step()

 

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()