import arcade
import random
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class GameState1(arcade.View):
    def __init__(self):
        super().__init__()
        
        self.speed = -4
        
        self.car = arcade.Sprite('car2.png',0.3)
        self.car.center_x = 300
        self.car.center_y = 100
        
        self.road1 = arcade.Sprite('road.png',0.2)
        self.road1.center_y = 380 
        self.road1.center_x = 300
        self.road1.change_y = self.speed
        
        self.stone = arcade.Sprite('stone.png',0.5)
        self.stone.center_x = random.randint(245,355)
        self.stone.center_y = 620
        self.stone.change_y = self.speed
        
        self.tree = []
        for i in range(10):
          tree = arcade.Sprite('tree.png',0.3)
          tree.center_x = random.randint(30,100)
          tree.center_y = 50*(i+1)
          tree.change_y = self.speed
          self.tree.append(tree)
          
          tree = arcade.Sprite('tree.png',0.2)
          tree.center_x = random.randint(440,580)
          tree.center_y = 55*(i+1)
          tree.change_y = self.speed
          
          self.tree.append(tree)
          
        self.setup()
        
        
    def setup(self):

        pass 

           
    def on_show(self):
        arcade.set_background_color(arcade.color.APPLE_GREEN)
    
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        if(self.speed>0):
               self.speed = 0
               
        self.road1.update()
        self.stone.update()
        self.car.update()
        
        self.road1.change_y = self.speed
        self.stone.change_y = self.speed
        
        
        if(self.road1.center_y < 300):
           self.road1.center_y = 380
         
        for i in self.tree:
            i.update() 
            i.change_y = self.speed
            
            if(i.center_y<-5):
               i.center_y = 610
               
        if(self.car.center_x<240):
           self.car.center_x = 240 
        
        if(self.car.center_x>360):
               self.car.center_x = 360 
               
        if(self.stone.center_y < -20):
               self.stone.center_y = 620
               self.stone.center_x = random.randint(245,355)
               self.stone.scale = random.randint(10,30)/100
               
        
        
    def on_draw(self):
        arcade.start_render()
        self.road1.draw()
        self.stone.draw()
     
        
        for i in self.tree:
            i.draw() 
            
        self.car.draw()
 
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('GameState3', 10, 20, arcade.color.WHITE, 14)
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
 
        if key == arcade.key.LEFT:
            self.car.change_x = -5
        elif key == arcade.key.RIGHT:
            self.car.change_x = 5
           

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.car.change_x = 0 
        elif(key == arcade.key.UP):
            self.speed = self.speed - 1
        elif(key == arcade.key.DOWN):
            self.speed = self.speed  + 1
            

def main():
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = GameState1()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
