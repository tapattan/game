import arcade

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

MOVEMENT_SPEED = 2

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)
    
    def setup(self):
        # Set up your game here
        self.mySprite = arcade.Sprite('hero.png',0.05) #SCALING = 0.3 
        self.mySprite.center_x = 700 
        self.mySprite.center_y = 500
        #pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here
        self.mySprite.draw()

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass

        self.mySprite.update()
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.W:
            self.mySprite.change_y = MOVEMENT_SPEED  
        elif key == arcade.key.S:
            self.mySprite.change_y = -MOVEMENT_SPEED
        elif key == arcade.key.A:
            self.mySprite.change_x = -MOVEMENT_SPEED
        elif key == arcade.key.D:
            self.mySprite.change_x = MOVEMENT_SPEED
         

        #https://api.arcade.academy/en/development/api_docs/keyboard.html
        if modifiers & arcade.key.MOD_SHIFT and key == arcade.key.W:
            print('run2')
            self.mySprite.change_y = MOVEMENT_SPEED*4
        elif modifiers & arcade.key.MOD_SHIFT and key == arcade.key.S:
            print('run2')
            self.mySprite.change_y = -MOVEMENT_SPEED*4
        elif modifiers & arcade.key.MOD_SHIFT and key == arcade.key.A:
            print('run2')
            self.mySprite.change_x = -MOVEMENT_SPEED*4
        elif modifiers & arcade.key.MOD_SHIFT and key == arcade.key.D:
            print('run2')
            self.mySprite.change_x = MOVEMENT_SPEED*4

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.W or key == arcade.key.S:
            self.mySprite.change_y = 0
        elif key == arcade.key.A or key == arcade.key.D:
            self.mySprite.change_x = 0

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
