import arcade

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

class MyGame(arcade.Window):
    def __init__(self):
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "Flip Sprite Example")
        self.sprite = None

    def setup(self):

        filename = '../resources/sokobanpack/Character1.png'
        self.sprite = arcade.Sprite(filename, center_x=SCREEN_WIDTH // 2, center_y=SCREEN_HEIGHT // 2)

        self.texture01 = arcade.load_texture(filename, flipped_horizontally=False)
        self.texture02 = arcade.load_texture(filename, flipped_horizontally=True)

    def on_draw(self):
        arcade.start_render()
        self.sprite.draw()

    def on_key_press(self, key, modifiers):
        if key == arcade.key.A:
            # พลิปสไปร์ตแนวนอน
            self.sprite.texture = self.texture01
        
        if key == arcade.key.D:
            # พลิปสไปร์ตแนวนอน
            self.sprite.texture = self.texture02

def main():
    game = MyGame()
    game.setup()
    arcade.run()

if __name__ == "__main__":
    main()