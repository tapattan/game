import arcade

SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500
MOVEMENT_SPEED = 5

class hero(arcade.Sprite):
    def __init__(self):

        # Set up parent class
        super().__init__()
        
 
        self.texturesDown = []
        self.texturesDown.append(arcade.load_texture("../resources/sokobanpack/Character4.png"))
        self.texturesDown.append(arcade.load_texture("../resources/sokobanpack/Character5.png"))
        self.texturesDown.append(arcade.load_texture("../resources/sokobanpack/Character6.png"))
        
        self.textureList = self.texturesDown 
        self.statusFrameaction = len(self.texturesDown)
        print(self.statusFrameaction)


        self.timeFrame = 0
        self.timeKFrame = 0
        self.texture = self.textureList[self.timeFrame]
        self.center_x = 100
        self.center_y = 300
        self.scale = 1

        self.texturesLeft = []
        self.texturesLeft.append(arcade.load_texture("../resources/sokobanpack/Character1.png"))
        self.texturesLeft.append(arcade.load_texture("../resources/sokobanpack/Character10.png"))
        
        self.texturesRight = []
        self.texturesRight.append(arcade.load_texture("../resources/sokobanpack/Character2.png"))
        self.texturesRight.append(arcade.load_texture("../resources/sokobanpack/Character3.png"))
        
        self.texturesUp = []
        self.texturesUp.append(arcade.load_texture("../resources/sokobanpack/Character7.png"))
        self.texturesUp.append(arcade.load_texture("../resources/sokobanpack/Character8.png"))
        self.texturesUp.append(arcade.load_texture("../resources/sokobanpack/Character9.png"))
        

    def update_animation(self, delta_time: float = 1 / 20):
        self.statusFrameaction = len(self.textureList)
        self.timeKFrame +=(delta_time)
        if(self.timeKFrame>0.2):
            self.timeKFrame=0
            self.timeFrame+=1
            if(self.timeFrame%self.statusFrameaction==0):
                self.timeFrame = 0
            
            try:
              self.texture = self.textureList[self.timeFrame]
            except:
              pass

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        pass

        self.hero = hero()

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here

        self.hero.draw()

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.hero.update()
        self.hero.update_animation()

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP or key == arcade.key.W:
            self.hero.change_y = MOVEMENT_SPEED
            self.hero.textureList = self.hero.texturesUp
        elif key == arcade.key.DOWN or key == arcade.key.S:
            self.hero.change_y = -MOVEMENT_SPEED
            self.hero.textureList = self.hero.texturesDown
        elif key == arcade.key.LEFT or key == arcade.key.A:
            self.hero.change_x = -MOVEMENT_SPEED
            self.hero.textureList = self.hero.texturesLeft
        elif key == arcade.key.RIGHT or key == arcade.key.D:
            self.hero.change_x = MOVEMENT_SPEED
            self.hero.textureList = self.hero.texturesRight

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP or key == arcade.key.DOWN or key == arcade.key.W or key == arcade.key.S:
            self.hero.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT or key == arcade.key.A or key == arcade.key.D:
            self.hero.change_x = 0

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()