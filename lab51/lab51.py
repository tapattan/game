import arcade
import numpy as np
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600


class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        pass
        self.ball = arcade.Sprite('ball.png')
        self.ball.center_x = 50
        self.ball.center_y = 100
        self.ball.scale = 0.05
        self.ball.change_angle = 5

        self.goal = arcade.Sprite('goal.png')
        self.goal.center_x = 680
        self.goal.center_y = 200
        self.goal.scale = 0.1
        
      
        #Linear Bézier curves
        t = np.arange(0,151)/150
        P0x,P0y = 50,100
        P1x,P1y = 680,200
        self.Bx = P0x + t*(P1x-P0x)
        self.By = P0y + t*(P1y-P0y)

       
        #Quadratic Bézier curves
        t = np.arange(0,151)/150
        P0x,P0y = 50,100
        P1x,P1y = 500,500
        P2x,P2y = 700,200

        self.Bx2 = P1x + ((1-t)**2)*(P0x-P1x)+((t**2)*(P2x-P1x))
        self.By2 = P1y + ((1-t)**2)*(P0y-P1y)+((t**2)*(P2y-P1y))
      
        self.timecurve = 0
        self.type_ = 1
        self.type_p = 1
        self.done = False

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here
        self.goal.draw()
        self.ball.draw()
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.ball.update()
        if(self.type_==1):
          self.ball.center_x = self.Bx[self.timecurve]
          self.ball.center_y = self.By[self.timecurve]
          self.done = False
        else:
          self.ball.center_x = self.Bx2[self.timecurve]
          self.ball.center_y = self.By2[self.timecurve]  
          self.done = False

        self.timecurve+=1

        if(self.timecurve==len(self.Bx)):
           self.timecurve=0 
           self.done = True

        if(self.done==True):
           self.type_ = self.type_p   
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.W:
           self.type_p = 1
        elif key == arcade.key.S:
           self.type_p = 2
    


def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
