import arcade
import random 
Adjustment_to_one_lane = 400
SCREEN_WIDTH = 1300 - Adjustment_to_one_lane
SCREEN_HEIGHT = 750

class MyGame(arcade.View):
    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()
    
    def setup(self):
        
        self.sprite_list = arcade.SpriteList()
        self.lane = arcade.Sprite('Lane.png')
        self.lane.center_x = 218
        self.lane.center_y = 290
        self.lane.scale = 1.22

        self.sprite_list.append(self.lane)

        self.lane2 = arcade.Sprite('Lane.png')
        self.lane2.center_x = 618
        self.lane2.center_y = 290
        self.lane2.scale = 1.22

        self.pin1 = arcade.Sprite('Pin.png')
        self.pin1.center_x = 183
        self.pin1.center_y = 510-5
        self.pin1.scale = 0.35

        #self.sprite_list.append(self.pin1)

        self.pin2 = arcade.Sprite('Pin.png')
        self.pin2.center_x = 213
        self.pin2.center_y = 520
        self.pin2.scale = 0.35
        #self.sprite_list.append(self.pin2)

        self.pin3 = arcade.Sprite('Pin.png')
        self.pin3.center_x = 153
        self.pin3.center_y = 520
        self.pin3.scale = 0.35
        #self.sprite_list.append(self.pin3)

        self.pin4 = arcade.Sprite('Pin.png')
        self.pin4.center_x = 126
        self.pin4.center_y = 535
        self.pin4.scale = 0.35
        #self.sprite_list.append(self.pin4)

        self.pin5 = arcade.Sprite('Pin.png')
        self.pin5.center_x = 240
        self.pin5.center_y = 535
        self.pin5.scale = 0.35
        #self.sprite_list.append(self.pin5)

        self.pin6 = arcade.Sprite('Pin.png')
        self.pin6.center_x = 183#+100
        self.pin6.center_y = 520
        self.pin6.scale = 0.35
        #self.sprite_list.append(self.pin6)

        self.pin7 = arcade.Sprite('Pin.png')
        self.pin7.center_x = 213
        self.pin7.center_y = 535
        self.pin7.scale = 0.35
        #self.sprite_list.append(self.pin7)

        self.pin8 = arcade.Sprite('Pin.png')
        self.pin8.center_x = 153
        self.pin8.center_y = 535
        self.pin8.scale = 0.35
        #self.sprite_list.append(self.pin8)

        self.pin9 = arcade.Sprite('Pin.png')
        self.pin9.center_x = 183
        self.pin9.center_y = 535
        self.pin9.scale = 0.35
        #self.sprite_list.append(self.pin9)


        self.sprite_list.append(self.pin7)
        self.sprite_list.append(self.pin8)
        self.sprite_list.append(self.pin9)
        self.sprite_list.append(self.pin4)
        self.sprite_list.append(self.pin5)
        self.sprite_list.append(self.pin6)
        self.sprite_list.append(self.pin2)
        self.sprite_list.append(self.pin3)
        self.sprite_list.append(self.pin1)

        ########################## Lane 2 Pins ##################################################################3

        self.pin11 = arcade.Sprite('Pin.png')
        self.pin11.center_x = 183+400
        self.pin11.center_y = 510-5
        self.pin11.scale = 0.35

        self.pin12 = arcade.Sprite('Pin.png')
        self.pin12.center_x = 213+400
        self.pin12.center_y = 520
        self.pin12.scale = 0.35

        self.pin13 = arcade.Sprite('Pin.png')
        self.pin13.center_x = 153+400
        self.pin13.center_y = 520
        self.pin13.scale = 0.35

        self.pin14 = arcade.Sprite('Pin.png')
        self.pin14.center_x = 126+400
        self.pin14.center_y = 535
        self.pin14.scale = 0.35

        self.pin15 = arcade.Sprite('Pin.png')
        self.pin15.center_x = 240+400
        self.pin15.center_y = 535
        self.pin15.scale = 0.35

        self.pin16 = arcade.Sprite('Pin.png')
        self.pin16.center_x = 183+400
        self.pin16.center_y = 520
        self.pin16.scale = 0.35

        self.pin17 = arcade.Sprite('Pin.png')
        self.pin17.center_x = 213+400
        self.pin17.center_y = 535
        self.pin17.scale = 0.35

        self.pin18 = arcade.Sprite('Pin.png')
        self.pin18.center_x = 153+400
        self.pin18.center_y = 535
        self.pin18.scale = 0.35

        self.pin19 = arcade.Sprite('Pin.png')
        self.pin19.center_x = 183+400
        self.pin19.center_y = 535
        self.pin19.scale = 0.35

#################################################################################################
        
        self.adjustment_to_one_lane = Adjustment_to_one_lane 

        self.ball1 = arcade.Sprite('Ball.png')
        self.ball1.center_x = 185 
        self.ball1.center_y = 120
        self.ball1.scale = 0.27
        self.sprite_list.append(self.ball1)

        
        self.bar = arcade.Sprite('Bar.png')
        self.bar.center_x = 1050 - self.adjustment_to_one_lane
        self.bar.center_y = 630
        self.bar.scale = 0.2
        self.sprite_list.append(self.bar)

        self.arrow = arcade.Sprite('Arrow.png')
        self.arrow.center_x = 1050 - self.adjustment_to_one_lane
        self.arrow.center_y = 685
        self.arrow.scale = 0.2
        self.sprite_list.append(self.arrow)

        self.direct_arrow = -1
        self.arrow_status = 'YES'

        self.arrow2 = arcade.Sprite('Arrow.png')
        self.arrow2.center_x = 1050 - self.adjustment_to_one_lane
        self.arrow2.center_y = 555
        self.arrow2.scale = 0.2
        self.sprite_list.append(self.arrow2)

        self.speed = 1
        self.speedtime = 0

        self.direct_arrow2 = -1
        self.arrow2_status = 'YES'

        self.bar2 = arcade.Sprite('Bar.png')
        self.bar2.center_x = 1050  - self.adjustment_to_one_lane
        self.bar2.center_y = 500
        self.bar2.scale = 0.2
        self.sprite_list.append(self.bar2)

       

        self.position_ball = 0 #1050
        self.force_ball = 0 
        self.timemovefirstBall = 0

        self.destination_x = 0
        self.destination_y = 0


        self.pin1MovementTime = 0
        self.pin1HitBall = False

        self.pin2MovementTime = 0
        self.pin2HitBall = False 

        self.pin3MovementTime = 0
        self.pin3HitBall = False 

        self.pin4MovementTime = 0
        self.pin4HitBall = False 

        self.pin5MovementTime = 0
        self.pin5HitBall = False 

        self.pin6MovementTime = 0
        self.pin6HitBall = False 

        self.pin7MovementTime = 0
        self.pin7HitBall = False 

        self.pin8MovementTime = 0
        self.pin8HitBall = False 

        self.pin9MovementTime = 0
        self.pin9HitBall = False 
        
        self.basket_pin = [self.pin1,self.pin2,self.pin3,self.pin4,self.pin5,self.pin6,self.pin7,self.pin8,self.pin9]

        self.basket_movement = [self.pin1MovementTime,self.pin2MovementTime,self.pin3MovementTime,
                           self.pin4MovementTime,self.pin5MovementTime,self.pin6MovementTime,
                           self.pin7MovementTime,self.pin8MovementTime,self.pin9MovementTime]
        
        self.basket_pinHitBall = [self.pin1HitBall,self.pin2HitBall,self.pin3HitBall,self.pin4HitBall,self.pin5HitBall,self.pin6HitBall,
                           self.pin7HitBall,self.pin8HitBall,self.pin9HitBall]
        

        for i in self.basket_pin:
           i.kd = random.choice([-1,1])

        pass

    def on_show(self):
        arcade.set_background_color(arcade.color.AIR_SUPERIORITY_BLUE)

    def on_draw(self):
        self.clear()
        #self.lane.draw()

        #self.pin7.draw() #left row3
        #self.pin8.draw() # left2 row3
        #self.pin9.draw() #left3 row3
        #self.pin4.draw() #left1 row3 **********
        #self.pin5.draw() #left5 row3
        #self.pin6.draw()
        #self.pin2.draw()
        #self.pin3.draw()
        #self.pin1.draw()
        


################## Lane 2 ###################################

        '''self.lane2.draw()
        self.pin17.draw()
        self.pin18.draw()
        self.pin19.draw()
        self.pin14.draw()
        self.pin15.draw()
        self.pin16.draw()
        self.pin12.draw()
        self.pin13.draw()
        self.pin11.draw()'''

#################################################################

        #self.ball1.draw()
        #self.bar.draw()
        #self.arrow.draw()
        #self.arrow2.draw()
        #self.bar2.draw()

        self.sprite_list.draw()

        arcade.draw_line(start_x=850-self.adjustment_to_one_lane, start_y=1400,end_x=850-self.adjustment_to_one_lane,end_y=-100, color=arcade.color.RED, line_width=10)
        pass 
    
    def on_update(self, delta_time):
        pass
        
        ################ zone control arrow ############
        
        # ควบคุม ลูกศรเลื่อนซ้ายและขวา
        if(self.arrow_status=='YES'):
          self.arrow.center_x = (self.arrow.center_x) + (self.direct_arrow*2)
          if(self.arrow.center_x < (930 - self.adjustment_to_one_lane)):
             self.direct_arrow = self.direct_arrow*-1
          if(self.arrow.center_x > (1170 - self.adjustment_to_one_lane )):
             self.direct_arrow = self.direct_arrow*-1

        # ควบคุม ลูกศรเลื่อนซ้ายและขวา
        if(self.arrow2_status=='YES'):
          self.arrow2.center_x = (self.arrow2.center_x ) + (self.direct_arrow2*4)
          if(self.arrow2.center_x < (930 - self.adjustment_to_one_lane  )):
             self.direct_arrow2 = self.direct_arrow2*-1
          if(self.arrow2.center_x > (1170 - self.adjustment_to_one_lane )):
             self.direct_arrow2 = self.direct_arrow2*-1

        

        if( self.arrow2_status == 'NO' and self.arrow_status == 'NO' ):
            px = (self.arrow.center_x )
            if(px>=650  ): #pin right  650 เป็นตรงกลางของบาร์
               px = self.ball1.center_x + (px - 650   )
            else: #pin left
               px = self.ball1.center_x - (650 - px )


            #BT = (1-t)*P0 +t*P1


            speed = self.arrow2.center_x  
            print(self.bar2.center_x,speed) 
            if(speed>750  ):
               self.speed = 0.005
               for i in range(len(self.basket_movement)): 
                 self.basket_movement[i] = 1.5
            elif(speed> 650  ):
               self.speed = 0.010
               for i in range(len(self.basket_movement)): 
                 self.basket_movement[i] = 1
            elif(speed> 600  ):
               self.speed = 0.020
               for i in range(len(self.basket_movement)): 
                 self.basket_movement[i] = 0.75
            elif(speed> 550 ):
               self.speed = 0.040
               for i in range(len(self.basket_movement)): 
                 self.basket_movement[i] = 0.5
            else:
               self.speed = 0.050
               for i in range(len(self.basket_movement)): 
                 self.basket_movement[i] = 0.2

            self.speedtime += delta_time
            if(self.speedtime >= self.speed):
              
              self.timemovefirstBall +=(delta_time/50)
              btx = (1-self.timemovefirstBall)*self.ball1.center_x +  self.timemovefirstBall*px 
              bty = (1-self.timemovefirstBall)*self.ball1.center_y +  self.timemovefirstBall*1000  


              #print(type(self.ball1.scale),self.ball1.scale)
              #self.ball1.scale = self.ball1.scale*0.9

              new_scale = (self.ball1.scale[0] * 0.994, self.ball1.scale[1] * 0.994)  #######-----version3.0
              self.ball1.scale = new_scale



              self.ball1.center_y = bty
              self.ball1.center_x = btx
              self.speedtime = 0 
              self.speed = 0 

              

              if(self.ball1.center_y>(600-10)):
                self.arrow_status = 'ACTION'
                self.arrow2_status = 'ACTION'
                self.timemovefirstBall = 0

                #print('**',btx,bty,self.arrow_status,self.arrow2_status , self.ball1.center_y)

        
        for i in range(len(self.basket_pin)): 
            g = arcade.check_for_collision( self.ball1, self.basket_pin[i])
            if(g):
                self.basket_pinHitBall[i] = True 
                #print('HIT',i)
                #self.basket_pin[i].kd = random.choice([-1,1])

                #for i in range(len(self.basket_movement)):
                   #print(self.basket_movement[i],self.basket_pinHitBall[i])


        for i in range(len(self.basket_pin)): 
            if(i!=3):
              if(self.basket_pinHitBall[i] == True ):
                  g = arcade.check_for_collision(self.basket_pin[i], self.pin4) #pin4
                  if(g):
                     self.basket_pinHitBall[3] = True  
                     #print('HIT',i)
                     #self.basket_pin[i].kd = random.choice([-1,1])

                     #for i in range(len(self.basket_movement)):
                     #   print(self.basket_movement[i],self.basket_pinHitBall[i]) 

        for i in range(len(self.basket_pin)): 
            if(i!=4):
              if(self.basket_pinHitBall[i] == True ):
                  g = arcade.check_for_collision(self.basket_pin[i], self.pin5) #pin5
                  if(g):
                     self.basket_pinHitBall[4] = True 
                     #print('HIT',i)
                     #self.basket_pin[i].kd = random.choice([-1,1])

                     #for i in range(len(self.basket_movement)):
                     #   print(self.basket_movement[i],self.basket_pinHitBall[i])                           
        
        

        for i in range(len(self.basket_pin)): 
            if(self.basket_pinHitBall[i] and self.basket_movement[i]>0): 
                    self.basket_pin[i].update()
                    self.basket_movement[i] -= delta_time
                    self.basket_pin[i].change_x = 1*self.basket_movement[i]*self.basket_pin[i].kd
                    self.basket_pin[i].change_y = 1*self.basket_movement[i] 
                    self.basket_pin[i].change_angle = 1*self.basket_pin[i].kd
                
            if(self.basket_movement[i]<=0):
                    self.basket_pin[i].change_x = 0
                    self.basket_pin[i].change_y = 0
                    self.basket_pin[i].change_angle = 0



        '''if( self.arrow2_status == 'NO' and self.arrow_status == 'NO' ):
            #print(self.arrow.center_x) #Left Right Ball 185   center Pin 1050
            px = self.arrow.center_x
            if(px>=1050): #pin right
              px = px - 1050
              self.ball1.center_x = self.ball1.center_x + (px/100)
              self.timemovefirstBall += (px/100)
              if(self.timemovefirstBall>=px):
                #protect ball move multi time  
                self.arrow_status = 'ACTION'
                self.arrow2_status = 'ACTION'
                self.timemovefirstBall = 0
            elif(px<1050): #pin left
              px = 1050 - px
              self.ball1.center_x = self.ball1.center_x - (px/100)
              self.timemovefirstBall += (px/100)
              if(self.timemovefirstBall>=px):
                #protect ball move multi time  
                self.arrow_status = 'ACTION'
                self.arrow2_status = 'ACTION'
                self.timemovefirstBall = 0'''
        
        
            
            
           
             
                
        



 
    
    def on_key_press(self, key, modifiers):            
        pass

    def on_key_release(self,key,modifiers):
        #print('1')
        if(key==arcade.key.SPACE and self.arrow_status=='YES' and self.arrow2_status=='YES'):
           self.arrow_status = 'NO'
           #print('2')
        
        elif(key==arcade.key.SPACE and self.arrow_status=='NO' and self.arrow2_status=='YES'):
           self.arrow2_status = 'NO'
           #print('3') 
           #print(self.arrow.center_x) 

        elif(key==arcade.key.SPACE and self.arrow_status=='NO' and self.arrow2_status=='NO'):
           self.arrow2_status = 'YES'
           self.arrow_status = 'YES'  
           self.ball1.center_x = 185 
           self.ball1.center_y = 120
           self.ball1.scale = 0.27

        elif(key==arcade.key.SPACE and self.arrow_status=='ACTION' and self.arrow2_status=='ACTION'):
           # come back to start point 
           self.arrow2_status = 'YES'
           self.arrow_status = 'YES'  

           self.ball1.center_x = 185 
           self.ball1.center_y = 120
           self.ball1.scale = 0.27

           self.pin1.center_x = 183
           self.pin1.center_y = 510-5

           self.pin2.center_x = 213
           self.pin2.center_y = 520

           self.pin3.center_x = 153
           self.pin3.center_y = 520

           self.pin4.center_x = 126
           self.pin4.center_y = 535

           self.pin5.center_x = 240
           self.pin5.center_y = 535

           self.pin6.center_x = 183#+100
           self.pin6.center_y = 520

           self.pin7.center_x = 213
           self.pin7.center_y = 535
   
           self.pin8.center_x = 153
           self.pin8.center_y = 535

           self.pin9.center_x = 183
           self.pin9.center_y = 535
           
           for i in range(len(self.basket_pin)):
             self.basket_pin[i].angle = 0
             
           for i in range(len(self.basket_pinHitBall)):
             self.basket_pinHitBall[i] = False

           for i in range(len(self.basket_movement)):
              self.basket_movement[i] = 0

           for i in self.basket_pin:
             i.kd = random.choice([-1,1])   

           

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "BowlingArena")
    game = MyGame() 
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()