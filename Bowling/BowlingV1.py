import arcade
import random 
SCREEN_WIDTH = 1300
SCREEN_HEIGHT = 750

class MyGame(arcade.View):
    def __init__(self):
        super().__init__()
        self.setup()
    
    def setup(self):

        self.lane = arcade.Sprite('Lane.png')
        self.lane.center_x = 218
        self.lane.center_y = 290
        self.lane.scale = 1.22

        self.lane2 = arcade.Sprite('Lane.png')
        self.lane2.center_x = 618
        self.lane2.center_y = 290
        self.lane2.scale = 1.22

        self.pin1 = arcade.Sprite('Pin.png')
        self.pin1.center_x = 183
        self.pin1.center_y = 510-5
        self.pin1.scale = 0.35

        self.pin2 = arcade.Sprite('Pin.png')
        self.pin2.center_x = 213
        self.pin2.center_y = 520
        self.pin2.scale = 0.35

        self.pin3 = arcade.Sprite('Pin.png')
        self.pin3.center_x = 153
        self.pin3.center_y = 520
        self.pin3.scale = 0.35

        self.pin4 = arcade.Sprite('Pin.png')
        self.pin4.center_x = 126
        self.pin4.center_y = 535
        self.pin4.scale = 0.35

        self.pin5 = arcade.Sprite('Pin.png')
        self.pin5.center_x = 240
        self.pin5.center_y = 535
        self.pin5.scale = 0.35

        self.pin6 = arcade.Sprite('Pin.png')
        self.pin6.center_x = 183#+100
        self.pin6.center_y = 520
        self.pin6.scale = 0.35

        self.pin7 = arcade.Sprite('Pin.png')
        self.pin7.center_x = 213
        self.pin7.center_y = 535
        self.pin7.scale = 0.35

        self.pin8 = arcade.Sprite('Pin.png')
        self.pin8.center_x = 153
        self.pin8.center_y = 535
        self.pin8.scale = 0.35

        self.pin9 = arcade.Sprite('Pin.png')
        self.pin9.center_x = 183
        self.pin9.center_y = 535
        self.pin9.scale = 0.35

        ########################## Lane 2 Pins ##################################################################3

        self.pin11 = arcade.Sprite('Pin.png')
        self.pin11.center_x = 183+400
        self.pin11.center_y = 510-5
        self.pin11.scale = 0.35

        self.pin12 = arcade.Sprite('Pin.png')
        self.pin12.center_x = 213+400
        self.pin12.center_y = 520
        self.pin12.scale = 0.35

        self.pin13 = arcade.Sprite('Pin.png')
        self.pin13.center_x = 153+400
        self.pin13.center_y = 520
        self.pin13.scale = 0.35

        self.pin14 = arcade.Sprite('Pin.png')
        self.pin14.center_x = 126+400
        self.pin14.center_y = 535
        self.pin14.scale = 0.35

        self.pin15 = arcade.Sprite('Pin.png')
        self.pin15.center_x = 240+400
        self.pin15.center_y = 535
        self.pin15.scale = 0.35

        self.pin16 = arcade.Sprite('Pin.png')
        self.pin16.center_x = 183+400
        self.pin16.center_y = 520
        self.pin16.scale = 0.35

        self.pin17 = arcade.Sprite('Pin.png')
        self.pin17.center_x = 213+400
        self.pin17.center_y = 535
        self.pin17.scale = 0.35

        self.pin18 = arcade.Sprite('Pin.png')
        self.pin18.center_x = 153+400
        self.pin18.center_y = 535
        self.pin18.scale = 0.35

        self.pin19 = arcade.Sprite('Pin.png')
        self.pin19.center_x = 183+400
        self.pin19.center_y = 535
        self.pin19.scale = 0.35

#################################################################################################

        self.ball1 = arcade.Sprite('Ball.png')
        self.ball1.center_x = 185
        self.ball1.center_y = 120
        self.ball1.scale = 0.27

        
        self.bar = arcade.Sprite('Bar.png')
        self.bar.center_x = 1050
        self.bar.center_y = 630
        self.bar.scale = 0.2

        self.arrow = arcade.Sprite('Arrow.png')
        self.arrow.center_x = 1050
        self.arrow.center_y = 685
        self.arrow.scale = 0.2

        self.direct_arrow = -1
        self.arrow_status = 'YES'

        self.arrow2 = arcade.Sprite('Arrow.png')
        self.arrow2.center_x = 1050
        self.arrow2.center_y = 555
        self.arrow2.scale = 0.2

        self.direct_arrow2 = -1
        self.arrow2_status = 'YES'

        self.bar2 = arcade.Sprite('Bar.png')
        self.bar2.center_x = 1050
        self.bar2.center_y = 500
        self.bar2.scale = 0.2
       
      
        pass

    def on_show(self):
        arcade.set_background_color(arcade.color.AIR_SUPERIORITY_BLUE)

    def on_draw(self):
        arcade.start_render()
        self.lane.draw()
        self.pin7.draw()
        self.pin8.draw()
        self.pin9.draw()
        self.pin4.draw()
        self.pin5.draw()
        self.pin6.draw()
        self.pin2.draw()
        self.pin3.draw()
        self.pin1.draw()


################## Lane 2 ###################################

        self.lane2.draw()
        self.pin17.draw()
        self.pin18.draw()
        self.pin19.draw()
        self.pin14.draw()
        self.pin15.draw()
        self.pin16.draw()
        self.pin12.draw()
        self.pin13.draw()
        self.pin11.draw()

#################################################################

        self.ball1.draw()
        self.bar.draw()
        self.arrow.draw()
        self.arrow2.draw()
        self.bar2.draw()

        arcade.draw_line(start_x=850, start_y=1400,end_x=850,end_y=-100, color=arcade.color.RED, line_width=10)
        pass 
    
    def update(self, delta_time):
        pass
        
        ################ zone control arrow ############
        
        if(self.arrow_status=='YES'):
          self.arrow.center_x = self.arrow.center_x + (self.direct_arrow*2)
          if(self.arrow.center_x < 930):
             self.direct_arrow = self.direct_arrow*-1
          if(self.arrow.center_x > 1170):
             self.direct_arrow = self.direct_arrow*-1


        if(self.arrow2_status=='YES'):
          self.arrow2.center_x = self.arrow2.center_x + (self.direct_arrow2*4)
          if(self.arrow2.center_x < 930):
             self.direct_arrow2 = self.direct_arrow2*-1
          if(self.arrow2.center_x > 1170):
             self.direct_arrow2 = self.direct_arrow2*-1



 
    
    def on_key_press(self, key, modifiers):            
        pass

    def on_key_release(self,key,modifiers):
        print('1')
        if(key==arcade.key.SPACE and self.arrow_status=='YES' and self.arrow2_status=='YES'):
           self.arrow_status = 'NO'
           print('2')
        
        elif(key==arcade.key.SPACE and self.arrow_status=='NO' and self.arrow2_status=='YES'):
           self.arrow2_status = 'NO'
           print('3') 

        elif(key==arcade.key.SPACE and self.arrow_status=='NO' and self.arrow2_status=='NO'):
           self.arrow2_status = 'YES'
           self.arrow_status = 'YES'   

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "BowlingArena")
    game = MyGame() 
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()