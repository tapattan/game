import arcade
import random 
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class GameState2(arcade.View):
    def __init__(self):
        super().__init__()
        self.score = 0
        self.setup()
    
    def setup(self):
        pass

    def on_show(self):
        arcade.set_background_color(arcade.color.BLUE)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('GameState2 : '+str(self.score), 10, 20, arcade.color.WHITE, 14)

class GameState1(arcade.View):
    def __init__(self):
        super().__init__()
        self.setup()
        self.score  = 0
    
    def setup(self):
        pass
         
         
    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('GameState1 : '+str(self.score), 10, 20, arcade.color.WHITE, 14)
    
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        print('state1',self.score)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.SPACE:
            self.score = self.score + 100
            game_view = GameState2()
            game_view.score = self.score
            self.window.show_view(game_view)
        

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass
 

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()


    def setup(self):
        self.sun = arcade.Sprite('../resources/sun.png')
        self.sun.center_x = 300
        self.sun.center_y = 300

        self.score = 0

        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()

        self.sun.draw()
         
        arcade.draw_text('main', 10, 20, arcade.color.WHITE, 14)
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """

        pass

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.SPACE:
            
            self.score = 100
            game_view = GameState1()
            game_view.score = self.score 
            self.window.show_view(game_view)
        

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass


def main():
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    window.total_score = 0
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()