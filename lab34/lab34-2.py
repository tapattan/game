import arcade
import random 

SCREEN_WIDTH = 300
SCREEN_HEIGHT = 300

MOVEMENT_SPEED = 1

class body_box(arcade.SpriteSolidColor):
      def __init__(self,x,y):
        super().__init__(10,10,arcade.color.BLACK)
        self.center_x = x
        self.center_y = y
        self.directPrev = 'R'
        self.direct = 'R'

class food_snake(arcade.SpriteCircle):  
      def __init__(self,x,y):
        super().__init__(radius=5, color=arcade.color.GOLD)
        self.center_x = x
        self.center_y = y
        

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)
        arcade.set_background_color(arcade.color.AMAZON)
    
    def setup(self):
        # Set up your game here
        self.snake = arcade.SpriteList()

        box1 = body_box(50,100)
        self.snake.append(box1)

        box2 = body_box(50-12,100)
        self.snake.append(box2)
        
        box2 = body_box(50-24,100)
        self.snake.append(box2)

        self.timesnake = 0

        self.food = food_snake(200,200)
        self.speed = 9
  
   
    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here
        self.snake.draw()
        self.food.draw()

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.snake.update()
        
        ########## ส่วนควบคุมการเคลื่อนที่ 
        #ควบคุมการเคลื่อนที่จากหลังไปหน้า ให้ลำตัวสุดท้ายค่อยๆ ขยับตาม
        self.timesnake+=delta_time 
        if(self.timesnake>1-(self.speed/10)):
           self.timesnake = 0 
           for i in range(len(self.snake)-1,-1,-1):
            print(i)
            if(i==0):
              if(self.snake[i].direct=='R'):
                 self.snake[i].center_x +=12
              elif(self.snake[i].direct=='U'):
                 self.snake[i].center_y +=12 
              elif(self.snake[i].direct=='L'):
                 self.snake[i].center_x -=12
              elif(self.snake[i].direct=='D'):
                 self.snake[i].center_y -=12       
            else: 
                 self.snake[i].center_x = self.snake[i-1].center_x 
                 self.snake[i].center_y = self.snake[i-1].center_y 
        ############ end zone ###############

        ######## zone เช็คกินอาหาร ###############
        if(arcade.check_for_collision(self.snake[0],self.food)):
             x = self.snake[len(self.snake)-1].center_x
             y = self.snake[len(self.snake)-1].center_y
             box_new = body_box(x,y)
             self.snake.append(box_new)

             self.food.center_x = random.randint(20,SCREEN_WIDTH)
             self.food.center_y = random.randint(20,SCREEN_HEIGHT)
             
             self.speed+=1
     
             if(self.speed>=10):
                self.speed = 9


    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        
        self.snake[0].directPrev = self.snake[0].direct

        if key == arcade.key.W:
            self.snake[0].direct = 'U'
        elif key == arcade.key.S:
            self.snake[0].direct = 'D'
        elif key == arcade.key.A:
            self.snake[0].direct = 'L'
        elif key == arcade.key.D:
            self.snake[0].direct = 'R' 
        
       
    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass
 

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
