import arcade
import random 

SCREEN_WIDTH = 300
SCREEN_HEIGHT = 300

MOVEMENT_SPEED = 1

class body_box(arcade.SpriteSolidColor):
      def __init__(self,x,y):
        super().__init__(10,10,arcade.color.BLACK)
        self.center_x = x
        self.center_y = y
        self.directPrev = 'R'
        self.direct = 'R'

class food_snake(arcade.SpriteCircle):  
      def __init__(self,x,y):
        super().__init__(radius=5, color=arcade.color.GOLD)
        self.center_x = x
        self.center_y = y
        
class GameOver(arcade.View):
    def __init__(self):
        super().__init__()
        self.score = 0
        self.setup()
    
    def setup(self):
        pass

    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('GameOver', 55, 200, arcade.color.WHITE, 30)
        arcade.draw_text('press \'R\' to new game', 100, 150, arcade.color.WHITE, 8)
        arcade.draw_text('GameOver State', 10, 10, arcade.color.WHITE, 8)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
 
        if key == arcade.key.R:
            game_view = MyGame()
            self.window.show_view(game_view)

#แบบฝึกหัดเกมงู
class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    
    def setup(self):
        # Set up your game here
        self.snake = arcade.SpriteList()
        
        for i in range(3):
          box1 = body_box(50-(i*12),100)
          self.snake.append(box1)

        self.timesnake = 0

        self.food = food_snake(200,200)
        self.speed = 9

        self.gameOver = False
        self.gameFade = False
        self.controlTimeEndGame = 0 
        self.FrameGameOver = 0
   
    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here

        if(self.gameFade==False):
          self.snake.draw()
          self.food.draw()

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.snake.update()
        
        ############# control effect game over #############
        if(self.gameOver==True):
            #print(self.gameFade,self.controlTimeEndGame)
            self.controlTimeEndGame += delta_time 
           
            if(self.controlTimeEndGame>0.3):
               self.FrameGameOver+=1 
               self.controlTimeEndGame = 0 

               if(self.FrameGameOver%2==0):
                 self.gameFade = True  
               else:
                 self.gameFade = False 

               if(self.FrameGameOver>=10):
                  game_view = GameOver()
                  self.window.show_view(game_view)
        ################ end control effect game over  #######


        ########## ส่วนควบคุมการเคลื่อนที่ 
        #ควบคุมการเคลื่อนที่จากหลังไปหน้า ให้ลำตัวสุดท้ายค่อยๆ ขยับตาม
        if(self.gameOver == False):
         self.timesnake+=delta_time 
         if(self.timesnake>1-(self.speed/10)):
           self.timesnake = 0 
           for i in range(len(self.snake)-1,-1,-1):
            if(i==0):
              if(self.snake[i].direct=='R'):
                 self.snake[i].center_x +=12
              elif(self.snake[i].direct=='U'):
                 self.snake[i].center_y +=12 
              elif(self.snake[i].direct=='L'):
                 self.snake[i].center_x -=12
              elif(self.snake[i].direct=='D'):
                 self.snake[i].center_y -=12       
            else: 
                 self.snake[i].center_x = self.snake[i-1].center_x 
                 self.snake[i].center_y = self.snake[i-1].center_y 
        ############ end zone ###############
        
        ######## zone เช็คกินอาหาร ###############
        if(arcade.check_for_collision(self.snake[0],self.food)):
             x = self.snake[len(self.snake)-1].center_x
             y = self.snake[len(self.snake)-1].center_y
             box_new = body_box(x,y)
             self.snake.append(box_new)

             self.food.center_x = random.randint(20,SCREEN_WIDTH-20)
             self.food.center_y = random.randint(20,SCREEN_HEIGHT-20)
             
             self.speed+=1
     
             if(self.speed>=10):
                self.speed = 9

        ###################### zone check game over #############
        if(self.snake[0].center_x>SCREEN_WIDTH or self.snake[0].center_x<0 
           or self.snake[0].center_y > SCREEN_HEIGHT or self.snake[0].center_y < 0 ):
            self.gameOver = True
            
            
        c = arcade.check_for_collision_with_list(self.snake[0],self.snake)
        for _ in c:
            self.gameOver = True
        ################### end zone check game over #############

        

        
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        
        self.snake[0].directPrev = self.snake[0].direct

        if key == arcade.key.W:
            self.snake[0].direct = 'U'
        elif key == arcade.key.S:
            self.snake[0].direct = 'D'
        elif key == arcade.key.A:
            self.snake[0].direct = 'L'
        elif key == arcade.key.D:
            self.snake[0].direct = 'R' 
        
       
    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass
 

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    #window.total_score = 0
    game = MyGame()
    window.show_view(game)
    arcade.run()



if __name__ == "__main__":
    main()
