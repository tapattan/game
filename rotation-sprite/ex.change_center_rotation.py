import arcade

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600


class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        self.r1 = arcade.SpriteSolidColor(300, 10,arcade.color.WHITE)
        self.r1.center_x = 400
        self.r1.center_y = 300
        self.r1.change_angle = 360/100

        

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here

        self.r1.draw()

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.r1.update()


def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
