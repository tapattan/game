import arcade

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600


class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        self.r1 = arcade.Sprite()
        self.r1.center_x = 400
        self.r1.center_y = 300
        self.r1.change_angle = 360/100

        image = arcade.get_image()
        # วาดสี่เหลี่ยมผืนผ้าบน surface นั้น
        arcade.draw_commands.draw_rectangle_filled(75, 0, 150, 5, arcade.color.RED)
        # บันทึกผลลัพธ์ลงใน texture
        image = arcade.get_image(75, 0, 150, 5)
       
        self.r1.texture = arcade.Texture("Rectangle", image)

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here

        self.r1.draw()

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.r1.update()


def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
