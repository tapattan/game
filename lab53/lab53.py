import arcade
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):

        self.SailorMoon = arcade.Sprite('SailorMoon01.gif')
        self.SailorMoon.center_x = 300
        self.SailorMoon.center_y = 300
        self.SailorMoon.scale = 0.25

        self.textureList = []
        for i in range(4): 
           k = arcade.load_texture('SailorMoon0'+str(i+1)+'.gif')
           self.textureList.append(k) 

        self.timeFrame = 0
        self.frameAt = 0   
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.SailorMoon.draw()
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """

        self.timeFrame = self.timeFrame+delta_time 
        if(self.timeFrame>0.2):
           self.timeFrame = 0 
           self.frameAt = self.frameAt+1 

        if(self.frameAt==4):
           self.frameAt = 0 

        self.SailorMoon.texture = self.textureList[self.frameAt]     

        pass

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
