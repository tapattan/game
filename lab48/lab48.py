import arcade
import random
SCREEN_WIDTH = 640
SCREEN_HEIGHT = 640
 
class boom(arcade.SpriteList):
    def __init__(self,posX,posY):
        super().__init__()
        
        self.colorx = [arcade.color.ALIZARIN_CRIMSON]
        
        for i in range(20):
         s1 = arcade.SpriteCircle(radius=random.randint(12,15),color=random.choice(self.colorx),soft=True)
         s1.center_x = posX
         s1.center_y = posY
         s1.alpha = random.randint(10,254)
         s1.change_x = random.randint(-3,3)
         s1.change_y = random.randint(-3,3)
         self.append(s1)
        
       
    def update_animation(self, delta_time=1/20):
        for i in self:
         if(i.alpha>5):
            i.alpha = i.alpha-5
         else:
            i.alpha = 0  
           
          
         
 
class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        print('init')
        self.setup()
    
    def on_show_view(self):
        pass
        print('showview')
       

    def setup(self):
        # Set up your game here
        pass
        print('setup')
        
        #load ร่างของตัวละคร
        self.mario = arcade.Sprite('5-mario-running-01.gif')
        self.mario.center_x = 180 
        self.mario.center_y = 150

        self.power = arcade.Sprite('power.png')
        self.power.scale = 0.25
        self.statuspower = False 
        
        #load เสื้อของตัวละครเข้ามา
        marioTexture01 = arcade.load_texture('5-mario-running-01.gif')  # ยืนตรง
        marioTexture02 = arcade.load_texture('5-mario-running-02.gif')  # มีการก้าวเท้า
        self.textureList = []
        self.textureList.append(marioTexture01)
        self.textureList.append(marioTexture02)

        self.timegame = 0 
        self.frameAt = 0
 
        map_name = "Map01.json"
        layer_options = {
            "layerwall": {
                "use_spatial_hash": True,
            },
        }

        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)


        self.LayerWall = self.tile_map.sprite_lists["layerwall"]

        # Keep player from running through the wall_list layer
        walls = [self.LayerWall, ]
        self.physics_engine = arcade.PhysicsEnginePlatformer(
            self.mario, walls, gravity_constant=1
        )  
        

        self.monster01 = arcade.Sprite('monster01.png')
        self.monster01.scale = 0.4
        self.monster01.center_x = 650
        self.monster01.center_y = 146
        self.monster01.change_x = -1
        
        

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.LayerWall.draw()
        self.monster01.draw()
        self.mario.draw()

        if(self.statuspower):
           self.power.draw()
        
        try:
          self.boom.draw()
        except:
          pass 

    def update(self, delta_time):
        self.physics_engine.update()
        self.mario.update()
        self.power.update()
        self.monster01.update()

        try:
          self.boom.update()
          self.boom.update_animation() 
        except:
          pass    

        if(self.statuspower):
           if(self.power.center_x>640):
              self.statuspower = False 
         
        
        if self.mario.center_x>=620:
            self.mario.center_x=620
        if self.mario.center_x<=30:
            self.mario.center_x=30
        
        
        
        #print(delta_time)
        self.timegame = self.timegame + delta_time
        if(self.timegame>0.2):
            self.frameAt = self.frameAt+1
            self.timegame = 0
            
        if(self.frameAt==2):
           self.frameAt = 0 
           
        self.mario.texture = self.textureList[self.frameAt]
        
   
        c = arcade.check_for_collision(self.power,self.monster01)
        if(c):
           self.boom = boom(self.monster01.center_x,self.monster01.center_y) 
           self.monster01.center_x = 800
           self.power.center_x = 1000 
        pass
       
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.W:
            self.mario.change_y = 10
        elif key == arcade.key.A:
            self.mario.change_x = -5
        elif key == arcade.key.D:
            self.mario.change_x = 5
        elif key == arcade.key.S:
            self.mario.change_y = -5  
        elif key ==arcade.key.SPACE:
            if(self.statuspower==False):  
              self.statuspower = True    
              self.power.center_x = self.mario.center_x 
              self.power.center_y = self.mario.center_y
              self.power.change_x = 5
    

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.W or key == arcade.key.S:
            self.mario.change_y = 0
        elif key == arcade.key.A or key == arcade.key.D:
            self.mario.change_x = 0
   
def main():
    window = arcade.Window(SCREEN_WIDTH, SCREEN_HEIGHT, "แบบฝึกหัดเกมของชั้น")
    menu = MyGame()
    window.show_view(menu)
    arcade.run()

    #game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    #game.setup()
    #arcade.run()


if __name__ == "__main__":
    main()