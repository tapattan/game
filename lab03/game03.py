import arcade
import random 

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

MOVEMENT_SPEED = 5

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)
    
    def setup(self):
        # Set up your game here

        self.mySpriteList = arcade.SpriteList() 

        for i in range(10):
            mySprite = arcade.Sprite('../resources/cloud.png',0.05) #ปรับขนาดเมฆ
            mySprite.center_x = random.randint(0,SCREEN_WIDTH) 
            mySprite.center_y = random.randint(400,SCREEN_HEIGHT)
            
            mySprite.change_x = 5

            self.mySpriteList.append(mySprite)
        #pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here
        self.mySpriteList.draw()

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass

        self.mySpriteList.update()
        for i in self.mySpriteList:
            if(i.center_x > SCREEN_WIDTH):
              i.center_x = -1*random.randint(10,200)
              i.center_y = random.randint(400,SCREEN_HEIGHT)

        #self.mySpriteList.update_animation()
        
    

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()