import arcade
import random 
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

from PIL import Image, ImageDraw, ImageFont

def adding_text(img, text, position, size, colour):
    font = ImageFont.truetype('Sukhumvit Set', size)
    draw = ImageDraw.Draw(img)
    draw.text(position, text, font=font, fill=colour)
    return img

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        

        img = Image.new('RGBA', (400, 250), color = (0, 0, 0, 0))
        fnt = ImageFont.truetype('SukhumvitSet-Text.ttf', 40)
        d = ImageDraw.Draw(img)
        d.text((10,10), "ทำงาน ทำงาน ทำงาน", font=fnt, fill=(255, 255, 0))
        img.save('pil_text_font.png')
        
        self.font = arcade.Sprite('pil_text_font.png')
        self.font.center_x = 300 
        self.font.center_y = 50
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
 
        arcade.draw_text('HELLO WORLD', 100, 500, arcade.color.RED,50,font_name='Sukhumvit Set')
        arcade.draw_text('this\nis\na\ncat', 200, 400, arcade.color.RED,20,font_name='Sukhumvit Set',multiline=True,width=300)

        #
        arcade.draw_text('สวัสดีครับ', 100, 200, arcade.color.RED,20,font_name='Helvetica Neue',multiline=True,width=300)

        self.font.draw()

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass

def main():
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    window.total_score = 0
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
