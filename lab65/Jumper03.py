import arcade
import random 

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
MAX_SCREEN_HEIGHT = 3000

MOVEMENT_SPEED = 4

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):

        self.hero = arcade.Sprite('images/bodyR.png')
        self.hero.center_x = 100
        self.hero.center_y = 100
        self.hero.scale = 0.8

        self.direction = 'right'
        self.frameat = 0
        self.timeaction = 0

        heroTexture01R = arcade.load_texture('images/right1.png')
        heroTexture02R = arcade.load_texture('images/right2.png')
        heroTexture03R = arcade.load_texture('images/right3.png')

        self.hereTextTureR = [heroTexture01R,heroTexture02R,heroTexture03R]

        heroTexture01L = arcade.load_texture('images/left1.png')
        heroTexture02L = arcade.load_texture('images/left2.png')
        heroTexture03L = arcade.load_texture('images/left3.png')

        self.hereTextTureL = [heroTexture01L,heroTexture02L,heroTexture03L]

        self.block = arcade.SpriteList(use_spatial_hash=True) 
        

        option1 = [200,400,600,800,1000,1200,1400,1600,1800,2000,2200,2400,2600,2800]
        for i in option1:
          for j in range(5):
            b1 = arcade.Sprite('b1.png')
            b1.center_x = random.randint(50,500)
            b1.center_y = i
            b1.scale = 0.5

            self.block.append(b1)


        for i in range(-2,20):
          b1 = arcade.Sprite('b4.jpg')
          b1.center_x = (i*50)
          b1.center_y = 10
          b1.scale = 0.5
          self.block.append(b1)


        self.view_position_player = 0 #กล้อง

        self.physics_engine = arcade.PhysicsEnginePlatformer(
            self.hero, self.block, gravity_constant=1
        )  

        pass

    def on_draw(self):
        """ Render the screen. """

        self.physics_engine.update()

        arcade.start_render()
        self.hero.draw()
        for i in self.block:
            i.draw()
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """

        self.hero.update()
        self.timeaction = self.timeaction + delta_time
        if(self.timeaction>0.2):
           self.timeaction = 0
           self.frameat+=1 
        if(self.frameat==3):
           self.frameat = 0 

        if(self.direction=='right'):
           self.hero.texture = self.hereTextTureR[self.frameat]
        if(self.direction=='left'):
           self.hero.texture = self.hereTextTureL[self.frameat]



        #ส่วนของกล้องที่เลื่อนตาม

        # arcade.set_viewport(left: float, right: float, bottom: float, top: float)

        self.view_position_player = self.hero.center_y - (SCREEN_HEIGHT//2)
        print(self.view_position_player)
        self.view_left = self.view_position_player

        if(self.view_position_player<0):
            print(1)
            arcade.set_viewport(0, #left
                                0+SCREEN_WIDTH,  #right
                                0, #buttom
                                SCREEN_HEIGHT)      #top           

            self.view_left = 0                  
        elif(self.view_position_player>MAX_SCREEN_HEIGHT-SCREEN_HEIGHT):  #6000-600
            print(2)
            arcade.set_viewport(0,
                                SCREEN_WIDTH ,
                                MAX_SCREEN_HEIGHT-SCREEN_HEIGHT, 
                                MAX_SCREEN_HEIGHT)
            self.view_left = MAX_SCREEN_HEIGHT - SCREEN_HEIGHT               
        else:
            print(3)
            arcade.set_viewport(0,
                                SCREEN_WIDTH,
                                self.view_position_player,
                                self.view_position_player+SCREEN_HEIGHT)

        #end ส่วนของกล่้องที่เลื่อนตาม



    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.SPACE:
            self.hero.change_y = MOVEMENT_SPEED*4
        elif key == arcade.key.D:
            self.hero.change_x = MOVEMENT_SPEED
            self.direction = 'right'
        elif key == arcade.key.A:
            self.hero.change_x = -MOVEMENT_SPEED
            self.direction = 'left'

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.SPACE:
            self.hero.change_y = 0
        elif key == arcade.key.A or key == arcade.key.D:
            self.hero.change_x = 0


def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
