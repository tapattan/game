import arcade
import random 

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
MAX_SCREEN_HEIGHT = 0

MOVEMENT_SPEED = 4
__LEVEL__ = 1 
__HEIGHT__NEXT_LEVEL__ = 1000

__SCORE_TIME__ = 0


class GameStateViewTitle(arcade.View):
    def __init__(self,yourLevel):
        super().__init__()
        self.score = 0
        __LEVEL__ = yourLevel
        self.setup()
    
    def setup(self):
        # ให้ฉากกลับมาเท่าขนาดเดิม
        
        #print('s',SCREEN_WIDTH,SCREEN_HEIGHT)

        pass

    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.set_viewport(0,                      #(left, right, bottom, top)
                                0+SCREEN_WIDTH,
                                0,
                                SCREEN_HEIGHT) 
        
        #print('s',SCREEN_WIDTH,SCREEN_HEIGHT)

        arcade.draw_text('Level : '+str(__LEVEL__), 200, 300, arcade.color.WHITE, 30)
        arcade.draw_text('Press Space to Start Game', 180, 100, arcade.color.WHITE, 15)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.SPACE:
            game_view = MyGame()
            game_view.score = self.score
            self.window.show_view(game_view)

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        global MAX_SCREEN_HEIGHT 
        MAX_SCREEN_HEIGHT = MAX_SCREEN_HEIGHT + __HEIGHT__NEXT_LEVEL__
        self.setup()

    def setup(self):

        self.hero = arcade.Sprite('images/bodyR.png')
        self.hero.center_x = 100
        self.hero.center_y = 100
        self.hero.scale = 0.8

        self.direction = 'right'
        self.frameat = 0
        self.timeaction = 0

        heroTexture01R = arcade.load_texture('images/right1.png')
        heroTexture02R = arcade.load_texture('images/right2.png')
        heroTexture03R = arcade.load_texture('images/right3.png')

        self.hereTextTureR = [heroTexture01R,heroTexture02R,heroTexture03R]

        heroTexture01L = arcade.load_texture('images/left1.png')
        heroTexture02L = arcade.load_texture('images/left2.png')
        heroTexture03L = arcade.load_texture('images/left3.png')

        self.hereTextTureL = [heroTexture01L,heroTexture02L,heroTexture03L]

        self.block = arcade.SpriteList(use_spatial_hash=True) 
        
        self.key = arcade.Sprite('images/Key.png')
        self.key.center_x = 200
        self.key.center_y= 50
        self.key.scale = 0.21

        self.keydisplay = arcade.Sprite('images/Key.png')
        self.keydisplay.center_x = 50
        self.keydisplay.center_y= -100
        self.keydisplay.scale = 0.21

        self.chestclose = arcade.Sprite('images/ChestClose.png')
        self.chestclose.center_x = 500
        self.chestclose.center_y = 50
        self.chestclose.scale = 0.26


        self.chestopen = arcade.load_texture('images/ChestOpen.png')



       
        self.topfix = SCREEN_HEIGHT-20
        self.keydisplayShow = 'off'



        #option1 = [200,400,600,800,1000,1200,1400,1600,1800,2000,2200,2400,2600,2800]
        option1=[]
        l=(MAX_SCREEN_HEIGHT/200)
        l=int(l)
        for i in range (l):
            option1.append((0+i)*200)
        

        lastPosX = []
        for i in range(len(option1)):
          for j in range(5):
            b1 = arcade.Sprite('b1.png')
            m = random.randint(50,500)
            b1.center_x = m
            b1.center_y = option1[i]
            b1.scale = 0.5

            self.block.append(b1)
            if(i==len(option1)-1):
               lastPosX.append(m)
          
        self.chestclose.center_y = option1[-1]+50     #***************** config to move box
        self.chestclose.center_x = max(lastPosX)

        self.key.center_y = option1[random.randint(0,len(option1)-1)] + 50

        for i in range(-2,20):
          b1 = arcade.Sprite('b4.jpg')
          b1.center_x = (i*50)
          b1.center_y = 10
          b1.scale = 0.5
          self.block.append(b1)


        self.view_position_player = 0 #กล้อง

        self.physics_engine = arcade.PhysicsEnginePlatformer(
            self.hero, self.block, gravity_constant=1
        )  

        pass
        
        self.coolDownNext = 0 
        self.coolDownNext_Status = False
        


        
        self.b2List = []
        Layer_map = round(MAX_SCREEN_HEIGHT / 240,0)
        Layer_map = int(Layer_map)+1
        
        print('--------setup--------',MAX_SCREEN_HEIGHT,Layer_map)

        for j in range(Layer_map):
          start_y = 0 + (j*240)
          start_x = random.randint(-200,0)
          for i in range(4):
            b2 = arcade.Sprite('b2.jpg')
            b2.center_x = start_x+(i*265) 
            b2.center_y = 100 + start_y
            b2.scale = 0.25

            self.b2List.append(b2)

        self.timestatusJump = 0 
        self.jumpcan = 'yes'

    def on_draw(self):
        """ Render the screen. """

        

        arcade.start_render()
        
        for i in self.b2List:
          i.draw()

        self.chestclose.draw()

        self.hero.draw()
        for i in self.block:
            i.draw()
        
        self.key.draw()

        if(self.keydisplayShow=='on'):
           self.keydisplay.draw()

        
        arcade.draw_text('TIMES : '+str(__SCORE_TIME__), 450, self.keydisplay.center_y, arcade.color.WHITE, 14)
       
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        
        global __LEVEL__
        global __SCORE_TIME__

        __SCORE_TIME__ += delta_time
        __SCORE_TIME__ = round(__SCORE_TIME__,2)

        self.physics_engine.update()
        self.hero.update()
        self.keydisplay.update()

        self.timeaction = self.timeaction + delta_time
        if(self.timeaction>0.2):
           self.timeaction = 0
           self.frameat+=1 
        if(self.frameat==3):
           self.frameat = 0 

        if(self.direction=='right'):
           self.hero.texture = self.hereTextTureR[self.frameat]
        if(self.direction=='left'):
           self.hero.texture = self.hereTextTureL[self.frameat]

        if(arcade.check_for_collision(self.hero,self.chestclose)):
            if (self.keydisplayShow=='on' and self.coolDownNext_Status==False):
                self.chestclose.texture = self.chestopen
                __LEVEL__ = __LEVEL__ + 1
                self.coolDownNext_Status = True 

        
        if(self.coolDownNext_Status):
           self.coolDownNext += delta_time 

        if(self.coolDownNext>1):
           your_level_end_game = 10  #------> จะให้จบด่านไหน
           if(__LEVEL__==your_level_end_game+1):
               game_view = GameWin()
               self.window.show_view(game_view)
           else:
               game_view = GameStateViewTitle(__LEVEL__)
               self.window.show_view(game_view) 

          
 

        #ส่วนของกล้องที่เลื่อนตาม

        # arcade.set_viewport(left: float, right: float, bottom: float, top: float)

        self.view_position_player = self.hero.center_y - (SCREEN_HEIGHT//2)
        #print(self.view_position_player)
        self.view_left = self.view_position_player
        if(self.view_position_player<0):
            #print(1)
            arcade.set_viewport(0, #left
                                0+SCREEN_WIDTH,  #right
                                0, #buttom
                                SCREEN_HEIGHT)      #top           

            self.view_left = 0                  
        elif(self.view_position_player>MAX_SCREEN_HEIGHT-SCREEN_HEIGHT):  #6000-600
            #print(2)
            arcade.set_viewport(0,
                                SCREEN_WIDTH ,
                                MAX_SCREEN_HEIGHT-SCREEN_HEIGHT, 
                                MAX_SCREEN_HEIGHT)
            self.view_left = MAX_SCREEN_HEIGHT - SCREEN_HEIGHT               
        else:
            #print(3)
            arcade.set_viewport(0,
                                SCREEN_WIDTH,
                                self.view_position_player,
                                self.view_position_player+SCREEN_HEIGHT)
            self.view_left = self.view_position_player
        #end ส่วนของกล่้องที่เลื่อนตาม

        #print(self.view_left,self.keydisplay.center_y)
        if(arcade.check_for_collision(self.hero,self.key)):
            self.key.center_y =-1000
            self.keydisplayShow = 'on'
        
        
        self.keydisplay.center_y = self.topfix + self.view_left -10


        #check bound
        #print('b',self.hero.center_x)
        if(self.hero.center_x >= 580):
           self.hero.center_x = 580 
        elif(self.hero.center_x <= 12):
           self.hero.center_x = 12   


        if(self.jumpcan == 'no'):
           self.timestatusJump += delta_time  

           if(self.timestatusJump>0.5):
               self.jumpcan = 'yes'
               self.timestatusJump = 0


    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.SPACE:
            if(self.jumpcan=='yes'):
              self.hero.change_y = MOVEMENT_SPEED*4
              self.jumpcan = 'no'

        elif key == arcade.key.D:
            self.hero.change_x = MOVEMENT_SPEED
            self.direction = 'right'
        elif key == arcade.key.A:
            self.hero.change_x = -MOVEMENT_SPEED
            self.direction = 'left'

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.SPACE:
            self.hero.change_y = 0
        elif key == arcade.key.A or key == arcade.key.D:
            self.hero.change_x = 0

import pandas as pd 

class GameWin(arcade.View):
    def __init__(self):
        super().__init__()
        self.score = 0
        self.HIGH_SCORE = 0 
        self.setup()
    
    def setup(self):
        global __SCORE_TIME__
        k = pd.read_csv('score.csv')
    
        km = k[['score']].min()
        print('**',km)
        self.HIGH_SCORE = km[0]

        if(__SCORE_TIME__<km[0]):
           df = pd.DataFrame({'score':[__SCORE_TIME__]}) 
           df.to_csv('score.csv')
           self.HIGH_SCORE = __SCORE_TIME__ 

        pass

    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)

    def on_draw(self):
        arcade.start_render()

        arcade.set_viewport(0,                      #(left, right, bottom, top)
                                0+SCREEN_WIDTH,
                                0,
                                SCREEN_HEIGHT) 
        
        """
        Draw "Game over" across the screen.
        """
        pass 
        global __SCORE_TIME__
        arcade.draw_text('You Win ', 200, 450, arcade.color.WHITE, 40)

        arcade.draw_text('Best Time : '+str(self.HIGH_SCORE), 220, 300, arcade.color.WHITE, 20)
        arcade.draw_text('Your Time : '+str(__SCORE_TIME__), 220, 250, arcade.color.WHITE, 20)

        arcade.draw_text('Press Space to New Start Game', 170, 100, arcade.color.WHITE, 15)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        global __LEVEL__
        global __SCORE_TIME__
        __SCORE_TIME__ = 0
        __LEVEL__ = 1
        if key == arcade.key.SPACE:
            game_view = GameStateViewTitle(1)
            self.window.show_view(game_view)

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = GameStateViewTitle(1) # GameWin()# 
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
