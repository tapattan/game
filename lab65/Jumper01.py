import arcade
import random 

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
MAX_SCREEN_HEIGHT = 3000

MOVEMENT_SPEED = 6

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):

        self.hero = arcade.Sprite('images/right1.png')
        self.hero.center_x = 100
        self.hero.center_y = 50
        self.hero.scale = 0.8

        self.block = [] 
        for i in range(100):
          b1 = arcade.Sprite('b1.png')
          b1.center_x = random.randint(50,500)
          b1.center_y = random.randint(50,2950)
          b1.scale = 0.5

          self.block.append(b1)


        self.view_position_player = 0 #กล้อง
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.hero.draw()
        for i in self.block:
            i.draw()
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """

        self.hero.update()


        #ส่วนของกล้องที่เลื่อนตาม

        # arcade.set_viewport(left: float, right: float, bottom: float, top: float)

        self.view_position_player = self.hero.center_y - (SCREEN_HEIGHT//2)
        print(self.view_position_player)
        self.view_left = self.view_position_player

        if(self.view_position_player<0):
            print(1)
            arcade.set_viewport(0, #left
                                0+SCREEN_WIDTH,  #right
                                0, #buttom
                                SCREEN_HEIGHT)      #top           

            self.view_left = 0                  
        elif(self.view_position_player>MAX_SCREEN_HEIGHT-SCREEN_HEIGHT):  #6000-600
            print(2)
            arcade.set_viewport(0,
                                SCREEN_WIDTH ,
                                MAX_SCREEN_HEIGHT-SCREEN_HEIGHT, 
                                MAX_SCREEN_HEIGHT)
            self.view_left = MAX_SCREEN_HEIGHT - SCREEN_HEIGHT               
        else:
            print(3)
            arcade.set_viewport(0,
                                SCREEN_WIDTH,
                                self.view_position_player,
                                self.view_position_player+SCREEN_HEIGHT)

        #end ส่วนของกล่้องที่เลื่อนตาม



    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP:
            self.hero.change_y = MOVEMENT_SPEED
        elif key == arcade.key.DOWN:
            self.hero.change_y = -MOVEMENT_SPEED
        elif key == arcade.key.LEFT:
            self.hero.change_x = -MOVEMENT_SPEED
        elif key == arcade.key.RIGHT:
            self.hero.change_x = MOVEMENT_SPEED

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP or key == arcade.key.DOWN:
            self.hero.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.hero.change_x = 0


def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
