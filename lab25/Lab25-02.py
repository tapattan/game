import arcade
import random
SCREEN_WIDTH = 640
SCREEN_HEIGHT = 640

class coin(arcade.Sprite):
    def __init__(self,position=(100,100)):

        # Set up parent class
        super().__init__()

        ####### ส่วนของเหรียญ 
        self.center_x = position[0] 
        self.center_y = position[1]
        self.scale = 0.05
        
        cointure01 = arcade.load_texture('coin/coin1.png')  #  
        cointure02 = arcade.load_texture('coin/coin2.png')  #  
        cointure03 = arcade.load_texture('coin/coin3.png')  #  
        cointure04 = arcade.load_texture('coin/coin4.png')  #  
        cointure05 = arcade.load_texture('coin/coin5.png')  #  
        cointure06 = arcade.load_texture('coin/coin6.png')  #  
        cointure07 = arcade.load_texture('coin/coin7.png')  #  
        cointure08 = arcade.load_texture('coin/coin8.png')  #  
            
        self.textureList = []
        self.textureList.append(cointure01)
        self.textureList.append(cointure02)
        self.textureList.append(cointure03)
        self.textureList.append(cointure04)
        self.textureList.append(cointure05)
        self.textureList.append(cointure06)
        self.textureList.append(cointure07)
        self.textureList.append(cointure08)
        
        self.statusFrameaction = 0 
        self.timeKFrame = 0
        self.timeFrame = 0 
        
    def update_animation(self, delta_time: float = 1 / 20):
        self.statusFrameaction = len(self.textureList)
        self.timeKFrame +=(delta_time)
        if(self.timeKFrame>0.2):
            self.timeKFrame=0
            self.timeFrame+=1
            if(self.timeFrame%self.statusFrameaction==0):
                self.timeFrame = 0
            
            try:
              self.texture = self.textureList[self.timeFrame]
            except:
              pass

        

class MyGame(arcade.Window):
    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        pass
        
        #load ร่างของตัวละคร
        self.mario = arcade.Sprite('5-mario-running-01.gif')
        self.mario.center_x = 180 
        self.mario.center_y = 150
        
        #load เสื้อของตัวละครเข้ามา
        marioTexture01 = arcade.load_texture('5-mario-running-01.gif')  # ยืนตรง
        marioTexture02 = arcade.load_texture('5-mario-running-02.gif')  # มีการก้าวเท้า
        self.textureList = []
        self.textureList.append(marioTexture01)
        self.textureList.append(marioTexture02)

        
        self.timegame = 0 
        self.frameAt = 0
        
        self.coinList = [] #list ของเหรียญของเรา
        for i in range(13):
            c = coin(position=(50+(i*50),230))
            self.coinList.append(c)  
       
        
        map_name = "Map01.json"
        layer_options = {
            "layerwall": {
                "use_spatial_hash": True,
            },
        }

        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)


        self.LayerWall = self.tile_map.sprite_lists["layerwall"]

        # Keep player from running through the wall_list layer
        walls = [self.LayerWall, ]
        self.physics_engine = arcade.PhysicsEnginePlatformer(
            self.mario, walls, gravity_constant=1
        )  
        
        self.LayerCloud = self.tile_map.sprite_lists["layercloud"]
        
        #load sound ใส่ที่ setup 
        self.audio = arcade.load_sound('smw_coin.wav',False)

        
        ## อยู่ใน setup นะ
        self.bgaudio = arcade.load_sound('bgsound1.wav',False)
        self.mediaplay_bg = arcade.play_sound(self.bgaudio,0.1,-1,True)
        
        

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.LayerWall.draw()
        self.LayerCloud.draw()
        self.mario.draw()
        
        #self.coin.draw()
        for i in self.coinList:
            i.draw()  
        


    def update(self, delta_time):
        self.physics_engine.update()
        self.mario.update()
        
        for i in self.coinList:
            i.update()
            i.update_animation()
            
        for i in self.LayerCloud:
            i.center_x += 1
            if(i.center_x>650):
               i.center_x = -10
              
               
            
        for i in range(len(self.coinList)):
             if(arcade.check_for_collision(self.mario,self.coinList[i])):
                self.coinList.pop(i) 
                arcade.play_sound(self.audio,1.0,-1,False)
                break # ทำเสร็จแล้วให้ออกจาก list ไปเลย
                 
        
        if self.mario.center_x>=620:
            self.mario.center_x=620
        if self.mario.center_x<=30:
            self.mario.center_x=30
        
        
        
        #print(delta_time)
        self.timegame = self.timegame + delta_time
        if(self.timegame>0.2):
            self.frameAt = self.frameAt+1
            self.timegame = 0
            
        if(self.frameAt==2):
           self.frameAt = 0 
           
        self.mario.texture = self.textureList[self.frameAt]
        
        
        ############## control coin #######
        #print(delta_time)
        #self.timegameCoin = self.timegameCoin + delta_time
        #if(self.timegameCoin>0.2):
        #    self.frameAtCoin = self.frameAtCoin+1
        #    self.timegameCoin = 0
            
        #if(self.frameAtCoin==8):
        #   self.frameAtCoin = 0 
           
        #self.coin.texture = self.textureListCoin[self.frameAtCoin]
        
        pass
       
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.W:
            self.mario.change_y = 10
        elif key == arcade.key.A:
            self.mario.change_x = -5
        elif key == arcade.key.D:
            self.mario.change_x = 5
        elif key == arcade.key.S:
            self.mario.change_y = -5  
        elif key == arcade.key.SPACE:
            #https://stackoverflow.com/questions/70340017/how-do-i-stop-the-background-music-in-the-arcade-library
            #self.mediaplay_bg.pause()   
            arcade.stop_sound(self.mediaplay_bg)
        elif key == arcade.key.P:
            self.mediaplay_bg.play()     


    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.W or key == arcade.key.S:
            self.mario.change_y = 0
        elif key == arcade.key.A or key == arcade.key.D:
            self.mario.change_x = 0
   
def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()