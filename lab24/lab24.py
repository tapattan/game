import arcade
import random 
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
 
 
class objLive(arcade.Sprite):
    
    def __init__(self,spritegif,scale=0.2,position=[100,100]):

        # Set up parent class
        super().__init__()
 
        self.texturesDown = []
        for i in spritegif:
          self.texturesDown.append(arcade.load_texture(i))
     
        self.textureList = self.texturesDown 
        self.statusFrameaction = len(self.texturesDown)
        print(self.statusFrameaction)

        self.timeFrame = 0
        self.timeKFrame = 0
        self.texture = self.textureList[self.timeFrame]
        
        self.center_x = position[0]
        self.center_y = position[1]
        self.scale = scale

    def update_animation(self, delta_time: float = 1 / 20):
        self.statusFrameaction = len(self.textureList)
        self.timeKFrame +=(delta_time)
        if(self.timeKFrame>0.2):
            self.timeKFrame=0
            self.timeFrame+=1
            if(self.timeFrame%self.statusFrameaction==0):
                self.timeFrame = 0
            
            try:
              self.texture = self.textureList[self.timeFrame]
            except:
              pass


class MyGame(arcade.View):
    """ Main application class. """
    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()
     

    def setup(self):
        pass
        birdList = ['bird5.png','bird6.png','bird7.png']
        self.bird = objLive(spritegif=birdList,position=[200,200])

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.bird.draw()
    

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.bird.update()
        self.bird.update_animation()

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        pass 
        

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass
         

def main():
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    window.total_score = 0
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
