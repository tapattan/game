import arcade
import random 
from objLive import objLive

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class MyGame(arcade.View):
    """ Main application class. """
    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        pass
        birdList = ['bird5.png','bird6.png','bird7.png']
        self.bird = objLive(spritegif=birdList,position=[200,200])
        
        
        mList = ['monster010.png','monster011.png','monster012.png',
                 'monster013.png','monster014.png','monster015.png']
        self.monster = objLive(spritegif=mList,position=[500,400])
        

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.bird.draw()
        self.monster.draw()

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.bird.update()
        self.bird.update_animation()
        
        self.monster.update_animation()

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP:
            self.bird.change_y = 5
        elif key == arcade.key.DOWN:
            self.bird.change_y = -5
        elif key == arcade.key.LEFT:
            self.bird.change_x = -5
        elif key == arcade.key.RIGHT:
            self.bird.change_x = 5

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP or key == arcade.key.DOWN:
            self.bird.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.bird.change_x = 0

         

def main():
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    window.total_score = 0
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
