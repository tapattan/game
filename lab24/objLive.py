import arcade 

class objLive(arcade.Sprite):
    
    def __init__(self,spritegif,scale=0.2,position=[100,100]):

        # Set up parent class
        super().__init__()
 
        self.texturesDown = []
        for i in spritegif:
          self.texturesDown.append(arcade.load_texture(i))
     
        self.textureList = self.texturesDown 
        self.statusFrameaction = len(self.texturesDown)
        print(self.statusFrameaction)

        self.timeFrame = 0
        self.timeKFrame = 0
        self.texture = self.textureList[self.timeFrame]
        
        self.center_x = position[0]
        self.center_y = position[1]
        self.scale = scale

    def update_animation(self, delta_time: float = 1 / 20):
        self.statusFrameaction = len(self.textureList)
        self.timeKFrame +=(delta_time)
        if(self.timeKFrame>0.2):
            self.timeKFrame=0
            self.timeFrame+=1
            if(self.timeFrame%self.statusFrameaction==0):
                self.timeFrame = 0
            
            try:
              self.texture = self.textureList[self.timeFrame]
            except:
              pass