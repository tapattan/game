import arcade
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        
        self.zelda01 = arcade.Sprite(filename = '../resources/zelda.png', 
                      scale =  0.4, 
                      image_x = 0, 
                      image_y = 0, 
                      image_width = 120, 
                      image_height = 130)
        self.zelda01.center_x = 100
        self.zelda01.center_y = 100
        


    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.zelda01.draw()
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
