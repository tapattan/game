import arcade
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        
        self.zelda01 = arcade.Sprite(filename = '../resources/zelda.png', 
                      scale =  0.4, 
                      image_x = 0, 
                      image_y = 0, 
                      image_width = 120, 
                      image_height = 130)
        self.zelda01.center_x = 100
        self.zelda01.center_y = 100
        
        self.textureStand = []
        for i in range(3):
            s1 = arcade.load_texture(file_name='../resources/zelda.png', 
                      x = 0+(i*120), 
                      y = 0, 
                      width = 120, 
                      height = 130)
            self.textureStand.append(s1)

        self.textureLeft = []
        for i in range(10):
            s1 = arcade.load_texture(file_name='../resources/zelda.png', 
                      x = 0+(i*120), 
                      y = 130*5, 
                      width = 120, 
                      height = 130)
            self.textureLeft.append(s1)   

        self.textureRight = []
        for i in range(10):
            s1 = arcade.load_texture(file_name='../resources/zelda.png', 
                      x = 0+(i*120), 
                      y = 130*7, 
                      width = 120, 
                      height = 130)
            self.textureRight.append(s1)      

        self.textureWalkDown = []
        for i in range(10):
            s1 = arcade.load_texture(file_name='../resources/zelda.png', 
                      x = 0+(i*120), 
                      y = 130*4, 
                      width = 120, 
                      height = 130)
            self.textureWalkDown.append(s1) 

        self.textureWalkUp = []
        for i in range(10):
            s1 = arcade.load_texture(file_name='../resources/zelda.png', 
                      x = 0+(i*120), 
                      y = 130*6, 
                      width = 120, 
                      height = 130)
            self.textureWalkUp.append(s1)     
        
        self.textureWalkStandBack = []
        for i in range(1):
            s1 = arcade.load_texture(file_name='../resources/zelda.png', 
                      x = 0+(i*120), 
                      y = 130*2, 
                      width = 120, 
                      height = 130)
            self.textureWalkStandBack.append(s1)

        self.gametime = 0 
        self.frameat  = 0

        self.status = 'LEFT'

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.zelda01.draw()
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.zelda01.update()
        self.gametime +=delta_time 
        
        if(self.status == 'STAND'):
          if(self.gametime > 0.1):
             self.gametime = 0
             self.frameat += 1 
        
          if(self.frameat==3):
             self.frameat = 0

          self.zelda01.texture = self.textureStand[self.frameat]

        if(self.status == 'LEFT'):
          if(self.gametime > 0.1):
             self.gametime = 0
             self.frameat += 1 
        
          if(self.frameat==10):
             self.frameat = 0

          self.zelda01.texture = self.textureLeft[self.frameat] 

        if(self.status == 'RIGHT'):
          if(self.gametime > 0.1):
             self.gametime = 0
             self.frameat += 1 
        
          if(self.frameat==10):
             self.frameat = 0

          self.zelda01.texture = self.textureRight[self.frameat] 

        if(self.status == 'WALKDOWN'):
          if(self.gametime > 0.1):
             self.gametime = 0
             self.frameat += 1 
        
          if(self.frameat==10):
             self.frameat = 0

          self.zelda01.texture = self.textureWalkDown[self.frameat] 


        if(self.status == 'WALKUP'):
          if(self.gametime > 0.1):
             self.gametime = 0
             self.frameat += 1 
        
          if(self.frameat==10):
             self.frameat = 0

          self.zelda01.texture = self.textureWalkUp[self.frameat] 


        if(self.status == 'STANDBACK'):
          if(self.gametime > 0.1):
             self.gametime = 0
             self.frameat += 1 
        
          if(self.frameat==1):
             self.frameat = 0

          self.zelda01.texture = self.textureWalkStandBack[self.frameat]        
        
        
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP:
            self.zelda01.change_y = 5
            self.status = 'WALKUP'
        elif key == arcade.key.DOWN:
            self.zelda01.change_y = -5
            self.status = 'WALKDOWN'
        elif key == arcade.key.LEFT:
            self.zelda01.change_x = -5
            self.status = 'LEFT'
        elif key == arcade.key.RIGHT:
            self.zelda01.change_x = 5
            self.status = 'RIGHT'

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.DOWN:
            self.zelda01.change_y = 0
            self.status = 'STAND'
            self.frameat = 0

        elif key == arcade.key.UP:
            self.zelda01.change_y = 0
            self.status = 'STANDBACK'
            self.frameat = 0    
 
            
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.zelda01.change_x = 0
            self.status = 'STAND'
            self.frameat = 0
      

        

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
