import arcade
import random 
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class GameState2(arcade.View):
    def __init__(self):
        super().__init__()
        self.score = 0
        self.setup()
    
    def setup(self):
        pass

    def on_show(self):
        arcade.set_background_color(arcade.color.BLUE)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('GameState2 : '+str(self.score), 10, 20, arcade.color.WHITE, 14)



class GameState1(arcade.View):
    def __init__(self):
        super().__init__()
        self.setup()
        self.score  = 0
        self.hero = arcade.Sprite('hero.png',0.5)
    
    def setup(self):
        pass
        
        self.timegame = 0
        self.frameAt = 0
        
        self.portal = arcade.Sprite('portal/portal-pixel-art1.png',0.2) 
        self.portal.center_x = 550
        self.portal.center_y = 550 
        
        portal1 = arcade.load_texture('portal/portal-pixel-art1.png')
        portal2 = arcade.load_texture('portal/portal-pixel-art2.png')
        portal3 = arcade.load_texture('portal/portal-pixel-art3.png')
        portal4 = arcade.load_texture('portal/portal-pixel-art4.png')
        portal5 = arcade.load_texture('portal/portal-pixel-art5.png')
        portal6 = arcade.load_texture('portal/portal-pixel-art6.png')
        portal7 = arcade.load_texture('portal/portal-pixel-art7.png')
        portal8 = arcade.load_texture('portal/portal-pixel-art8.png')
        portal9 = arcade.load_texture('portal/portal-pixel-art9.png')
        
        self.portaltextureList = [portal1,portal2,portal3,portal4,portal5,portal6,portal7,portal8,portal9]
        
         
    def on_show(self):
        arcade.set_background_color(arcade.color.BABY_BLUE)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        self.portal.draw() 
        self.hero.draw()
         
        
        arcade.draw_text('GameState1 : '+str(self.score), 10, 20, arcade.color.WHITE, 14)
    
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
    
        self.hero.update()
        print('state1',self.score)
        
        self.timegame += delta_time   #คือเวลาที่อยู่ใน game 
        if(self.timegame>0.05):
            self.frameAt +=1 
            self.timegame = 0 
        
        if(self.frameAt==9):
            self.frameAt = 0 
        
        self.portal.texture = self.portaltextureList[self.frameAt] 

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
           
        
        if key == arcade.key.W:
            self.hero.change_y = 5
              
        if key == arcade.key.S:
            self.hero.change_y = -5
             
        if key == arcade.key.A:
            self.hero.change_x = -5
        
        if key == arcade.key.D:
            self.hero.change_x = 5    
        

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass
        if key == arcade.key.W or key == arcade.key.S:
                self.hero.change_y = 0
                
        if key == arcade.key.A or key == arcade.key.D:
                self.hero.change_x = 0
 



class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()


    def setup(self):
        self.hero = arcade.Sprite('hero.png',0.5)
        self.hero.center_x = 100
        self.hero.center_y = 100
        
        self.portal = arcade.Sprite('portal/portal-pixel-art1.png',0.2) 
        self.portal.center_x = 550
        self.portal.center_y = 550 
        
        
        self.score = 0
        
        self.timegame = 0
        self.frameAt = 0
        
        
        portal1 = arcade.load_texture('portal/portal-pixel-art1.png')
        portal2 = arcade.load_texture('portal/portal-pixel-art2.png')
        portal3 = arcade.load_texture('portal/portal-pixel-art3.png')
        portal4 = arcade.load_texture('portal/portal-pixel-art4.png')
        portal5 = arcade.load_texture('portal/portal-pixel-art5.png')
        portal6 = arcade.load_texture('portal/portal-pixel-art6.png')
        portal7 = arcade.load_texture('portal/portal-pixel-art7.png')
        portal8 = arcade.load_texture('portal/portal-pixel-art8.png')
        portal9 = arcade.load_texture('portal/portal-pixel-art9.png')
        
        self.portaltextureList = [portal1,portal2,portal3,portal4,portal5,portal6,portal7,portal8,portal9]
        
        self.can_warp = False 
        
        
        self.monster01 = arcade.Sprite('monster1/horrorwalk01.gif',0.5) 
        self.monster01.center_x = 550
        self.monster01.center_y = 100 
        monster01 = arcade.load_texture('monster1/horrorwalk01.gif')
        monster02 = arcade.load_texture('monster1/horrorwalk02.gif')
        monster03 = arcade.load_texture('monster1/horrorwalk03.gif')
        monster04 = arcade.load_texture('monster1/horrorwalk04.gif')
        monster05 = arcade.load_texture('monster1/horrorwalk05.gif')
        self.monsterTextureList = [monster01,monster02,monster03,monster04,monster05]
        self.timegameMonster = 0
        self.frameAtMonster = 0
        self.monster01_life = True
        
        
        self.trap = arcade.Sprite('trap_im/trap1.png',0.2) 
        self.trap.center_x = 350
        self.trap.center_y = 500 
        trap01 = arcade.load_texture('trap_im/trap1.png') 
        trap02 = arcade.load_texture('trap_im/trap2.png') 
        trap03 = arcade.load_texture('trap_im/trap3.png') 
        trap04 = arcade.load_texture('trap_im/trap4.png') 
     
        self.trapTextureList = [trap01,trap02,trap03,trap04]
        self.timegameTrap = 0
        self.frameAtTrap = 0
        
        self.trapHIT = False
        
        self.trap_mirror = arcade.Sprite('trap_im/trap0.png',0.2)  #วิญาณ ของกับดัก
        self.trap_mirror.center_x = 350
        self.trap_mirror.center_y = 500
        
        
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
     
        self.portal.draw() 
        self.hero.draw()
        
        self.monster01.draw()
        self.trap.draw()
        
        arcade.draw_text('main', 10, 20, arcade.color.WHITE, 14)
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.hero.update()
        
        ############### control จุดวาร์ป #################
        self.timegame += delta_time   #คือเวลาที่อยู่ใน game 
        if(self.timegame>0.05):
            self.frameAt +=1 
            self.timegame = 0 
        
        if(self.frameAt==9):
            self.frameAt = 0 
        self.portal.texture = self.portaltextureList[self.frameAt]
        ################### end control จุดวาร์ป #################
        
        
        ################# control Monster ##################
        
        self.timegameMonster += delta_time   #คือเวลาที่อยู่ใน game 
        if(self.timegameMonster>0.1):
            self.frameAtMonster +=1 
            self.timegameMonster = 0 
        
        if(self.frameAtMonster==5):
            self.frameAtMonster = 0 
        self.monster01.texture = self.monsterTextureList[self.frameAtMonster] 
        
        if(self.monster01_life):
            if(self.hero.center_x > self.monster01.center_x):  #แสดงว่า hero อยู่ขวามือของ monster  
                self.monster01.center_x = self.monster01.center_x + 2
            else:
                self.monster01.center_x = self.monster01.center_x - 2
            
            if(self.hero.center_y > self.monster01.center_y):  #แสดงว่า hero อยู่ด้านบนของฉาก อยู่เหนือ monster
                self.monster01.center_y = self.monster01.center_y + 2
            else:
                self.monster01.center_y = self.monster01.center_y - 2
            
        ################# end control Monster #################
        
        
        ################# control trap ##################
        c = arcade.check_for_collision(self.monster01,self.trap_mirror)
        if(c):
          self.trapHIT = True
          self.monster01_life = False
          
        
        if(self.trapHIT):
            self.timegameTrap += delta_time   #คือเวลาที่อยู่ใน game 
            if(self.timegameTrap>0.1):
                self.frameAtTrap +=1 
                self.timegameTrap = 0 
            
            if(self.frameAtTrap==4):
                self.frameAtTrap = 0 
            self.trap.texture = self.trapTextureList[self.frameAtTrap] 
            
        ################# end control trap #################
        
        
        
        
        c = arcade.check_for_collision(self.hero,self.portal)
        if(c): #ถ้า ตัวละคร ชนกับจุดวาร์ป 
            self.can_warp = True 
        else:
            self.can_warp = False 
        
        pass

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        
        if(self.can_warp):
         if key == arcade.key.SPACE:
            self.score = 100
            game_view = GameState1()
            game_view.score = self.score 
            game_view.hero = self.hero
            self.window.show_view(game_view)
            
        
        if key == arcade.key.W:
            self.hero.change_y = 5
              
        if key == arcade.key.S:
            self.hero.change_y = -5
             
        if key == arcade.key.A:
            self.hero.change_x = -5
        
        if key == arcade.key.D:
            self.hero.change_x = 5    
        

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass
        if key == arcade.key.W or key == arcade.key.S:
                self.hero.change_y = 0
                
        if key == arcade.key.A or key == arcade.key.D:
                self.hero.change_x = 0

def main():
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    window.total_score = 0
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
