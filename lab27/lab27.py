import arcade
import random
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class GameState1(arcade.View):
    def __init__(self):
        super().__init__()
        self.hero = arcade.Sprite('hero.png',0.7)
        self.hero.center_x = 100
        self.hero.center_y = 480
        
        self.setup()
        
    def setup(self):
        
        self.bird = arcade.Sprite('bird.png',0.5) 
        self.bird.center_x = 500
        self.bird.center_y = 500
        
        self.bird.change_x = -5
        
        self.stone = arcade.Sprite('stone.png',0.5)
        self.canHit = True 
        self.positionStart = (0,0)
        self.positionTarget = (0,0)
        pass 
    
    '''def on_mouse_press(self, x, y, button, modifiers):
        print("Mouse button is pressed")
        if(self.canHit==True):
          self.positionStart = (self.hero.center_x,self.hero.center_y)
          self.positionTarget = (x,y)
          self.stone.center_x = self.positionStart[0]
          self.stone.center_y = self.positionStart[1]
          self.canHit = False'''
           
    def on_show(self):
        arcade.set_background_color(arcade.color.APPLE_GREEN)
    
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.bird.update()
        self.hero.update()
        self.stone.update()
        
        if(self.stone.center_x > 620):
            self.canHit = True
        
        
        if(self.canHit==False):
            self.stone.center_x +=5
            
        
        if(self.bird.center_x<-10):
            self.bird.center_x = 620
            self.bird.center_y = random.randint(50,550)
        
    def on_draw(self):
        arcade.start_render()
        self.hero.draw()
        self.bird.draw()
        
        if(self.canHit==False):
           self.stone.draw() 
        
       
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('GameState1', 10, 20, arcade.color.WHITE, 14)
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        
        if key == arcade.key.SPACE and self.canHit==True:
            self.canHit = False
            self.stone.center_x = self.hero.center_x
            self.stone.center_y = self.hero.center_y
            self.stone.change_angle = 10
            
        if key == arcade.key.UP:
            self.hero.change_y = 5
        elif key == arcade.key.DOWN:
            self.hero.change_y = -5
        elif key == arcade.key.LEFT:
            self.hero.change_x = -5
        elif key == arcade.key.RIGHT:
            self.hero.change_x = 5

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
            
        if key == arcade.key.UP or key == arcade.key.DOWN:
                self.hero.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.hero.change_x = 0    
            

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()

        arcade.set_background_color(arcade.color.BABY_BLUE)
        self.setup()

    def setup(self):
        
        self.hero = arcade.Sprite('hero.png',0.7)
        self.hero.center_x = 100
        self.hero.center_y = 100
        
        
        self.door = arcade.Sprite('door.png',0.7)
        self.door.center_x = 100
        self.door.center_y = 480
        
        self.statusHitDoor = False
        
        self.door2 = arcade.Sprite('door2.png',0.7) #วิญาณประตู 
        self.door2.center_x = 100
        self.door2.center_y = 480 
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        
        self.door.draw()
        self.hero.draw()
        
         
        arcade.draw_text('main', 10, 20, arcade.color.WHITE, 14)
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        
        self.hero.update()
        if(arcade.check_for_collision(self.hero,self.door2)):  #เราจะชนกับเฉพาะตัวลูกกุญแจ ซึ่งก็คือ door2
              self.statusHitDoor = True 
        else:
              self.statusHitDoor = False  

        
        
        pass
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP:
            self.hero.change_y = 5
        elif key == arcade.key.DOWN:
            self.hero.change_y = -5
        elif key == arcade.key.LEFT:
            self.hero.change_x = -5
        elif key == arcade.key.RIGHT:
            self.hero.change_x = 5

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.SPACE and self.statusHitDoor==True:
            game_view = GameState1()
            self.window.show_view(game_view)
            
        if key == arcade.key.UP or key == arcade.key.DOWN:
                self.hero.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.hero.change_x = 0    
 
        
        

def main():
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
