from turtle import color
import arcade
import random
import math

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class Smoke(arcade.SpriteCircle):
    """ This represents a puff of smoke """
    def __init__(self, size):
        print(2)
        super().__init__(size, arcade.color.LIGHT_GRAY, soft=True)
        self.change_y = 0.5
        self.scale = 0.25
        
        direction = random.randrange(360)
        self.change_x = math.sin(math.radians(direction)) * 1
        self.change_y = math.cos(math.radians(direction)) * 1

    def update(self):
        """ Update this particle """
        print(1)
        if self.alpha <= 10:
            # Remove faded out particles
            self.remove_from_sprite_lists()
            pass
        else:
            # Update values
            self.alpha -= 5
            self.center_x += self.change_x*random.randint(1,3)
            self.center_y += self.change_y*random.randint(1,3)
            self.scale += 0.03
            
class MyGame(arcade.Window):
    """ Main application class. """
    
    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        pass
        self.explosions_list = arcade.SpriteList()
        
        self.timeaction = 0

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here
        
        self.explosions_list.draw()

    def on_update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        
        self.explosions_list.update()
        self.timeaction+=delta_time
        
        k = round(self.timeaction)
        if(k>1):
          self.timeaction = 0
          print(k)  
          for i in range(40):
            try:  
                p1 = Smoke(random.randrange(60))
                p1.center_x = random.randint(280,320)
                p1.center_y = random.randint(280,320)
                
                self.explosions_list.append(p1) 
            except:
                pass
 
        
            
    def on_mouse_press(self, x, y, button, modifiers):
        for i in range(100):
            p1 = Smoke(random.randrange(30))
            p1.center_x = x
            p1.center_y = y+50
            
            self.explosions_list.append(p1) 
     
       

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
