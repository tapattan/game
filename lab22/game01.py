import arcade
import random
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600


class MyGame(arcade.Window):
    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        pass
        
        self.coin = arcade.Sprite('coin_animate2.png',0.2)
        self.coin.center_x = 300
        self.coin.center_y = 300
        
        #texture เปรียบเสมือนเสื้อผ้าของเรา
        namefile = 'coin_animate3.png'
        coin_act1 = arcade.load_texture(namefile,x=0,y=0,width=193,height=172)
        self.coin.texture = coin_act1
        
        coin_act2 = arcade.load_texture(namefile,x=1*193,y=0,width=193,height=172)
        coin_act3 = arcade.load_texture(namefile,x=2*193,y=0,width=193,height=172)
        coin_act4 = arcade.load_texture(namefile,x=3*193,y=0,width=193,height=172)
        coin_act5 = arcade.load_texture(namefile,x=790,y=0,width=193,height=172)
        coin_act6 = arcade.load_texture(namefile,x=1027,y=0,width=1158-1027,height=172)

        self.timegame = 0
        self.frameAt = 0
        self.textureList = [coin_act1,coin_act2,coin_act3,coin_act4,coin_act5,coin_act6]
        
        
        
        self.mario = arcade.Sprite('5-mario-running1.png',1)
        mario_act1 = arcade.load_texture('5-mario-running1.png',x=0,y=0,width=80,height=80)
        mario_act2 = arcade.load_texture('5-mario-running2.png',x=0,y=0,width=80,height=80)
        
        self.textureListMario = [mario_act1,mario_act2]
        
        self.mario.center_x = 100
        self.mario.center_y = 100
        self.timegameMario = 0
        self.frameAtMario = 0 
    
    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.coin.draw()
        self.mario.draw()
        

    def update(self, delta_time):
        
        self.coin.update()
        self.mario.update()
        
        self.timegame += delta_time   #คือเวลาที่อยู่ใน game 
 
        if(self.timegame>0.2):
            self.frameAt +=1 
            self.timegame = 0 
        
        if(self.frameAt==6):
            self.frameAt = 0 
        
        self.coin.texture = self.textureList[self.frameAt]  
        
        
        
        self.timegameMario += delta_time   #คือเวลาที่อยู่ใน game 
       
        if(self.timegameMario>0.2):
            self.frameAtMario +=1 
            self.timegameMario = 0 
        
        if(self.frameAtMario==2):
            self.frameAtMario = 0 
        
        self.mario.texture = self.textureListMario[self.frameAtMario]     

        
        pass
       
    
    def on_key_press(self, key, modifiers):
       pass
        

    def on_key_release(self, key, modifiers):
       pass
   
def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()