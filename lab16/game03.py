import arcade
from arcade.experimental.lights import Light, LightLayer

import random 

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
MOVEMENT_SPEED = 5

AMBIENT_COLOR = (10, 10, 10)

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height,'เกมของชั้น 🔦')
        arcade.set_background_color(arcade.color.AMAZON)
 

    def setup(self):
        # Set up your game here
        pass
    
        self.player_sprite = arcade.Sprite("sun.png",0.2)
        self.player_sprite.center_x = 250
        self.player_sprite.center_y = 250
        
        self.background =arcade.load_texture("bg.jpg")
    
    
        # Create a light layer, used to render things to, then post-process and
        # add lights. This must match the screen size.
        self.light_layer = LightLayer(SCREEN_WIDTH, SCREEN_HEIGHT)
        # We can also set the background color that will be lit by lights,
        # but in this instance we just want a black background
        self.light_layer.set_background_color(arcade.color.BLACK)
        
        # Create a small white light
        x = random.randint(0,SCREEN_WIDTH)
        y = random.randint(0,SCREEN_HEIGHT)
        radius = 50
        mode = 'soft'
        color = arcade.csscolor.WHITE
        self.light = Light(x, y, radius, color, mode)
        self.light_layer.add(self.light)
        
        
        
        #radius = 150
        #mode = 'soft'
        #color = arcade.csscolor.WHITE
        #self.player_light = Light(150, 250, radius, color, mode)
        
        
        #self.light_layer.add(self.player_light)

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        
        with self.light_layer:
          arcade.draw_texture_rectangle(300, 300, 600,
                                      600, self.background)
          #self.player_sprite.draw()
          print(1)
            
        # Your drawing code goes here
        self.light_layer.draw(ambient_color=AMBIENT_COLOR)
   

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.player_sprite.update()
        self.light.position = self.player_sprite.position
        
        if(self.player_sprite.center_x>600):
           self.player_sprite.center_x = 600
        elif(self.player_sprite.center_x<0):
               self.player_sprite.center_x = 0   
               
        if(self.player_sprite.center_y>600):
               self.player_sprite.center_y = 600
        elif(self.player_sprite.center_y<0):
               self.player_sprite.center_y = 0  
            
        
        pass
    
    def on_key_press(self, key, _):
        """Called whenever a key is pressed. """

        if key == arcade.key.W:
            self.player_sprite.change_y = MOVEMENT_SPEED
        elif key == arcade.key.S:
            self.player_sprite.change_y = -MOVEMENT_SPEED
        elif key == arcade.key.A:
            self.player_sprite.change_x = -MOVEMENT_SPEED
        elif key == arcade.key.D:
            self.player_sprite.change_x = MOVEMENT_SPEED
       

    def on_key_release(self, key, _):
        """Called when the user releases a key. """

        if key == arcade.key.W or key == arcade.key.S:
            self.player_sprite.change_y = 0
        elif key == arcade.key.A or key == arcade.key.D:
            self.player_sprite.change_x = 0

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()