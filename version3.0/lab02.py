import arcade
import random
SCREEN_WIDTH = 300
SCREEN_HEIGHT = 300

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        
        self.player_list = arcade.SpriteList()
        for i in range(30):
            s1 = arcade.Sprite('star.png')
            s1.scale = 0.05
            s1.center_x = random.randint(0,300)
            s1.center_y = random.randint(0,300)
            s1.velocity_x = random.choice([-5,-4,-3,-2,-1,1,2,3,4,5])
            s1.velocity_y = random.choice([-5,-4,-3,-2,-1,1,2,3,4,5])
            self.player_list.append(s1)

    def on_draw(self):
        """ Render the screen. """
        self.clear() # เปลี่ยนมาใช้ self.clear
        self.player_list.draw()

    def on_update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        
        # You can still use this to apply any additional updates like animations
        self.player_list.update()
        print(delta_time)
        # Update positions based on a set velocity rather than random repositioning
        for sprite in self.player_list:
            # Assign a random velocity if not already assigned
            sprite.center_x += sprite.velocity_x
            sprite.center_y += sprite.velocity_y
            
            # Check for boundary collision and reverse velocity if necessary
            if sprite.center_x < 0 or sprite.center_x > SCREEN_WIDTH:
                sprite.velocity_x *= -1
            if sprite.center_y < 0 or sprite.center_y > SCREEN_HEIGHT:
                sprite.velocity_y *= -1

    

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
