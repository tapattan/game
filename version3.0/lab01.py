import arcade

# Set constants for the screen size
SCREEN_WIDTH = 300
SCREEN_HEIGHT = 300

# Open the window. Set the window title and dimensions (width and height)
arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "Drawing Example")

# Set the background color to white.
# For a list of named colors see:
# http://arcade.academy/arcade.color.html
# Colors can also be specified in (red, green, blue) format and
# (red, green, blue, alpha) format.
arcade.set_background_color(arcade.color.WHITE)

# Start the render process. This must be done before any drawing commands.
arcade.start_render()
 
player_list = arcade.SpriteList()

s1 = arcade.Sprite('star.png')
s1.scale = 0.1
s1.center_x = 150
s1.center_y = 150

player_list.append(s1)

player_list.draw()  #### ใน version 3.0 ต้องใช้การ draw ใน SpriteList แทน

# Finish drawing and display the result
arcade.finish_render()

# Keep the window open until the user hits the 'close' button
arcade.run()
