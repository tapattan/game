# ถ้าต้องการลง version 2.6.17
pip uninstall arcade

pip install arcade==2.6.17

หรือ

python3 -m pip install arcade==2.6.17

# ตรวจสอบ version
pip list

หรือจะดูรายละเอียด

pip show arcade