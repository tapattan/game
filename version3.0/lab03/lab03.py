import arcade

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

MOVEMENT_SPEED = 2

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)
    
    def setup(self):
        # Set up your game here
        self.mySprite = arcade.Sprite('hero.png') #SCALING = 0.3 
        self.mySprite.scale = 0.1
        self.mySprite.center_x = 300 
        self.mySprite.center_y = 300

        self.obj_list = arcade.SpriteList()
        self.obj_list.append(self.mySprite)

        self.skin_left = arcade.load_texture('heroL.png')
        self.skin_right = arcade.load_texture('hero.png')
        

        self.power_list = arcade.SpriteList()
        self.power = arcade.Sprite('fireR.png')
        self.power.scale = 0.25
        self.power_list.append(self.power)

        self.hit = False
        self.direct = 'right'
        self.skin_powerleft = arcade.load_texture('fireL.png')
        self.skin_powerright = arcade.load_texture('fireR.png')
        #pass

    def on_draw(self):
        """ Render the screen. """
        #arcade.start_render()
        # Your drawing code goes here
        self.clear()
        if(self.hit==True):
          self.power_list.draw()

        self.obj_list.draw()

    def on_update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass

        self.mySprite.update()
        self.power.update()
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.W:
            self.mySprite.change_y = MOVEMENT_SPEED  
        elif key == arcade.key.S:
            self.mySprite.change_y = -MOVEMENT_SPEED
        elif key == arcade.key.A:
            self.mySprite.change_x = -MOVEMENT_SPEED
            self.mySprite.texture = self.skin_left
            self.direct = 'left'
        elif key == arcade.key.D:
            self.mySprite.change_x = MOVEMENT_SPEED
            self.mySprite.texture = self.skin_right
            self.direct = 'right'

        elif key == arcade.key.SPACE:
            self.hit = True

            if(self.direct == 'right'):
              self.power.change_x = 5
              self.power.texture = self.skin_powerright
            if(self.direct == 'left'):
              self.power.change_x = -5
              self.power.texture = self.skin_powerleft

            self.power.center_x = self.mySprite.center_x
            self.power.center_y = self.mySprite.center_y
         
 
    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.W or key == arcade.key.S:
            self.mySprite.change_y = 0
        elif key == arcade.key.A or key == arcade.key.D:
            self.mySprite.change_x = 0

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
