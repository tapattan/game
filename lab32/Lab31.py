import arcade
import random
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class GameOver(arcade.View):
    def __init__(self):
        super().__init__()
        self.setup()
    
    def setup(self):
        pass

    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('GameOver', 220, 300, arcade.color.WHITE, 30)
        arcade.draw_text("press 'spacebar' new game", 150, 240, arcade.color.WHITE, 20)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.SPACE:
            game_view = MyGame_state1()  #กลับไปฉากแรก 
            self.window.show_view(game_view)

            
    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass

 
class MyGame_state1(arcade.View):
    def __init__(self):
        super().__init__()
        self.setup()
    
    def setup(self):
        pass
        ############################# Mario #############################################
        self.mario = arcade.Sprite('5-mario-running-01.gif',1) 
        self.mario.center_x = 550
        self.mario.center_y = 600
        
        self.textureList = [] 
        mario1 = arcade.load_texture('5-mario-running-01.gif')
        mario2 = arcade.load_texture('5-mario-running-02.gif')
        self.textureList.append(mario1)
        self.textureList.append(mario2)
        
        self.textureList_left = [] 
        mario1_left = arcade.load_texture('5-mario-running-01_left.gif')
        mario2_left = arcade.load_texture('5-mario-running-02_left.gif')
        self.textureList_left.append(mario1_left)
        self.textureList_left.append(mario2_left)
        
        self.direct = 'right'
        #pass
    
        self.gametime = 0
        self.frameAt = 0
        self.jump = 'no'
        self.timejump = 0

        ############################## End Mario ##################### 
        
        self.hitObject = 'close'

        ################## chest######################################
        self.chest = arcade.Sprite('chest/frame_00.gif')
        self.chest.center_x = 300
        self.chest.center_y = 300
        self.chest.scale = 0.3

        self.gametime_chest = 0
        self.frameAt_chest = 0

        self.textureList_chest = [] 
        chest1 = arcade.load_texture('chest/frame_00.gif')
        chest2 = arcade.load_texture('chest/frame_01.gif')
        chest3 = arcade.load_texture('chest/frame_02.gif')
        chest4 = arcade.load_texture('chest/frame_03.gif')
        chest5 = arcade.load_texture('chest/frame_04.gif')
        chest6 = arcade.load_texture('chest/frame_05.gif')
        chest7 = arcade.load_texture('chest/frame_06.gif')
        chest8 = arcade.load_texture('chest/frame_07.gif')
        self.textureList_chest.append(chest1)
        self.textureList_chest.append(chest2)
        self.textureList_chest.append(chest3)
        self.textureList_chest.append(chest4)
        self.textureList_chest.append(chest5)
        self.textureList_chest.append(chest6)
        self.textureList_chest.append(chest7)
        self.textureList_chest.append(chest8)

        self.textureList_chest_close = [] 
        chest1 = arcade.load_texture('chest/frame_08.gif')
        chest2 = arcade.load_texture('chest/frame_09.gif')
        chest3 = arcade.load_texture('chest/frame_10.gif')
        chest4 = arcade.load_texture('chest/frame_11.gif')
        chest5 = arcade.load_texture('chest/frame_12.gif')
        chest6 = arcade.load_texture('chest/frame_13.gif')
        chest7 = arcade.load_texture('chest/frame_14.gif')
        self.textureList_chest_close.append(chest1)
        self.textureList_chest_close.append(chest2)
        self.textureList_chest_close.append(chest3)
        self.textureList_chest_close.append(chest4)
        self.textureList_chest_close.append(chest5)
        self.textureList_chest_close.append(chest6)
        self.textureList_chest_close.append(chest7)

        ##################### end chest ################################


        self.chestList = arcade.SpriteList(use_spatial_hash=True)   #SpriteList 
        self.chestList.append(self.chest)

        self.physics_engine = arcade.PhysicsEnginePlatformer(
           self.mario, self.chestList, gravity_constant=0) # 0 คือ bird eye view

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.physics_engine.update()

        self.chest.update()
        self.mario.update() 
        
        ####################### Mario animation #########################
        self.gametime = self.gametime+delta_time 
        if(self.gametime>0.2):
           self.gametime = 0 
           self.frameAt = self.frameAt + 1
        
        if(self.frameAt==2):
           self.frameAt = 0 
        
        if(self.direct=='right'):   
          self.mario.texture = self.textureList[self.frameAt] #ก็คือใส่ให้ตัวละครของเราเปลี่ยนชุด ในตำแหน่ง texturelist นั้นๆ 
        elif(self.direct =='left'):
          self.mario.texture = self.textureList_left[self.frameAt]    #อันนี้คือเสื้อผ้าของหันซ้าย
        ############################## End ##########################


        ####################### Chest animation #########################
        if(self.hitObject=='open'):
          self.gametime_chest = self.gametime_chest+delta_time 
          if(self.gametime_chest>0.2):
            self.gametime_chest = 0 
            self.frameAt_chest = self.frameAt_chest + 1
        
          if(self.frameAt_chest==8):
            self.frameAt_chest = 7 #frame สุดท้าย
            self.hitObject = 'openmax' 
        
          self.chest.texture = self.textureList_chest[self.frameAt_chest]    #อันนี้คือเสื้อผ้าของหันซ้าย
          if(self.hitObject=='openmax'):
             self.frameAt_chest = 0 
        
        if(self.hitObject=='will_close'):
          self.gametime_chest = self.gametime_chest+delta_time 
          if(self.gametime_chest>0.2):
            self.gametime_chest = 0 
            self.frameAt_chest = self.frameAt_chest + 1
        
          if(self.frameAt_chest==7):
            self.frameAt_chest = 6 #frame สุดท้าย
            self.hitObject = 'close' 
        
          self.chest.texture = self.textureList_chest_close[self.frameAt_chest]    #อันนี้คือเสื้อผ้าของหันซ้าย
          if(self.hitObject=='close'):
             self.frameAt_chest = 0 

        ############################## End ##########################

        c = arcade.check_for_collision(self.mario,self.chest)
        if(c and self.hitObject=='close'): #มันปิดอยู่นะ
           self.hitObject = 'open'
           print('hit chest')
           frameAt_chest = 0
        
  
        absX = abs(self.mario.center_x - self.chest.center_x)
        absY = abs(self.mario.center_y - self.chest.center_y)
        #print(absX,absY)
        if(absX>60 or absY>60):
           print('ห่างนะ')
           if(self.hitObject=='openmax'):
               self.hitObject='will_close'
               self.frameAt_chest = 0
    
            

        
        
       
    def on_show(self):
        arcade.set_background_color(arcade.color.BABY_BLUE)

    def on_draw(self):
        arcade.start_render()
        self.chest.draw()
        self.mario.draw()

        pass 
        arcade.draw_text('game state1', 10, 10, arcade.color.WHITE, 10)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        
        if key == arcade.key.A:
            self.mario.change_x = -5
            self.direct = 'left'
        elif key == arcade.key.D:
            self.mario.change_x = 5
            self.direct = 'right'
        elif key == arcade.key.W:
            self.mario.change_y = 5
        elif key == arcade.key.S:
            self.mario.change_y = -5    
            
    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.A or key == arcade.key.D:
            self.mario.change_x = 0
        if key == arcade.key.W or key == arcade.key.S:
            self.mario.change_y = 0
 ##################################################################  End ################################################################# 


def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame_state1()  # จุด start game
    window.show_view(game)
    arcade.run()



if __name__ == "__main__":
    main()
