import arcade
import random
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600


class MyGame(arcade.Window):
    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        pass
        self.mySprite = arcade.Sprite('hero.png', 1) #SCALING = 0.3 
        self.mySprite.center_x = 30 
        self.mySprite.center_y = 300
        self.direct = 1
        self.textureR = arcade.load_texture("hero.png")
        self.textureL = arcade.load_texture("hero2.png")
  
        self.basket = [] #สร้างตะกร้าเก็บผลไม้เอาไว้ 
        
        #สร้าง apple ขึ้น 10 ลูกแบบสุ่มตำแหน่ง 
        for i in range(10):  
          myApple = arcade.Sprite('apple.png', 0.01) #SCALING = 0.3   
          myApple.center_x = random.randint(30,580)
          myApple.center_y = random.randint(30,580)
          
          #แล้วยัด apple ลงไปในตะกร้า
          self.basket.append(myApple)
          
          self.walk = 0
          
          self.score = 0
        

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        
        self.mySprite.draw()  #ทำการวาด ตัวละครออกไป 
        
        for i in self.basket:
            i.draw()
            
        # Your drawing code goes here
        arcade.draw_text("Score : "+str(self.score),
                         10,
                         10,
                         arcade.color.BLACK)

    def update(self, delta_time):
        
        self.mySprite.update() #คำนวณ ค่าต่างๆ ของตัวละครใหม่
        
        print(self.walk)
        if(self.walk==0):
            self.mySprite.change_y = 0
            self.mySprite.change_x = 0
                
                
        for i in self.basket: #วนลูปใน basket
            
            #ทำการตรวจสอบว่า ตัวละครของเรา ชนกับ apple ในตะกร้าไหม
            c = arcade.check_for_collision(self.mySprite,i)
            if(c): #ถ้าชน เราจะทำการย้ายตำแหน่งให้ออกนอกจอไป 
               i.center_x = random.randint(30,580)     
               i.center_y = random.randint(30,580) 
               
               self.score +=1
                
        #self.myApple.update()
        """ All the logic to move, and the game logic goes here. """
        pass
        """print(self.mySprite.center_x)
        self.mySprite.center_x = self.mySprite.center_x + self.direct
        if self.mySprite.center_x >= 772:
          self.direct = -1
          self.mySprite.texture = arcade.load_texture("hero2.png") 
        elif self.mySprite.center_x <= 30:
           self.direct = 1
           self.mySprite.texture = arcade.load_texture("hero.png")"""
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        
        self.walk+=1   #นับจำนวนของนิ้วที่กดลงมา 
        
        if key == arcade.key.W:
            self.mySprite.change_y = 5
              
        if key == arcade.key.S:
            self.mySprite.change_y = -5
             
        if key == arcade.key.A:
            self.mySprite.change_x = -5
            self.mySprite.texture = self.textureL
        
        if key == arcade.key.D:
            self.mySprite.change_x = 5
            self.mySprite.texture = self.textureR
         
      

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
   
        self.walk-=1 
        
        #ถ้าเทอ ปล่อยนิ้ว พร้อมกัน หมด self.walk ต้องเป็นศูนย์ ถึงจะหยุดการเคลื่อนที่
        if(self.walk==0):
            if key == arcade.key.W or key == arcade.key.S:
                self.mySprite.change_y = 0
                
            if key == arcade.key.A or key == arcade.key.D:
                self.mySprite.change_x = 0
def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()