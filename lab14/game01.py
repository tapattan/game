import arcade
import pyglet
import random 
import numpy as np
 
SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500

import pyautogui

class trash:
    sp : arcade.Sprite 
    status : bool 
    def __init__(self,sp,status):
       self.sp = sp
       self.status = status 

class GameWin(arcade.View):
    def __init__(self):
        super().__init__()
        self.setup()
    
    def setup(self):
        pass
         
    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('You Win', (SCREEN_WIDTH/2)-90, SCREEN_HEIGHT/2, 
                                arcade.color.WHITE, 30,font_name="comic")
    
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        #print('state1',self.score)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.SPACE:
            #self.score = self.score + 100
            game_view = MyGame()
            #game_view.score = self.score
            self.window.show_view(game_view)
        

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass

class GameOver(arcade.View):
    def __init__(self):
        super().__init__()
        self.setup()
    
    def setup(self):
        pass
         
    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('GameOver', (SCREEN_WIDTH/2)-90, SCREEN_HEIGHT/2, 
                                arcade.color.WHITE, 30,font_name="comic")
    
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        #print('state1',self.score)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.SPACE:
            #self.score = self.score + 100
            game_view = MyGame()
            #game_view.score = self.score
            self.window.show_view(game_view)
        

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass
 
class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        
        arcade.set_background_color(arcade.color.AQUAMARINE)
     
        print('init')
        value = self.window.get_location()
        pyautogui.click(x=value[0]+50, y=value[1]+100)

        self.setup()

    def setup(self):
        # Set up your game here
        

        self.sea = arcade.Sprite('../resources/sea.png',0.4) #None’, ‘Simple’ or ‘Detailed’.
    
        self.sea.center_x = 110
        self.sea.center_y = 200
        self.timedelta = 0
        self.timedelayWin = 0
        self.deleyWinAct = False

        self.color_sea = '#000000'
        
        self.fish = arcade.Sprite('../resources/fish.png',0.03)   
        self.fishtextureToR = arcade.load_texture("../resources/fish.png",
                                      flipped_horizontally=True)
        self.fishtextureToL = arcade.load_texture("../resources/fish.png",
                                      flipped_horizontally=False)                              


        self.fish.texture = self.fishtextureToR                              

        self.fish.change_x = 5
        self.fish.center_x = 200
        self.fish.center_y = 200

        self.hook = arcade.Sprite('../resources/hook500.png',1) #None’, ‘Simple’ or ‘Detailed’. ,hit_box_algorithm='Detailed'
        self.hook_obj = arcade.Sprite('../resources/hook_obj500.png',1) #None’, ‘Simple’ or ‘Detailed’. ,hit_box_algorithm='Detailed'
        self.hook.center_y = 500
        self.hook.center_x = 50
  

        self.col_trash = []
        for i in range(9):
          if(i<3):
            obj = arcade.Sprite('../resources/can.png',0.05) 
          elif(i<6):
            obj = arcade.Sprite('../resources/boot.png',0.05)   
          else:
            obj = arcade.Sprite('../resources/bottle.png',0.1)   

          obj.center_x = random.randint(10,490)
          obj.center_y = random.randint(10,300)
        
          obj.angle = random.randint(-360,360)
          self.col_trash.append(trash(obj,False))

        self.max_trash = len(self.col_trash)
        self.score = (self.max_trash - len(self.col_trash))*int(np.floor((255/self.max_trash)))
        
        
     
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        #self.hook_obj.draw_hit_box(color=[255,0,0])

        self.sea.color = arcade.color_from_hex_string(self.color_sea)
        self.sea.draw()

        for i in self.col_trash:
            i.sp.draw()
        

        self.fish.draw()
        self.hook.draw()
        
   
    
    #Called when the user presses a mouse button
    def on_mouse_press(self, x, y, button, key_modifiers):
         
        pass

    #Called when the user presses a mouse button.
    def on_mouse_release(self, x: float, y: float, button: int, modifiers: int):
        self.hook.angle = 0
         
        pass

    #User moves mouse
    def on_mouse_motion(self, x: float, y: float, dx: float, dy: float):
        self.hook.center_x = x 
        self.hook.center_y = y*1.6

        self.hook_obj.center_x = self.hook.center_x 
        self.hook_obj.center_y = self.hook.center_y
        
        for i in self.col_trash:
          if(i.status):
            i.sp.center_x = self.hook_obj.center_x
            i.sp.center_y = self.hook_obj.center_y-200
            if(i.sp.center_y<50):
               i.sp.center_y = 50

        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass  

        
        
        #print(self.hook.center_x,self.hook.center_y)

        self.fish.update()
        if(self.fish.center_x>550):
           self.fish.change_x = -5
           self.fish.center_y = random.randint(50,300)
           self.fish.texture = self.fishtextureToL
        elif(self.fish.center_x<-50):
           self.fish.change_x = 5
           self.fish.center_y = random.randint(50,300)
           self.fish.texture = self.fishtextureToR
        
        if(self.hook_obj.center_y>750):
            for i in range(len(self.col_trash)):
                if(self.col_trash[i].status):
                    del self.col_trash[i]
                    self.score = (self.max_trash - len(self.col_trash))*int(np.floor((255/self.max_trash)))
                    break

        if(self.hook.center_y<=250):
            self.hook.center_y = 250

        self.timedelta +=delta_time
 
        if(int(self.timedelta)%2==0):
            self.sea.center_y = self.sea.center_y-1
        else:
            self.sea.center_y = self.sea.center_y+1

        
        for i in self.col_trash:
          hit_hook = arcade.check_for_collision(self.hook_obj,i.sp)
          if(hit_hook):
            i.status = True
          
       
        
        hit_fish = arcade.check_for_collision(self.hook_obj,self.fish)
        if(hit_fish):
            game_view = GameOver()
            self.window.show_view(game_view)

        if(len(self.col_trash)==0):
           self.timedelayWin += delta_time
           if(self.timedelayWin>3 and self.deleyWinAct==False):
               if(self.fish.center_x>240 and self.fish.center_x<260):
                 self.fish.change_x = 0 
                 self.fish.scale = 0.1 
                 self.deleyWinAct = True
                 self.timedelayWin = 0
           elif(self.timedelayWin>1 and self.deleyWinAct==True):  
                 game_view = GameWin()
                 self.window.show_view(game_view)

                
        self.color_sea = '#%02X%02X%02X' % (255,255,self.score )

        


def main(): 
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "🐶  🕶  แบบฝึกหัดเกมของฉัน")
    #window.total_score = 0
    window.set_mouse_visible(True)
    game = MyGame() 

    #icon1 = pyglet.image.load('apple.ico')
    #game.set_icon(icon1)
    #window.set_location(100,100)
    game.window = window 
    
 

    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
