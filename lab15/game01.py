import arcade

SCREEN_WIDTH = 640
SCREEN_HEIGHT = 640
MOVEMENT_SPEED = 5 
            
from level.GameState1 import GameState1


class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        print('____',arcade.__version__)

        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        # Set up your game here
        pass
        
        
    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
       

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        game_view = GameState1()
        self.window.show_view(game_view)

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")

    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
