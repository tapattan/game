import arcade

class small_metal(arcade.Sprite):
    def __init__(self,X,Y):
        super().__init__()   
        self.texturesAction = []
        self.texturesAction.append(arcade.load_texture("small_metal/small_metal_spike_01.png"))
        self.texturesAction.append(arcade.load_texture("small_metal/small_metal_spike_02.png"))
        self.texturesAction.append(arcade.load_texture("small_metal/small_metal_spike_03.png"))
        
        self.textureList = self.texturesAction 
        self.scale = 0.5
        self.timeFrame = 2
        self.timeKFrame = 2
        self.center_x = X 
        self.center_y = Y
    
    def update_animation(self, delta_time: float = 1 / 20):

        self.statusFrameaction = len(self.textureList)
        self.timeKFrame +=(delta_time)
        if(self.timeKFrame>2):
            self.timeKFrame=0
            self.timeFrame+=1
            if(self.timeFrame%self.statusFrameaction==0):
                self.timeFrame = 0
            
            try:
              self.texture = self.textureList[self.timeFrame]
            except:
              pass