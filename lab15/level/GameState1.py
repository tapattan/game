import arcade 
from level.gameOver import gameOver 
from level.GameState2 import GameState2 
from level.hero import hero 
MOVEMENT_SPEED = 5  

class GameState1(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()

        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        # Set up your game here
        #pass
        
        self.hero = hero()

        ### zone load map ########
        # Name of map file to load
        map_name = "map01-a.json"

        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "LayerGrass": {
                "use_spatial_hash": True,
            },
            "LayerWall": {
                "use_spatial_hash": True,
            },
        }

        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)


        #self.LayerGrass = self.tile_map.sprite_lists["background"]
        self.LayerWall = self.tile_map.sprite_lists["background"]
        self.floor = self.tile_map.sprite_lists["floor"]

        # Keep player from running through the wall_list layer
        walls = [self.LayerWall, ]
        self.physics_engine = arcade.PhysicsEnginePlatformer(
            self.hero, walls, gravity_constant=1
        )  
        
        
        self.LayerWall2 = self.tile_map.sprite_lists["background2"]
        self.floor2 = self.tile_map.sprite_lists["floor2"]
        # Keep player from running through the wall_list layer
        walls2 = [self.LayerWall2, ]
        self.physics_engine2 = arcade.PhysicsEnginePlatformer(
            self.hero, walls2, gravity_constant=1
        )  
        
        ### end zone load map ####
        self.timetrapHold = 0
        self.startTrapHold = False
         

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.hero.draw()
        # Your drawing code goes here
        #self.LayerGrass.draw()
        
        if( self.startTrapHold ==True):
          self.LayerWall2.draw()
        else:
          self.LayerWall.draw()  
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        if(self.hero.jump_status==True):
            self.startTrapHold = True
        
        if(self.startTrapHold==True):
          self.timetrapHold  += delta_time
          self.physics_engine2.update()
        else:
          self.physics_engine.update()  
        
        if(self.timetrapHold>2):
            self.startTrapHold = False
            self.timetrapHold = 0
        
        
        
        if(self.hero.center_y < -1):
            arcade.play_sound(self.hero.sound_dead)
            self.hero.center_y = 1000
            
            game_view = gameOver()
            self.window.show_view(game_view)
            
        
        if(self.hero.center_x>640):
            game_view = GameState2(self.hero)
            self.window.show_view(game_view)
        
        
        soul = self.hero 
        soul.center_y = self.hero.center_y - 1
        hit_list = arcade.check_for_collision_with_list(soul,self.floor)
        k = len(hit_list)
     
        if(k>0):
            self.hero.jump_status = False
            
            
        

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP or key == arcade.key.W and self.hero.jump_status == False:
            self.hero.center_y += 70
            self.hero.jump_status = True
                
            #self.hero.texture = self.hero.texturesUp
        elif key == arcade.key.DOWN or key == arcade.key.S:
            #self.hero.change_y = -MOVEMENT_SPEED w
            self.hero.texture = self.hero.texturesDown
        elif key == arcade.key.LEFT or key == arcade.key.A:
            self.hero.change_x = -MOVEMENT_SPEED
            self.hero.texture = self.hero.texturesLeft
        elif key == arcade.key.RIGHT or key == arcade.key.D:
            self.hero.change_x = MOVEMENT_SPEED
            self.hero.texture = self.hero.texturesRight
       

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP or key == arcade.key.DOWN or key == arcade.key.W or key == arcade.key.S:
            self.hero.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT or key == arcade.key.A or key == arcade.key.D:
            self.hero.change_x = 0