import arcade 
SCREEN_WIDTH = 640
SCREEN_HEIGHT = 640

class gameWin(arcade.View):
    def __init__(self):
        # Set up parent class
        super().__init__()
        arcade.set_background_color(arcade.color.BLACK)
    def setup(self):
        pass
    
    def on_draw(self):
        arcade.start_render()
        arcade.draw_text('You Win', SCREEN_WIDTH/2-80, SCREEN_HEIGHT/2, arcade.color.WHITE, 30)
        arcade.draw_text('Press Space Bar New Game', SCREEN_WIDTH/2-120, SCREEN_HEIGHT/2-100, arcade.color.WHITE, 15)
        
    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        if key == arcade.key.SPACE:
            from level.GameState1 import GameState1 
            game_view = GameState1()
            self.window.show_view(game_view)