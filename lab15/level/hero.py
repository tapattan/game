import arcade

class hero(arcade.Sprite):
    def __init__(self):
        
        # Set up parent class
        super().__init__()
 
        self.center_x = 100
        self.center_y = 700
        self.scale = 0.02
        
        self.texturesDown = arcade.load_texture("character/down.png")

        self.texturesLeft = arcade.load_texture("character/left.png")

        self.texturesRight = arcade.load_texture("character/right.png")

        self.texturesUp = arcade.load_texture("character/up.png") 
        
        self.texture = self.texturesRight
        
        self.sound_dead = arcade.load_sound('sound/life-lost.mp3')
        self.sound_win = arcade.load_sound('sound/smb_stage_clear.wav') 
        self.jump_status = False