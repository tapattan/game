import arcade 
from level.small_metal import small_metal
from level.GameState4 import GameState4
from level.gameOver import gameOver  
MOVEMENT_SPEED = 5 

class GameState3(arcade.View):
    def __init__(self,hero):
        
        super().__init__()
        self.hero = hero 
        self.hero.center_x = 10 #ให้เกิดข้างซ้ายจอ
        self.setup()
        
    
    def setup(self):
        
        #zone load map load แผนที่ ####
        map_name = "map03.json"

        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "LayerGrass": {
                "use_spatial_hash": True,
            },
            "LayerWall": {
                "use_spatial_hash": True,
            },
        }

        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)


        #self.LayerGrass = self.tile_map.sprite_lists["background"]
        self.LayerWall = self.tile_map.sprite_lists["background"]

        # Keep player from running through the wall_list layer
        walls = [self.LayerWall, ]
        self.physics_engine = arcade.PhysicsEnginePlatformer(
            self.hero, walls, gravity_constant=1
        )  
        ### end zone load map ####
        
        
        self.list_small_metal = []
        self.small_metal1 = small_metal(220,210)
        small_metal2 = small_metal(500,210)
        small_metal3 = small_metal(300,227)
        small_metal3.angle = 180
        self.small_metal4 = small_metal(140,227)
        self.small_metal4.angle = 180
        self.list_small_metal.append(self.small_metal1)
        self.list_small_metal.append(small_metal2)
        self.list_small_metal.append(small_metal3)
        self.list_small_metal.append(self.small_metal4)
        
        self.timetrapmove = 0
 
        pass
         
    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass
        self.hero.draw() 
        self.LayerWall.draw()
        
        for i in self.list_small_metal:
          i.draw()
      
        #arcade.draw_text('GameState1 : '+str(self.score), 10, 20, arcade.color.WHITE, 14)
    
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.physics_engine.update()
        
        self.hero.update()
        for i in self.list_small_metal:
             i.update_animation()
             i.update()
        
        self.timetrapmove += round(delta_time,5)
    
        if(round(self.timetrapmove,0)>2):
            if(self.hero.center_x > self.small_metal4.center_x):
                self.small_metal4.change_x = 2
            else:
                self.small_metal4.change_x = -2
            
        
    
                
        
        
        
        if(self.hero.center_x<=2):
            self.hero.center_x = 2
        
        for i in self.list_small_metal:
         if(arcade.check_for_collision(self.hero,i)):
      
            if(i.timeFrame>=1 and i.timeKFrame>1):
                arcade.play_sound(self.hero.sound_dead)
            
                game_view = gameOver()
                self.window.show_view(game_view)
        
        
        if(self.hero.center_x>640):
            game_view = GameState4(self.hero)
            self.window.show_view(game_view) 
        pass
        

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP or key == arcade.key.W:
            self.hero.center_y += 70
            #self.hero.texture = self.hero.texturesUp
        elif key == arcade.key.DOWN or key == arcade.key.S:
            #self.hero.change_y = -MOVEMENT_SPEED
            self.hero.texture = self.hero.texturesDown
        elif key == arcade.key.LEFT or key == arcade.key.A:
            self.hero.change_x = -MOVEMENT_SPEED
            self.hero.texture = self.hero.texturesLeft
        elif key == arcade.key.RIGHT or key == arcade.key.D:
            self.hero.change_x = MOVEMENT_SPEED
            self.hero.texture = self.hero.texturesRight
       

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP or key == arcade.key.DOWN or key == arcade.key.W or key == arcade.key.S:
            self.hero.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT or key == arcade.key.A or key == arcade.key.D:
            self.hero.change_x = 0 