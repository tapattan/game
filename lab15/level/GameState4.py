import arcade 
MOVEMENT_SPEED = 5 
from level.gameOver import gameOver  
from level.gameWin import gameWin

class GameState4(arcade.View):
    def __init__(self,hero):
        
        super().__init__()
        self.hero = hero 
        self.hero.center_x = 30
        self.hero.center_y = 210
        self.setup()
        
    
    def setup(self):
        
        #zone load map load แผนที่ ####
        map_name = "map04-a.json"

        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "LayerGrass": {
                "use_spatial_hash": True,
            },
            "LayerWall": {
                "use_spatial_hash": True,
            },
        }

        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)


        #self.LayerGrass = self.tile_map.sprite_lists["background"]
        self.LayerWall = self.tile_map.sprite_lists["background"]
        self.floor = self.tile_map.sprite_lists["floor"]
        
        # Keep player from running through the wall_list layer
        walls = [self.LayerWall, ]
        self.physics_engine = arcade.PhysicsEnginePlatformer(
            self.hero, walls, gravity_constant=1
        )  
        ### end zone load map ####
        
        self.rock = arcade.Sprite('rock.png')
        self.rock.change_angle =5 
        self.rock.change_x+=1
        self.rock.center_y = 210
        self.rock.scale = 0.04
        
        self.rock2 = arcade.Sprite('rock.png')
        self.rock2.change_angle = 0 
        self.rock2.change_x = 0
        self.rock2.center_x = 2
        self.rock2.center_y = 210
        self.rock2.scale = 0.12
        
        self.physics_engine2 = arcade.PhysicsEnginePlatformer(  
            self.rock, walls, gravity_constant=1
        )  
        self.physics_engine3 = arcade.PhysicsEnginePlatformer(  
            self.rock2, walls, gravity_constant=1
        ) 
        pass
    
        self.start_rock2 = 0
        self.start_rock = False
         
    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass
        self.LayerWall.draw()
        self.hero.draw() 
        self.rock.draw()
        self.rock2.draw()
        
  
      
        #arcade.draw_text('GameState1 : '+str(self.score), 10, 20, arcade.color.WHITE, 14)
    
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.physics_engine.update()
        self.physics_engine2.update()
        self.physics_engine3.update()
        
        self.rock.update()
        self.rock2.update()
        self.hero.update
        
        self.start_rock2 += round(delta_time,5)
    
        if(round(self.start_rock2,0)>3  and self.start_rock==False): 
           self.rock2.change_x = 2
           self.rock2.change_angle = 5
           self.start_rock = True
        elif(round(self.start_rock2,0)%8==0 and self.start_rock==True):
           self.rock2.center_x = 2
           self.rock2.center_y = 210   
           self.rock2.change_x = 2
       
           
        
        if(self.rock.center_x>320):
            self.rock.change_x = 0
            self.rock.change_angle = 0
        
        

        if(self.hero.center_x>640):
            self.hero.center_x = 640      
        pass
    
    
        if(arcade.check_for_collision(self.hero,self.rock2)):

                arcade.play_sound(self.hero.sound_dead)
            
                game_view = gameOver()
                self.window.show_view(game_view)
                
        if(self.hero.center_x<0):
    
                arcade.play_sound(self.hero.sound_win)
            
                game_view = gameWin()
                self.window.show_view(game_view)
                
                
        soul = self.hero 
        soul.center_y = self.hero.center_y - 1
        hit_list = arcade.check_for_collision_with_list(soul,self.floor)
        k = len(hit_list)
       
        if(k>0):
            self.hero.jump_status = False
        

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP or key == arcade.key.W and self.hero.jump_status==False:
            self.hero.center_y += 100
            self.hero.jump_status = True
            #self.hero.texture = self.hero.texturesUp
        elif key == arcade.key.DOWN or key == arcade.key.S:
            #self.hero.change_y = -MOVEMENT_SPEED
            self.hero.texture = self.hero.texturesDown
        elif key == arcade.key.LEFT or key == arcade.key.A:
            self.hero.change_x = -MOVEMENT_SPEED
            self.hero.texture = self.hero.texturesLeft
        elif key == arcade.key.RIGHT or key == arcade.key.D:
            self.hero.change_x = MOVEMENT_SPEED
            self.hero.texture = self.hero.texturesRight
       

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP or key == arcade.key.DOWN or key == arcade.key.W or key == arcade.key.S:
            self.hero.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT or key == arcade.key.A or key == arcade.key.D:
            self.hero.change_x = 0