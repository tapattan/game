import arcade
import numpy as np 
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

import random 

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.ANTI_FLASH_WHITE)
        self.setup()

    def setup(self):
        pass
        self.b3  = arcade.SpriteList()
        for i in range(3000):
          m = random.randint(3,12)  
          c1 = arcade.SpriteCircle(m, arcade.color.BABY_BLUE, soft=True)
          c1.center_x = random.randint(0,800)
          c1.center_y = random.randint(400,600)
          self.b3.append(c1)

        self.b2  = arcade.SpriteList()
        for i in range(3000):
          m = random.randint(3,12)  
          c1 = arcade.SpriteCircle(m, arcade.color.WHITE, soft=True)
          c1.center_x = random.randint(0,800)
          c1.center_y = random.randint(200,400)
          self.b2.append(c1)  

        self.b1  = arcade.SpriteList()
        for i in range(3000):
          m = random.randint(3,12)  
          c1 = arcade.SpriteCircle(m, arcade.color.BABY_BLUE, soft=True)
          c1.center_x = random.randint(0,800)
          c1.center_y = random.randint(0,200)
          self.b1.append(c1)   

        
        self.timegame = 0

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.b3.draw()
        self.b2.draw()
        self.b1.draw()
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass

        self.timegame = self.timegame+delta_time 
        if(self.timegame > 0.5):
           self.timegame = 0 
           
           k = np.random.randint(0,3000,1000)
           for i in range(len(self.b3)):
              if(i in k):
                self.b3[i].center_x = random.randint(0,800)
                self.b3[i].center_y = random.randint(400,600)
           
           for i in range(len(self.b2)):
              if(i in k):
                self.b2[i].center_x = random.randint(0,800)
                self.b2[i].center_y = random.randint(200,400)
           
       
           for i in range(len(self.b1)):
              if(i in k):
                self.b1[i].center_x = random.randint(0,800)
                self.b1[i].center_y = random.randint(0,200)        


def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
