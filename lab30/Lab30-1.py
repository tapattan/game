import arcade
import random
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class GameOver(arcade.View):
    def __init__(self):
        super().__init__()
        self.setup()
    
    def setup(self):
        pass

    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)

    def on_draw(self):
        arcade.start_render()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('GameOver', 220, 300, arcade.color.WHITE, 30)
        arcade.draw_text("press 'spacebar' new game", 150, 240, arcade.color.WHITE, 20)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.SPACE:
            game_view = MyGame_state1()  #กลับไปฉากแรก 
            self.window.show_view(game_view)

            
    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        pass

########################################################### MyGame2 #####################################################################
class MyGame_state1(arcade.View):
    def __init__(self):
        super().__init__()
        self.setup()
    
    def setup(self):
        pass
############################# Mario map3 ##########################################################
        self.mario = arcade.Sprite('5-mario-running-01.gif',1) 
        self.mario.center_x = 550
        self.mario.center_y = 600
        
        self.textureList = [] 
        mario1 = arcade.load_texture('5-mario-running-01.gif')
        mario2 = arcade.load_texture('5-mario-running-02.gif')
        self.textureList.append(mario1)
        self.textureList.append(mario2)
        
        self.textureList_left = [] 
        mario1_left = arcade.load_texture('5-mario-running-01_left.gif')
        mario2_left = arcade.load_texture('5-mario-running-02_left.gif')
        self.textureList_left.append(mario1_left)
        self.textureList_left.append(mario2_left)
        
        self.direct = 'right'
        #pass

        self.gametime = 0
        self.frameAt = 0
        self.jump = 'no'
        self.timejump = 0

        #
        self.cloud = arcade.SpriteList(use_spatial_hash=True)   #SpriteList 

        for i in range(20):
          CloudX = arcade.Sprite('cloud1.png')
          CloudX.center_x = random.randint(10,580)
          CloudX.center_y = random.randint(10,580)
          CloudX.change_x = 5
          self.cloud.append(CloudX)
        
        #cloud = [self.Cloud1, ]

        self.physics_engine = arcade.PhysicsEnginePlatformer(
           self.mario, self.cloud, gravity_constant=1
        ) 

       
############################# End mario map2 ###########################################
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        
        
        self.mario.update()
        self.physics_engine.update()
        
        for i in self.cloud:
            if(i.center_x > 620):
               i.center_x = -20
               i.center_y = random.randint(10,580)
        
        if(self.jump=='yes'):
               self.timejump = self.timejump + delta_time
               self.mario.center_y = self.mario.center_y + 30

        if(self.timejump>0.20):
            self.jump = 'no'
            self.timejump = 0    
        

        ####################### Mario animation #########################
        self.mario.update() 
        
        self.gametime = self.gametime+delta_time 
        if(self.gametime>0.2):
           self.gametime = 0 
           self.frameAt = self.frameAt + 1
        
        if(self.frameAt==2):
           self.frameAt = 0 
        
        if(self.direct=='right'):   
          self.mario.texture = self.textureList[self.frameAt] #ก็คือใส่ให้ตัวละครของเราเปลี่ยนชุด ในตำแหน่ง texturelist นั้นๆ 
        elif(self.direct =='left'):
          self.mario.texture = self.textureList_left[self.frameAt]    #อันนี้คือเสื้อผ้าของหันซ้าย
        ############################## End ##########################
        
        
        if(self.mario.center_y < -100):
            game_view = GameOver()
            self.window.show_view(game_view)
       
            
    def on_show(self):
        arcade.set_background_color(arcade.color.BABY_BLUE)

    def on_draw(self):
        arcade.start_render()
    
        self.mario.draw()
        self.cloud.draw()
        """
        Draw "Game over" across the screen.
        """
        pass 
        arcade.draw_text('game state3', 10, 10, arcade.color.WHITE, 10)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.LEFT:
            self.mario.change_x = -5
            self.direct = 'left'
            
        elif key == arcade.key.RIGHT:
            self.mario.change_x = 5
            self.direct = 'right'
        elif key == arcade.key.UP:
            self.jump = 'yes'
            #self.mario.center_y = self.mario.center_y + 70
            
    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        
        if key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.mario.change_x = 0
 ##################################################################  End ################################################################# 


def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame_state1()  # จุด start game
    window.show_view(game)
    arcade.run()



if __name__ == "__main__":
    main()
