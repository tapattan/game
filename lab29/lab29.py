import arcade
import math
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

MOVEMENT_SPEED = 5

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)
    
    def setup(self):
        # Set up your game here
        self.mySprite = arcade.Sprite('car.png',0.3) #SCALING = 0.3 
        self.mySprite.center_x = 300 
        self.mySprite.center_y = 300

        self.mySprite.angle = 90
        #pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here
        self.mySprite.draw()

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.mySprite.update()
        
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        
     
        if key == arcade.key.UP:
            self.mySprite.change_x =  6*math.cos(math.radians(self.mySprite.angle))
            self.mySprite.change_y =  6*math.sin(math.radians(self.mySprite.angle))

        elif key == arcade.key.DOWN:
            self.mySprite.change_x = -1*6 * math.cos(math.radians(self.mySprite.angle))
            self.mySprite.change_y = -1*6 * math.sin(math.radians(self.mySprite.angle))

        if key == arcade.key.LEFT:
            self.mySprite.angle +=10 
            #self.mySprite.change_x =  6*math.cos(math.radians(self.mySprite.angle))
            #self.mySprite.change_y =  6*math.sin(math.radians(self.mySprite.angle))
        elif key == arcade.key.RIGHT:
            self.mySprite.angle -=10 
            #self.mySprite.change_x =  6*math.cos(math.radians(self.mySprite.angle))
            #self.mySprite.change_y =  6*math.sin(math.radians(self.mySprite.angle))


    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        if key == arcade.key.UP or key == arcade.key.DOWN:
            self.mySprite.change_y = 0
            self.mySprite.change_x = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.mySprite.change_x = 0
            self.mySprite.change_y = 0

        pass 

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
