import arcade
import random
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        pass
        
        '''self.listBox = arcade.SpriteList()
        for i in range(12):
          blockGreen = arcade.SpriteSolidColor(50,20,arcade.color.GREEN)
          blockGreen.center_x = 25+(i*50)
          blockGreen.center_y = 500

          self.listBox.append(blockGreen)

          blockRED = arcade.SpriteSolidColor(50,20,arcade.color.RED)
          blockRED.center_x = 25+(i*50)
          blockRED.center_y = 520

          self.listBox.append(blockRED)

          blockYELLOW = arcade.SpriteSolidColor(50,20,arcade.color.YELLOW)
          blockYELLOW.center_x = 25+(i*50)
          blockYELLOW.center_y = 540

          self.listBox.append(blockYELLOW)'''

        
        self.ball = arcade.SpriteCircle(50,arcade.color.SILVER,False)
        self.ball.center_x = 100
        self.ball.center_y = 100
        self.ball.scale = 0.1
        self.ball.change_y = 4

        self.player = arcade.SpriteSolidColor(70,10,arcade.color.SILVER)
        self.player.center_x = 100
        self.player.center_y = 20


        self.BOT = arcade.SpriteSolidColor(70,10,arcade.color.GOLD)
        self.BOT.center_x = 400
        self.BOT.center_y = 580

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        
        #self.listBox.draw()
        self.ball.draw()
        self.player.draw()
        self.BOT.draw()
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """

        self.ball.update()
        self.player.update()
        self.BOT.update()
        
        '''for i in range(len(self.listBox)):
          c = arcade.check_for_collision(self.ball,self.listBox[i])
          if(c):
           self.ball.change_y = -4  
           self.ball.change_x = random.randint(-4,4)
           self.listBox.pop(i)
           break '''

        c = arcade.check_for_collision(self.ball,self.player)
        if(c):
           self.ball.change_y = 4 + random.randint(0,10)
           self.ball.change_x = random.randint(-10,10)

        c = arcade.check_for_collision(self.ball,self.BOT)
        if(c):
           self.ball.change_y = -4 
           self.ball.change_x = random.randint(-4,4)   

        if(self.ball.center_x < 10 or self.ball.center_x > 590):
           self.ball.change_x = self.ball.change_x*-1

        

        ### AI ###
        self.BOT.center_x = self.ball.center_x

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.D:
            self.player.change_x = 5
        elif key == arcade.key.A:
            self.player.change_x = -5
       

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.D or key == arcade.key.A:
            self.player.change_x = 0


def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
