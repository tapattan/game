import arcade

SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500


class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        self.hammer = arcade.Sprite('../resources/hammer.png',0.2)
        self.hammer.center_x = 100
        self.hammer.center_y = 300

        self.button = arcade.Sprite('../resources/button1.png',0.2)
        self.button.center_x = 300
        self.button.center_y = 300

        self.texuteBtnNormal = arcade.load_texture('../resources/button1.png')
        self.texuteBtnHit = arcade.load_texture('../resources/button2.png')

        self.status_hit = False
        self.button_hit = False

        self.timeButton_Back = 0
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.hammer.draw()
        self.button.draw()
        # Your drawing code goes here
    
    #Called when the user presses a mouse button.
    def on_mouse_press(self, x, y, button, key_modifiers):
        self.hammer.angle = 45
        self.status_hit = True
        pass

    #Called when the user presses a mouse button.
    def on_mouse_release(self, x: float, y: float, button: int, modifiers: int):
        self.hammer.angle = 0
        self.status_hit = False
        pass

    #User moves mouse
    def on_mouse_motion(self, x: float, y: float, dx: float, dy: float):
        self.hammer.center_x = x 
        self.hammer.center_y = y 
        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        self.hammer.update()
        self.hammer.update_animation()
        
        if(arcade.check_for_collision(self.hammer,self.button) and self.status_hit):
           self.button.texture = self.texuteBtnHit
           self.button_hit = True
        
        if(self.button_hit==True):
          self.timeButton_Back += delta_time
          print(self.timeButton_Back)
          if(self.timeButton_Back>0.5):
              self.timeButton_Back = 0 
              self.button_hit = False
              self.button.texture = self.texuteBtnNormal

        pass


def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()