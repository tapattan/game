from PIL import Image
import numpy as np 

def find_content_bounds(image_path):
    image = Image.open(image_path)
    
    # ตรวจสอบและแปลงภาพให้เป็น RGBA หากไม่ใช่
    if image.mode != 'RGBA':
        image = image.convert('RGBA')
        
    pixels = np.array(image)
    
    # ตอนนี้ pixels จะเป็นอาร์เรย์สี่มิติเสมอ [width, height, RGBA]
    alpha = pixels[:, :, -1]  # อินเด็กซ์สำหรับ alpha channel
    
    # หา columns และ rows ที่มีเนื้อหา (alpha value > 0)
    content_columns = np.any(alpha > 0, axis=0)
    content_rows = np.any(alpha > 0, axis=1)
    
    # หา index ของ row และ column แรกและสุดท้ายที่มีเนื้อหา
    if content_columns.any() and content_rows.any():
        xmin, xmax = np.where(content_columns)[0][[0, -1]]
        ymin, ymax = np.where(content_rows)[0][[0, -1]]
        
        # คำนวณความกว้างและความสูงของพื้นที่ที่มีเนื้อหา
        content_width = xmax - xmin + 1
        content_height = ymax - ymin + 1
        
        return content_width, content_height
    else:
        # หากไม่พบเนื้อหา, คืนค่า 0
        return 0, 0
    
# สมมติว่า 'path_to_image.png' คือที่ตั้งไฟล์ของภาพ PNG ที่คุณต้องการตรวจสอบ
image_path = 'images/ambipom.png'
# ตัวอย่างการใช้งาน

content_width, content_height = find_content_bounds(image_path)
print(f'Content Width: {content_width}, Content Height: {content_height}')