import arcade
import os
import random 

SCREEN_WIDTH = 1200
SCREEN_HEIGHT = 800

def readContent():
    # ตั้งค่า path ของ directory ที่ต้องการอ่านไฟล์
    directory_path = 'images/'

    # เดินผ่าน directory และอ่านไฟล์ทั้งหมด
    k = os.listdir(directory_path)
    return k 

class MyGame(arcade.View):
    """ Main application class. """
    
    def __init__(self):
        super().__init__()
        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        pass

        self.mouse = arcade.Sprite('mouse.png')
        self.mouse.scale = 0.5

        self.pokeball_ball = arcade.Sprite('pokeball_ball.png') # โชว์แสดงบอลเฉยๆ 
        self.pokeball_ball.center_x = 150
        self.pokeball_ball.center_y = 600
        self.pokeball_ball.scale = 0.5

        self.pokeball_ball_btn = arcade.Sprite('pokeball_ball_button.png') # อันนี้คือปุ่มที่กดได้ เพราะต้องกดตรงกลาง
        self.pokeball_ball_btn.center_x = 150
        self.pokeball_ball_btn.center_y = 600
        self.pokeball_ball_btn.scale = 0.5

        self.ListNamePokemon = readContent()
        #k = random.randint(0,len(self.ListNamePokemon)-1)

        self.monDisPlay = arcade.Sprite() 
        self.monDisPlay.center_x = 400
        self.monDisPlay.center_y = 650

        self.border = arcade.Sprite('border.png') 
        self.border.center_x = 400 
        self.border.center_y = 600 
        self.border.scale = 0.5

        self.btnTake = arcade.Sprite('b1.png') 
        self.btnTake.center_x = 360 
        self.btnTake.center_y = 550 
        self.btnTake.scale = 0.25

        self.btnReject = arcade.Sprite('b2.png') 
        self.btnReject.center_x = 440 
        self.btnReject.center_y = 550 
        self.btnReject.scale = 0.27

        
        self.inventory = arcade.Sprite('inventory.jpg') 
        self.inventory.center_x = 350
        self.inventory.center_y = 240
        self.inventory.scale = 0.9
        
        self.inventoryList = [] # ตัวที่เก็บว่าตัวละคร มี่กี่ตัวแล้ว
        self.name_tmp = ''#self.ListNamePokemon[k]

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.pokeball_ball.draw()
        self.pokeball_ball_btn.draw()
        self.monDisPlay.draw() 

        self.border.draw()
        self.btnTake.draw() 
        self.btnReject.draw() 

        self.inventory.draw()

        for i in self.inventoryList:
            i.draw() 

        self.mouse.draw()

        pass

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
    
    def on_mouse_motion(self, x, y, dx, dy): 
        """ 
        Called whenever the mouse moves. 
        """
        self.mouse.center_x = x 
        self.mouse.center_y = y 

    def on_mouse_press(self, x, y, button, modifiers): 
        #print("Mouse button is pressed")
        k = arcade.check_for_collision(self.mouse, self.pokeball_ball_btn)
        if(k):
          k = random.randint(0,len(self.ListNamePokemon)-1)
          self.name_tmp = self.ListNamePokemon[k]
          t = arcade.load_texture('images/'+self.ListNamePokemon[k])
          self.monDisPlay.texture = t 
        

        k = arcade.check_for_collision(self.mouse, self.btnTake)
        if(k):
          print('take') 
          m = arcade.Sprite('images/'+self.name_tmp) 
          m.center_x = 120+(len(self.inventoryList)*78)+0
          m.center_y = 400
          m.scale = 1
          self.inventoryList.append(m)

        k = arcade.check_for_collision(self.mouse, self.btnReject)
        if(k):
          print('reject')

def main():
    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
