import arcade
import random 
SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        self.ball4 = arcade.Sprite('ball4.png', 0.5)
        self.ball4.center_x = 300
        self.ball4.center_y = 200
        self.ball4.change_x = 5
        self.ball4.change_y = 5
        self.ball4.change_angle = 5
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.ball4.draw()
        # Your drawing code goes here

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.ball4.update()
        if(self.ball4.center_y>SCREEN_HEIGHT-15):
            self.ball4.change_y = -5+random.randint(-1,1)
        elif(self.ball4.center_x>SCREEN_WIDTH-15):
            self.ball4.change_x = -5 +random.randint(-1,1)
        elif(self.ball4.center_y<0+15):
            self.ball4.change_y = 5 +random.randint(-1,1)
        elif(self.ball4.center_x<0+15):
            self.ball4.change_x = 5 +random.randint(-1,1)


def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()