import arcade
import random 
SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        
        self.basket = []
        for i in range(3):
          ball4 = arcade.Sprite('ball4.png', 0.5)
          ball4.center_x = random.randint(50,SCREEN_WIDTH-50)
          ball4.center_y = random.randint(50,SCREEN_HEIGHT-50)
          ball4.change_x = 5
          ball4.change_y = 5
          ball4.change_angle = 5
          self.basket .append(ball4)
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        
        for i in self.basket:
          i.draw()
        # Your drawing code goes here

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        for i in self.basket:
          i.update()
          if(i.center_y>SCREEN_HEIGHT-15):
            i.change_y = -5+random.randint(-1,1) 
          elif(i.center_x>SCREEN_WIDTH-15):
            i.change_x = -5 +random.randint(-1,1)
          elif(i.center_y<0+15):
            i.change_y = 5 +random.randint(-1,1)
          elif(i.center_x<0+15):
            i.change_x = 5 +random.randint(-1,1)
            
          for j in self.basket:
             if(i!=j):
               c = arcade.check_for_collision(i,j)
               if(c):  
                  if(i.change_y>0 and j.change_y>0):
                      i.change_x  *= -1
                      j.change_x *= -1
                  elif(i.change_y> 0 and j.change_y<0):
                      i.change_y *= -1 
                      j.change_y *= -1
                      i.change_x *= -1 
                      j.change_x *= -1
                  elif(i.change_y<0 and j.change_y<0):
                      i.change_x *= -1 
                      j.change_x *= -1
                  elif(i.change_y< 0 and j.change_y>0):
                      i.change_x *= -1 
                      j.change_x *= -1
                      i.change_x *= -1 
                      j.change_x *= -1
                     
                  
                  break
                   
 


def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()