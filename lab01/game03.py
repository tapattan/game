import arcade

# Set constants for the screen size
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

# Open the window. Set the window title and dimensions (width and height)
arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "Drawing Example")

# Set the background color to white.
# For a list of named colors see:
# http://arcade.academy/arcade.color.html
# Colors can also be specified in (red, green, blue) format and
# (red, green, blue, alpha) format.
arcade.set_background_color(arcade.color.WHITE)

# Start the render process. This must be done before any drawing commands.
arcade.start_render()
 

arcade.draw_circle_outline(100, 285+50, 80, arcade.color.BLUE, 5)
arcade.draw_circle_outline(300, 285+50, 80, arcade.color.BLACK, 5)
arcade.draw_circle_outline(500, 285+50, 80, arcade.color.RED, 5)
arcade.draw_circle_outline(200, 185+50, 88, arcade.color.YELLOW, 5)
arcade.draw_circle_outline(400, 185+50, 88, arcade.color.GREEN, 5)

# Finish drawing and display the result
arcade.finish_render()

# Keep the window open until the user hits the 'close' button
arcade.run()