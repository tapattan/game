import arcade

# Set constants for the screen size
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

# Open the window. Set the window title and dimensions (width and height)
arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")

# Set the background color to white.
# For a list of named colors see:
# http://arcade.academy/arcade.color.html
# Colors can also be specified in (red, green, blue) format and
# (red, green, blue, alpha) format.
arcade.set_background_color(arcade.color.AMAZON)

# Start the render process. This must be done before any drawing commands.
arcade.start_render()
 

s1 = arcade.Sprite('star.png')
s1.scale = 0.1

b = 11
c = b//2 
for i in range(b):
  for j in range(b):  
    if(j>=c and j<=(b//2)+i):
     s1.center_x = 50 + (50*j) 
     s1.center_y = 500 - (50*i)
     s1.draw() 
  c-=1   
  if(i>=(b//2)):
    break


# Finish drawing and display the result
arcade.finish_render()

# Keep the window open until the user hits the 'close' button
arcade.run()
