import arcade
import random 

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class monster(arcade.Sprite):
    def __init__(self,filename,scale,cx,cy,hpbar):
        super().__init__(filename,scale)

        self.center_x = cx 
        self.center_y = cy 

        self.dead = False
        self.HPBAR = hpbar

    def draw_hp(self):
        self.HPBAR.draw()

    def update_animate(self,delta_time=1/20):
        for i in range(len(self.HPBAR)):
            self.HPBAR[i].center_x = self.center_x
            self.HPBAR[i].center_y = self.top+self.HPBAR.barMarginH
             
            if(i==1):
              if(self.HPBAR.HP<=0):
                 self.HPBAR[i].width = 0 
              else:     
                 self.HPBAR[i].width = self.HPBAR.HP - 2
                 
              self.HPBAR[i].left = self.HPBAR[i].center_x - (self.HPBAR.init_hp/2) + 1

class hpbar(arcade.SpriteList):
    def __init__(self,levelHP,barH=10,barMarginH=20):
        super().__init__()
        self.init_hp = levelHP
        self.barMarginH = barMarginH
        width = levelHP
        height = barH
        border_size = 1
        
        self.HP = levelHP
        
        box = arcade.SpriteSolidColor(
            self.HP - border_size*3  ,
            height - border_size*3 ,
            arcade.color.GREEN,)
        
        box2 = arcade.SpriteSolidColor(
            width + border_size,
            height + border_size,
            arcade.color.BLACK,)

        self.append(box2)
        self.append(box)
    


class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        pass
        #ระดับของเลือดที่ต้องการ : levelHP
        #ความสูงของหลอดเลือด : barH
        #ระยะห่างระหว่างหัว monster และหลอดเลือด : barMarginH
        HPBAR = hpbar(levelHP=50,barH=10,barMarginH=10) 
        self.monster = monster('zombie1.gif',0.2,300,300,HPBAR)  
        


        # อาวุธ โจมตี
        self.star_weapon = arcade.Sprite('star_weapon.png',0.5)
        self.star_weapon.center_x = 700
        self.star_weapon.center_y = random.randint(10,580)
        self.star_weapon.change_x = -5
        self.star_weapon.change_angle = 5

        

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()

        self.monster.draw()
        self.monster.draw_hp()
        
        self.star_weapon.draw()
        # Your drawing code goes here

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.star_weapon.update()
  
        self.monster.update()
        self.monster.update_animate()
        
        if(arcade.check_for_collision(self.monster,self.star_weapon) and self.monster.dead==False):
            self.monster.HPBAR.HP -= 10
            self.star_weapon.center_x = 700
            if(self.monster.HPBAR.HP<=0):
               self.monster.angle = 90
               self.monster.dead = True
        
               
        if(self.star_weapon.center_x < -20):
           self.star_weapon.center_x = 700
           self.star_weapon.center_y = random.randint(10,580)
        
            
          
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        
        if(self.monster.dead==False):
            if key == arcade.key.W:
                self.monster.change_y = 5
            elif key == arcade.key.S:
                self.monster.change_y = -5
            elif key == arcade.key.A:
                self.monster.change_x = -5
            elif key == arcade.key.D:
                self.monster.change_x = 5

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.W or key == arcade.key.S:
            self.monster.change_y = 0
        elif key == arcade.key.A or key == arcade.key.D:
            self.monster.change_x = 0



def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
