
# ผลการทดลอง
# https://www.youtube.com/watch?v=0w1tVXwQj7A

# คู่มือจาก 
# https://py2app.readthedocs.io/en/latest/tutorial.html
1. pip3 install -U py2app
2. py2applet --make-setup myapplication.py
3. python setup.py py2app -A   ### สำหรับการทดสอบ 
4. python setup.py py2app or python3 setup.py py2app


# เว็บสำหรับแปลงไฟล์ .png ให้เป็น .icns
# https://anyconv.com/th/paelng-png-pen-icns/  

# การสร้าง virtualenv 
# คู่มือจาก https://www.freecodecamp.org/news/how-to-setup-virtual-environments-in-python/
1. pip install virtualenv

2. step2
 mkdir projectA
 cd projectA
 python3.8 -m venv env

3. step3
 source env/bin/activate

4. copy งาน to projectA 

5. ทดสอบ pip list 
   ของต้องว่างๆ


