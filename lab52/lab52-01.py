import arcade
import random 

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

class hpbar(arcade.SpriteList):
    def __init__(self):
        super().__init__()
        
        width = 50
        height = 10
        border_size = 1
        
        self.HP = 50
        
        box = arcade.SpriteSolidColor(
            self.HP - border_size*3  ,
            height - border_size*3 ,
            arcade.color.GREEN,)
        
        box2 = arcade.SpriteSolidColor(
            width + border_size,
            height + border_size,
            arcade.color.BLACK,)

        self.append(box2)
        self.append(box)
    
 

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        pass
 
        
        self.hero = arcade.Sprite('zombie1.gif',0.2)
        self.hero.center_x = 300
        self.hero.center_y = 300 - 25
        self.dead = False 
        

        self.star_weapon = arcade.Sprite('star_weapon.png',0.5)
        self.star_weapon.center_x = 700
        self.star_weapon.center_y = random.randint(10,580)
        self.star_weapon.change_x = -5
        self.star_weapon.change_angle = 5

        self.HPBAR = hpbar()

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.HPBAR.draw()
        self.hero.draw()
        self.star_weapon.draw()
        # Your drawing code goes here

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.star_weapon.update()
        self.HPBAR.update()
        self.hero.update()
        
        if(arcade.check_for_collision(self.hero,self.star_weapon) and self.dead==False):
            self.HPBAR.HP = self.HPBAR.HP - 10
            self.star_weapon.center_x = 700
            if(self.HPBAR.HP<=0):
               self.hero.angle = 90
               self.dead = True
        
        if(self.star_weapon.center_x < -20):
           self.star_weapon.center_x = 700
           self.star_weapon.center_y = random.randint(10,580)
        
            
          
        for i in range(len(self.HPBAR)):
            self.HPBAR[i].center_x = self.hero.center_x
            self.HPBAR[i].center_y = self.hero.top+10
            
            if(i==1):
              if(self.HPBAR.HP<=0):
                 self.HPBAR[i].width = 0 
              else:     
                 self.HPBAR[i].width = self.HPBAR.HP - 2
                 
              self.HPBAR[i].left = self.HPBAR[i].center_x - 24
    
    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        
        if(self.dead==False):
            if key == arcade.key.W:
                self.hero.change_y = 5
            elif key == arcade.key.S:
                self.hero.change_y = -5
            elif key == arcade.key.A:
                self.hero.change_x = -5
            elif key == arcade.key.D:
                self.hero.change_x = 5

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.W or key == arcade.key.S:
            self.hero.change_y = 0
        elif key == arcade.key.A or key == arcade.key.D:
            self.hero.change_x = 0



def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
