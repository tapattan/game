import arcade

# Set up constants for the screen size
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

# Set up constants for the background image size
BACKGROUND_WIDTH = 4000
BACKGROUND_HEIGHT = 600

# Set up constants for the scrolling speed
SCROLL_SPEED = 10

class MyGame(arcade.Window):
    def __init__(self, width, height, title):
        super().__init__(width, height, title)
        self.background_x = 0  # Initial x position of the background

    def setup(self):
        # Load the background image
        self.background = arcade.load_texture("IcyMap5.png")

    def on_draw(self):
        arcade.start_render()

        # Draw the background image twice, side by side, to create an infinite scrolling effect
        arcade.draw_texture_rectangle(self.background_x, SCREEN_HEIGHT // 2, BACKGROUND_WIDTH, BACKGROUND_HEIGHT, self.background)
        arcade.draw_texture_rectangle(self.background_x + BACKGROUND_WIDTH, SCREEN_HEIGHT // 2, BACKGROUND_WIDTH, BACKGROUND_HEIGHT, self.background)

    def update(self, delta_time):
        # Update the position of the background image to achieve scrolling effect
        self.background_x -= SCROLL_SPEED

        # If the background image moves completely offscreen to the left, reset its position to the right
        if self.background_x < -BACKGROUND_WIDTH:
            self.background_x = 0

def main():
    window = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT, "Scrolling Background")
    window.setup()
    arcade.run()

if __name__ == "__main__":
    main()