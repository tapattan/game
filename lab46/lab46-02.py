import arcade

SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500
MOVEMENT_SPEED = 5

class hero(arcade.Sprite):
    def __init__(self):

        # Set up parent class
        super().__init__()
        
 
        self.texturesCat = []
        self.texturesCat.append(arcade.load_texture("catAct1.gif"))
        self.texturesCat.append(arcade.load_texture("catAct2.gif"))
        self.texturesCat.append(arcade.load_texture("catAct3.gif"))
        self.texturesCat.append(arcade.load_texture("catAct4.gif"))
        self.texturesCat.append(arcade.load_texture("catAct5.gif"))
        self.texturesCat.append(arcade.load_texture("catAct6.gif"))
        self.texturesCat.append(arcade.load_texture("catAct7.gif"))
        self.texturesCat.append(arcade.load_texture("catAct8.gif"))
 
        
        self.textureList = self.texturesCat 
        self.statusFrameaction = len(self.texturesCat)
        print(self.statusFrameaction)


        self.timeFrame = 0
        self.timeKFrame = 0
        self.texture = self.textureList[self.timeFrame]
        self.center_x = 100
        self.center_y = 300
        self.scale = 0.1

        self.timewave = 0

        self.rainbow = []
        self.rainbow2 = []
        self.rainbow3 = []
        self.rainbow4 = []
        self.rainbow5 = []
        self.rainbow6 = []
        for i in range(24):
          p1 = arcade.SpriteSolidColor(5,5,arcade.color.RED)
          self.rainbow.append(p1)
          p1 = arcade.SpriteSolidColor(5,5,arcade.color.ORANGE)
          self.rainbow2.append(p1)
          p1 = arcade.SpriteSolidColor(5,5,arcade.color.YELLOW)
          self.rainbow3.append(p1)
          p1 = arcade.SpriteSolidColor(5,5,arcade.color.GREEN)
          self.rainbow4.append(p1)
          p1 = arcade.SpriteSolidColor(5,5,arcade.color.BLUE)
          self.rainbow5.append(p1)
          p1 = arcade.SpriteSolidColor(5,5,arcade.color.BLUE_VIOLET)
          self.rainbow6.append(p1)

    def update_animation(self, delta_time: float = 1 / 20):
        self.statusFrameaction = len(self.textureList)
        self.timeKFrame +=(delta_time)
        if(self.timeKFrame>0.2):
            self.timeKFrame=0
            self.timeFrame+=1
            if(self.timeFrame%self.statusFrameaction==0):
                self.timeFrame = 0
            
            try:
              self.texture = self.textureList[self.timeFrame]
            except:
              pass
        
        self.timewave += delta_time 
        k = 1
        if(self.timewave < 2):
           k = 0
        elif(self.timewave>2 and self.timewave < 4):
           k = 1
        else:
           self.timewave = 0 

        p = 0
        for i in range(len(self.rainbow)):
          if(p<6):  
            self.rainbow[i].center_x = self.center_x-20-(i*5)
            self.rainbow[i].center_y = self.center_y+15+(-5*k)
            self.rainbow2[i].center_x = self.center_x-20-(i*5)
            self.rainbow2[i].center_y = self.center_y+10+(-5*k)
            self.rainbow3[i].center_x = self.center_x-20-(i*5)
            self.rainbow3[i].center_y = self.center_y+5+(-5*k)
            self.rainbow4[i].center_x = self.center_x-20-(i*5)
            self.rainbow4[i].center_y = self.center_y+0+(-5*k)
            self.rainbow5[i].center_x = self.center_x-20-(i*5)
            self.rainbow5[i].center_y = self.center_y+-5+(-5*k)
            self.rainbow6[i].center_x = self.center_x-20-(i*5)
            self.rainbow6[i].center_y = self.center_y-10+(-5*k)
          elif(p< 12):
            self.rainbow[i].center_x = self.center_x-20-(i*5)
            self.rainbow[i].center_y = self.center_y+15-5+(5*k)  
            self.rainbow2[i].center_x = self.center_x-20-(i*5)
            self.rainbow2[i].center_y = self.center_y+10-5+(5*k) 
            self.rainbow3[i].center_x = self.center_x-20-(i*5)
            self.rainbow3[i].center_y = self.center_y+5-5+(5*k) 
            self.rainbow4[i].center_x = self.center_x-20-(i*5)
            self.rainbow4[i].center_y = self.center_y+0-5+(5*k) 
            self.rainbow5[i].center_x = self.center_x-20-(i*5)
            self.rainbow5[i].center_y = self.center_y+-5-5+(5*k) 
            self.rainbow6[i].center_x = self.center_x-20-(i*5)
            self.rainbow6[i].center_y = self.center_y+-10-5+(5*k) 
          elif(p<18):  
            self.rainbow[i].center_x = self.center_x-20-(i*5)
            self.rainbow[i].center_y = self.center_y+15+(-5*k) 
            self.rainbow2[i].center_x = self.center_x-20-(i*5)
            self.rainbow2[i].center_y = self.center_y+10+(-5*k)
            self.rainbow3[i].center_x = self.center_x-20-(i*5)
            self.rainbow3[i].center_y = self.center_y+5+(-5*k)
            self.rainbow4[i].center_x = self.center_x-20-(i*5)
            self.rainbow4[i].center_y = self.center_y+0+(-5*k)
            self.rainbow5[i].center_x = self.center_x-20-(i*5)
            self.rainbow5[i].center_y = self.center_y+-5+(-5*k)
            self.rainbow6[i].center_x = self.center_x-20-(i*5)
            self.rainbow6[i].center_y = self.center_y-10+(-5*k)
          else:
            self.rainbow[i].center_x = self.center_x-20-(i*5)
            self.rainbow[i].center_y = self.center_y+15-5+(5*k)  
            self.rainbow2[i].center_x = self.center_x-20-(i*5)
            self.rainbow2[i].center_y = self.center_y+10-5+(5*k)  
            self.rainbow3[i].center_x = self.center_x-20-(i*5)
            self.rainbow3[i].center_y = self.center_y+5-5+(5*k)  
            self.rainbow4[i].center_x = self.center_x-20-(i*5)
            self.rainbow4[i].center_y = self.center_y+0-5+(5*k) 
            self.rainbow5[i].center_x = self.center_x-20-(i*5)
            self.rainbow5[i].center_y = self.center_y+-5-5+(5*k) 
            self.rainbow6[i].center_x = self.center_x-20-(i*5)
            self.rainbow6[i].center_y = self.center_y+-10-5+(5*k) 
          p+=1  

    def effect(self,delta_time: float = 1 / 20):
        for i in self.rainbow:
          i.draw() 
        for i in self.rainbow2:
          i.draw()   
        for i in self.rainbow3:
          i.draw()   
        for i in self.rainbow4:
          i.draw()
        for i in self.rainbow5:
          i.draw()
        for i in self.rainbow6:
          i.draw()        
         

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        pass

        self.hero = hero()

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here
        #self.hero.p1.draw()
        self.hero.effect()
        self.hero.draw()
        
        
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.hero.update()
        self.hero.update_animation()
        

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP or key == arcade.key.W:
            self.hero.change_y = MOVEMENT_SPEED
        elif key == arcade.key.DOWN or key == arcade.key.S:
            self.hero.change_y = -MOVEMENT_SPEED
        elif key == arcade.key.LEFT or key == arcade.key.A:
            self.hero.change_x = -MOVEMENT_SPEED
        elif key == arcade.key.RIGHT or key == arcade.key.D:
            self.hero.change_x = MOVEMENT_SPEED

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP or key == arcade.key.DOWN or key == arcade.key.W or key == arcade.key.S:
            self.hero.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT or key == arcade.key.A or key == arcade.key.D:
            self.hero.change_x = 0

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
