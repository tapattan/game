import arcade

SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500
MOVEMENT_SPEED = 5

class hero(arcade.Sprite):
    def __init__(self):

        # Set up parent class
        super().__init__()
        
 
        self.texturesCat = []
        self.texturesCat.append(arcade.load_texture("catAct1.gif"))
        self.texturesCat.append(arcade.load_texture("catAct2.gif"))
        self.texturesCat.append(arcade.load_texture("catAct3.gif"))
        self.texturesCat.append(arcade.load_texture("catAct4.gif"))
        self.texturesCat.append(arcade.load_texture("catAct5.gif"))
        self.texturesCat.append(arcade.load_texture("catAct6.gif"))
        self.texturesCat.append(arcade.load_texture("catAct7.gif"))
        self.texturesCat.append(arcade.load_texture("catAct8.gif"))
 
        
        self.textureList = self.texturesCat 
        self.statusFrameaction = len(self.texturesCat)
        print(self.statusFrameaction)


        self.timeFrame = 0
        self.timeKFrame = 0
        self.texture = self.textureList[self.timeFrame]
        self.center_x = 100
        self.center_y = 300
        self.scale = 0.1

        self.timewave = 0

    def update_animation(self, delta_time: float = 1 / 20):
        self.statusFrameaction = len(self.textureList)
        self.timeKFrame +=(delta_time)
        if(self.timeKFrame>0.2):
            self.timeKFrame=0
            self.timeFrame+=1
            if(self.timeFrame%self.statusFrameaction==0):
                self.timeFrame = 0
            
            try:
              self.texture = self.textureList[self.timeFrame]
            except:
              pass
        
    
    def effect(self,delta_time: float = 1 / 20):
        self.timewave += delta_time 
        k = 1
        if(self.timewave < 2):
           k = 0
        elif(self.timewave>2 and self.timewave < 4):
           k = 1
        else:
           self.timewave = 0 

        p = 0
        for i in range(60):
           if(p<3):
                arcade.draw_rectangle_filled(self.center_x-20-(i*5), self.center_y+15+(-5*k),
                                 5, 5,
                                 arcade.color.RED)    
                arcade.draw_rectangle_filled(self.center_x-20-(i*5), self.center_y+10+(-5*k),
                                 5, 5,
                                 arcade.color.ORANGE)     
                arcade.draw_rectangle_filled(self.center_x-20-(i*5), self.center_y+5+(-5*k),
                                 5, 5,
                                 arcade.color.YELLOW)  
                arcade.draw_rectangle_filled(self.center_x-20-(i*5), self.center_y+0+(-5*k),
                                 5, 5,
                                 arcade.color.GREEN) 
                arcade.draw_rectangle_filled(self.center_x-20-(i*5), self.center_y-5+(-5*k),
                                 5, 5,
                                 arcade.color.BLUE) 

                arcade.draw_rectangle_filled(self.center_x-20-(i*5), self.center_y-10+(-5*k),
                                 5, 5,
                                 arcade.color.BLUE_VIOLET) 
           elif(p>=3 and p<=5):
                 arcade.draw_rectangle_filled(self.center_x-20-(i*5), self.center_y+15-5+(5*k),
                                 5, 5,
                                 arcade.color.RED)  
                 arcade.draw_rectangle_filled(self.center_x-20-(i*5), self.center_y+10-5+(5*k),
                                 5, 5,
                                 arcade.color.ORANGE)  
                 arcade.draw_rectangle_filled(self.center_x-20-(i*5), self.center_y+5-5+(5*k),
                                 5, 5,
                                 arcade.color.YELLOW)   
                 arcade.draw_rectangle_filled(self.center_x-20-(i*5), self.center_y+0-5+(5*k),
                                 5, 5,
                                 arcade.color.GREEN)

                 arcade.draw_rectangle_filled(self.center_x-20-(i*5), self.center_y+-5-5+(5*k),
                                 5, 5,
                                 arcade.color.BLUE)
                 arcade.draw_rectangle_filled(self.center_x-20-(i*5), self.center_y-10-5+(5*k),
                                 5, 5,
                                 arcade.color.BLUE_VIOLET)

       
           p+=1    
           if(p>5):
            p = 0                      

         

class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        pass

        self.hero = hero()

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Your drawing code goes here
        self.hero.effect()
        self.hero.draw()
        
        

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass
        self.hero.update()
        self.hero.update_animation()
        

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP or key == arcade.key.W:
            self.hero.change_y = MOVEMENT_SPEED
        elif key == arcade.key.DOWN or key == arcade.key.S:
            self.hero.change_y = -MOVEMENT_SPEED
        elif key == arcade.key.LEFT or key == arcade.key.A:
            self.hero.change_x = -MOVEMENT_SPEED
        elif key == arcade.key.RIGHT or key == arcade.key.D:
            self.hero.change_x = MOVEMENT_SPEED

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP or key == arcade.key.DOWN or key == arcade.key.W or key == arcade.key.S:
            self.hero.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT or key == arcade.key.A or key == arcade.key.D:
            self.hero.change_x = 0

def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
