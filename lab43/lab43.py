import arcade
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

 
class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()

        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        arcade.draw_text('main', 10, 20, arcade.color.WHITE, 14)
        
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """

        pass


def main():
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
