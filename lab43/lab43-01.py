import arcade
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

 
class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()

        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        pass

        map_name = "map01.json"

        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "mazeLayer": {
                "use_spatial_hash": True,
            },
        }

        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)
        
        self.mazeLayer_map = self.tile_map.sprite_lists["mazeLayer"]

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.mazeLayer_map.draw()
        arcade.draw_text('main', 10, 20, arcade.color.WHITE, 14)
        
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """

        pass


def main():
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
