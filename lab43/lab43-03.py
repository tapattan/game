import arcade
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

MAX_SCREEN_WIDTH = 2000
MAX_SCREEN_HEIGHT = 2000

class MyGame(arcade.View):
    """ Main application class. """

    def __init__(self):
        super().__init__()

        arcade.set_background_color(arcade.color.AMAZON)
        self.setup()

    def setup(self):
        pass
        

        ########## zone hero ################
        self.timeaction = 0 #Running only for the first time once
        self.asunaFrame = 0

        self.asuna = arcade.Sprite('Character/Walk1.png')
        self.asuna.center_x = 100
        self.asuna.center_y = 100
        self.asuna.scale= 0.5
        
        self.asunaTexture1 = arcade.load_texture('Character/Walk2.png')
        self.asunaTexture2 = arcade.load_texture('Character/Walk3.png') #Loading Frame 

        self.timeaction = 0 #Running only for the first time once
        self.asunaFrame = 0

        self.asunaTexture3 = arcade.load_texture('Character/Right1.png')
        self.asunaTexture4 = arcade.load_texture('Character/Right3.png') #Loading Frame

        self.asunaTexture5 = arcade.load_texture('Character/Back1.png')
        self.asunaTexture6 = arcade.load_texture('Character/Back3.png') #Loading Frame

        self.asunaTexture7 = arcade.load_texture('Character/Left1.png')
        self.asunaTexture8 = arcade.load_texture('Character/Left3.png') #Loading Frame

        self.direction= 'Walk' 
        ############### end zone hero ##############


        ############## zone load map ############
        map_name = "map01.json"

        # Layer specific options are defined based on Layer names in a dictionary
        # Doing this will make the SpriteList for the platforms layer
        # use spatial hashing for detection.
        layer_options = {
            "mazeLayer": {
                "use_spatial_hash": True,
            },
        }

        # Read in the tiled map
        TILE_SCALING = 1
        self.tile_map = arcade.load_tilemap(map_name, TILE_SCALING, layer_options)
        
        self.mazeLayer_map = self.tile_map.sprite_lists["mazeLayer"]
        ############## end zone load map ################       
 
        
        ############# zone set physic with hero 
        walls = [self.mazeLayer_map, ]
        self.physics_engine = arcade.PhysicsEnginePlatformer(
            self.asuna, walls, gravity_constant=0
        )
        ###################### end zone set physic with hero 

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.mazeLayer_map.draw()
        self.asuna.draw()
        #arcade.draw_text('main', 10,10, arcade.color.WHITE, 14)
        
    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        
        self.physics_engine.update()

        ########## zone control hero #############
        self.asuna.update()
        self.timeaction = self.timeaction + delta_time 

        if(self.timeaction < 0.2): #Means that 1 second has already passed
            self.asunaFrame = 1
        elif(self.timeaction>0.2 and self.timeaction<0.4):
            self.asunaFrame = 2
        else:
           self.timeaction = 0 #Commands them to recount
         

        if(self.direction=='Walk'):
            if(self.asunaFrame==1):
                self.asuna.texture = self.asunaTexture2
            if(self.asunaFrame==2):
                self.asuna.texture = self.asunaTexture1
        if(self.direction=='Right'):
    
            if(self.asunaFrame==1):
                self.asuna.texture = self.asunaTexture3
            if(self.asunaFrame==2):
                self.asuna.texture = self.asunaTexture4
        if(self.direction=='Left'):
          
            if(self.asunaFrame==1):
                self.asuna.texture = self.asunaTexture7
            if(self.asunaFrame==2):
                self.asuna.texture = self.asunaTexture8
        if(self.direction=='Back'):
            if(self.asunaFrame==1):
                self.asuna.texture = self.asunaTexture5
            if(self.asunaFrame==2):
                self.asuna.texture = self.asunaTexture6
        ############## end zone control hero  ############      
        pass



        #ส่วนของกล้องที่เลื่อนตาม
        self.view_position_playerX = self.asuna.center_x - (SCREEN_WIDTH//2)
        self.view_position_playerY = self.asuna.center_y - (SCREEN_HEIGHT//2)

        print(self.view_position_playerX,self.view_position_playerY)
   

        if(self.view_position_playerX<0 and self.view_position_playerY < 0): #ซ้ายล่าง
            print(1)
            arcade.set_viewport(0,                      #(left, right, bottom, top)
                                0+SCREEN_WIDTH,
                                0,
                                SCREEN_HEIGHT)  
        elif(self.view_position_playerX>MAX_SCREEN_WIDTH-SCREEN_WIDTH and self.view_position_playerY >MAX_SCREEN_HEIGHT-SCREEN_HEIGHT ): #ขวาบน
            print(6)
            arcade.set_viewport(MAX_SCREEN_WIDTH-SCREEN_WIDTH,
                                MAX_SCREEN_WIDTH,
                                MAX_SCREEN_HEIGHT-SCREEN_HEIGHT,
                                MAX_SCREEN_HEIGHT)     
        elif(self.view_position_playerY>MAX_SCREEN_HEIGHT-SCREEN_HEIGHT and self.view_position_playerX > 0 ):  #บนๆ กลางๆ 
            print(7)
            arcade.set_viewport(self.view_position_playerX,
                                self.view_position_playerX+SCREEN_WIDTH,
                                MAX_SCREEN_HEIGHT-SCREEN_HEIGHT,
                                MAX_SCREEN_HEIGHT)

        elif(self.view_position_playerX>MAX_SCREEN_WIDTH-SCREEN_WIDTH and self.view_position_playerY > 0 ):  #ขวากลางๆ 
            print(5)
            arcade.set_viewport(MAX_SCREEN_WIDTH-SCREEN_WIDTH,
                                MAX_SCREEN_WIDTH,
                                self.view_position_playerY,
                                self.view_position_playerY+SCREEN_HEIGHT)

        elif(self.view_position_playerX < 0 and self.view_position_playerY <MAX_SCREEN_HEIGHT-SCREEN_HEIGHT):  #บนกลางซ้าย
            print(4)
            arcade.set_viewport(0,
                                0+SCREEN_WIDTH,
                                self.view_position_playerY,
                                self.view_position_playerY+SCREEN_HEIGHT)                                           
        elif(self.view_position_playerX>MAX_SCREEN_WIDTH-SCREEN_WIDTH and self.view_position_playerY < 0):  #ขวาสุด
            print(2)
            arcade.set_viewport(MAX_SCREEN_WIDTH-SCREEN_WIDTH,
                                MAX_SCREEN_WIDTH,
                                0,
                                SCREEN_HEIGHT)
        elif(self.view_position_playerX>0 and self.view_position_playerY<0 and self.view_position_playerY <MAX_SCREEN_HEIGHT-SCREEN_HEIGHT):  #กลางๆ ล่างๆ  
            print(3)
            arcade.set_viewport(self.view_position_playerX,
                                self.view_position_playerX+SCREEN_WIDTH,
                                0,
                                SCREEN_HEIGHT)
        elif(self.view_position_playerX<0 and self.view_position_playerY >MAX_SCREEN_HEIGHT-SCREEN_HEIGHT ): #ซ้ายบน
            print(8)
            arcade.set_viewport(0,                      #(left, right, bottom, top)
                                0+SCREEN_WIDTH,
                                MAX_SCREEN_HEIGHT-SCREEN_HEIGHT,
                                MAX_SCREEN_HEIGHT) 
        else:
            print(9)
            arcade.set_viewport(self.view_position_playerX,
                                self.view_position_playerX+SCREEN_WIDTH,
                                self.view_position_playerY,
                                self.view_position_playerY+SCREEN_HEIGHT)

                        
        
        #end ส่วนของกล่้องที่เลื่อนตาม 

       


    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        if key == arcade.key.W:
            self.direction='Back'
            self.asuna.change_y = 3
        elif key == arcade.key.A:
            self.asuna.change_x = -3
            self.direction='Left'
        elif key == arcade.key.S:
            self.asuna.change_y = -3
            self.direction='Walk'
        elif key == arcade.key.D:
            self.asuna.change_x = 3
            self.direction='Right'

    def on_key_release(self, key, modifiers):
        if key == arcade.key.W:
            self.asuna.change_y = 0
        elif key == arcade.key.A:
            self.asuna.change_x = 0
        elif key == arcade.key.S:
            self.asuna.change_y = 0
        elif key == arcade.key.D:
            self.asuna.change_x = 0

def main():
    
    #game.setup()
    #arcade.run()

    window = arcade.Window(SCREEN_WIDTH,SCREEN_HEIGHT, "แบบฝึกหัดเกมของฉัน")
    game = MyGame()
    window.show_view(game)
    arcade.run()


if __name__ == "__main__":
    main()
